package engine

import (
	"gitlab.com/meeting-master/sdk/errors"
	"gitlab.com/meeting-master/sdk/messaging/model"
	"gitlab.com/meeting-master/sdk/messaging/repository"
)

type MessageEngine interface {
	ActivationMessage(name, to, code, lang string) (string, *errors.Error)
	Messages() ([]model.Message, *errors.Error)
	UpdateMessage(messages []model.Message) *errors.Error
	AppointmentNotification(data model.AppointmentNotification) (string, *errors.Error)
	CancelAppointmentNotification(data model.AppointmentNotification) (string, *errors.Error)
	ForgotPassword(to, name, password string) (string, *errors.Error)
}

type engine struct {
	repository repository.MessagingRepository
}


func Init(repo repository.MessagingRepository) MessageEngine {
	return &engine{repository: repo}
}