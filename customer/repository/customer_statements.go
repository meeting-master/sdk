package repository

const (
	insertNewCustomer = `INSERT INTO common.customer (alias, name, phone_number, web_site, activities)
							VALUES (:alias, :name, :phonenumber, :website, :activities) 
							RETURNING id`

	insertNewUser = `INSERT INTO common.user( username, password, activation_code, customer_id)
						VALUES(:username, :password, :code, :customerid)
						RETURNING id`

	customers = `SELECT c.id, c.alias, c.name, c.phone_number, c.web_site, c.logo_url, c.rating, c.value
					FROM common.customer c WHERE is_archived = false`

	customerById = `SELECT c.id, c.alias, c.name, c.phone_number, c.web_site, c.tags, c.logo_url, c.rating, c.value, c.activities
					FROM common.customer c WHERE c.id::TEXT = $1 AND is_archived = false`

	customersByTag = `SELECT c.id as customer_id, c.logo_url, c.rating, c.activities, o.id as office_id, o.longitude, o.latitude, o.district_id
						FROM common.customer c
						INNER JOIN common.office o ON o.customer_id = c.id
						WHERE (LOWER(c.name) LIKE $1 OR LOWER(c.tags) LIKE $1) AND c.is_archived = false AND c.value > 0`

	customersByTownAndTag = `SELECT c.id as customer_id, c.logo_url, c.rating, c.activities, o.id as office_id, o.longitude, o.latitude, o.district_id
						FROM common.customer c
						INNER JOIN common.office o ON o.customer_id = c.id
						INNER JOIN location.district d ON o.district_id = d.id
						INNER JOIN location.town t ON d.town_id = t.id
						WHERE (LOWER(c.name) LIKE $1 OR LOWER(c.tags) LIKE $1) AND c.is_archived = false AND c.value > 0 AND t.id = $2`

	updateCustomer = `UPDATE common.customer SET alias = :alias, name = :name, phone_number = :phonenumber, web_site = :website, 
							tags = :tagstring, logo_url = :logourl, activities = :activities
						WHERE id = :id
						RETURNING id`

	deleteCustomer = `UPDATE common.customer SET is_archived = true WHERE id = :id RETURNING id`

	appointmentStream = `
			SELECT 
					ap.id, ap.created_at, ap.canceled_at, ap.consumed_at,
					s.id as service_id, s.name as service_name,
					o.id as office_id, o.name as office_name
			FROM common.appointment ap
			inner join common.time_slots ts on ts.id = ap.time_slot_id
			inner join common.office o on o.id = ts.office_id 
			inner join common.service s on s.id = ts.service_id
			WHERE o.customer_id = $1
			AND ap.created_at BETWEEN $2 and $3
			`
	logo = `SELECT name, logo_url FROM common.customer WHERE logo_url NOTNULL`

	appointments = `
			SELECT office_id, count(ap.id)
			FROM common.appointment ap
			inner join common.time_slots ts on ts.id = ap.time_slot_id
			inner join common.office o on o.id = ts.office_id
			WHERE o.customer_id = $1
			AND ap.created_at BETWEEN $2 and $3
			group by ts.office_id
			`
	consumedAppointments = `
			SELECT office_id, count(ap.id)
			FROM common.appointment ap
			inner join common.time_slots ts on ts.id = ap.time_slot_id
			inner join common.office o on o.id = ts.office_id
			WHERE o.customer_id = $1
			AND ap.consumed_at BETWEEN $2 and $3 
			group by ts.office_id
			`
	canceledAppointments = `
			SELECT office_id, count(ap.id)
			FROM common.appointment ap
			inner join common.time_slots ts on ts.id = ap.time_slot_id
			inner join common.office o on o.id = ts.office_id
			WHERE o.customer_id = $1
			AND ap.canceled_at BETWEEN $2 and $3
			group by ts.office_id
			`
)
