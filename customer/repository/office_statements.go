package repository

const (
	insertNewOffice = `INSERT INTO common.office (alias, name, street_number, street_name, zip_code,
							phone_number, web_site, email, longitude, latitude, district_id, customer_id, service_ids)
						VALUES (:alias, :name, :street_number, :street_name, :zip_code, :phone_number,
								:web_site, :email, :longitude, :latitude, :district_id, :customer_id, :service_ids) 
						RETURNING id`

	updateOffice = `UPDATE common.office SET alias = :alias, name = :name, street_number = :street_number, street_name = :street_name,
								zip_code = :zip_code, phone_number = :phone_number, web_site = :web_site, email = :email, longitude = :longitude,
								latitude = :latitude, district_id = :district_id, customer_id = :customer_id, service_ids = :service_ids, is_archived = :is_archived
 					WHERE id = :id
					RETURNING id`

	officesByCustomer = `SELECT o.id,
				o.alias, 
				o.name, 
				o.street_number,
				o.street_name,
				o.zip_code,
				o.phone_number, 
				o.web_site,
				o.email,
				o.longitude,
				o.latitude,
				o.district_id,
				o.customer_id,
				o.is_archived,
				o.service_ids,
				c.name as "customer_name", c.alias as "customer_alias" 
				FROM common.office o
				inner join common.customer c on o.customer_id = c.id
				WHERE o.customer_id::TEXT = $1`

	officesByIds = `SELECT o.id,
				o.alias, 
				o.name, 
				o.street_number,
				o.street_name,
				o.zip_code,
				o.phone_number, 
				o.web_site,
				o.email,
				o.longitude,
				o.latitude,
				o.district_id,
				o.customer_id,
				o.is_archived,
				c.name as "customer_name", c.alias as "customer_alias",
				CONCAT(t.name, '-', d.name) as location
				FROM common.office o
					INNER JOIN common.customer c on o.customer_id = c.id
					LEFT JOIN location.district d ON d.id = o.district_id
					LEFT JOIN location.town t on t.id = d.town_id 
				WHERE o.is_archived = false AND o.id::TEXT IN (?)`

	deleteOffice = `UPDATE common.office SET is_archived = true WHERE id = :id RETURNING id`

	deleteOfficesByCustomer = `UPDATE common.office SET is_archived = true WHERE customer_id = :id`

	officeRating = `SELECT r.id, r.rating, r.created_at, r.comment
						from common.rating r
						inner join common.appointment a on a.id = r.appointment_id
						inner join common.time_slots ts on ts.id = a.time_slot_id
						inner join common.office o on o.id = ts.office_id
						where o.id = $1 AND r.created_at BETWEEN $2 and $3 AND r.comment != ''`

)
