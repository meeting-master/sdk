package model

import "time"

type PubRequest struct {
	ID         string    `json:"id"`
	StartDate  time.Time `json:"startDate"`
	Duration   int       `json:"duration"`
	Picture    string    `json:"picture"`
	Url        string    `json:"url"`
	OwnerID    string    `json:"ownerId"`
	LocationID string    `json:"locationId"`
}

func (pr PubRequest) IsValid() bool {
	return !pr.StartDate.IsZero() &&
		pr.StartDate.After(time.Now().UTC()) &&
		pr.OwnerID != "" &&
		pr.Picture != "" &&
		pr.Duration > 0
}

type ValidationRequest struct {
	ID      string `json:"id"`
	Comment string `json:"comment"`
	Valid   bool   `json:"valid"`
}

func (vr ValidationRequest) IsValid() bool {
	return vr.ID != ""
}
