package service

import (
	"context"
	"gitlab.com/meeting-master/sdk/messaging/engine"
	"time"

	"gitlab.com/meeting-master/sdk/auth/service"
	"gitlab.com/meeting-master/sdk/customer/model"
	"gitlab.com/meeting-master/sdk/customer/repository"
	"gitlab.com/meeting-master/sdk/errors"
)

type CustomerService interface {
	UpsertCustomer(request model.CustomerRequest) (string, string, *errors.Error)
	DeleteCustomer(ID string) (string, *errors.Error)
	Customers() ([]model.Customer, *errors.Error)
	CustomerById(customerID string) (model.Customer, *errors.Error)
	CustomerRating(customerID, startDate, endDate string) ([]model.Rating, *errors.Error)
	CustomerStats(customerID, startDate, endDate string) (model.Stats, *errors.Error)

	UpsertOffice(ctx context.Context, request model.OfficeRequest) (string, *errors.Error)
	DeleteOffice(id string) (string, *errors.Error)
	DeleteOfficesByCustomer(customerId string) (string, *errors.Error)
	OfficesByCustomer(customerId string) ([]model.Office, *errors.Error)
	OfficesByIds(ids ...string) ([]model.Office, *errors.Error)
	OfficesByTag(tag string, townId string, lat, lon float64) ([]model.Office, *errors.Error)
	OfficesAppointmentStream(ctx context.Context, request model.StreamRequest) ([]model.Office, *errors.Error)
	OfficeRating(officeID, startDate, endDate string) ([]model.Rating, *errors.Error)

	UpsertService(ctx context.Context, request model.ServiceRequest) (string, *errors.Error)
	DeleteService(id string) (string, *errors.Error)
	DeleteServicesByCustomer(customerId string) (string, *errors.Error)
	ServicesByCustomer(customerId string) ([]model.Service, *errors.Error)
	ServicesByOffice(officeId string) ([]model.Service, *errors.Error)

	Logos() []model.Logo
	CreateOrder(ctx context.Context, request model.OrderRequest) *errors.Error
	CustomerOrders(ctx context.Context, customerID string) ([]model.Order, *errors.Error)
	AllOrders(ctx context.Context, startDate, endDate time.Time) ([]model.Order, *errors.Error)
	UpdateAvailableAppointment(customerID string, value int) *errors.Error
}

type customerService struct {
	repository  repository.CustomerRepository
	authService service.AuthService
	mailEngine  engine.MessageEngine
}

func NewCustomerService(repo repository.CustomerRepository, auth service.AuthService) CustomerService {
	return InitWithEngine(repo, auth, nil)
}

func InitWithEngine(repo repository.CustomerRepository, auth service.AuthService, engine engine.MessageEngine) CustomerService {
	return &customerService{
		repository:  repo,
		authService: auth,
		mailEngine:  engine}
}
