package repository

import (
	"gitlab.com/meeting-master/sdk/connector/pgsql"
	"gitlab.com/meeting-master/sdk/customer/model"
	"gitlab.com/meeting-master/sdk/errors"
	"time"
)

type CustomerRepository interface {
	UpsertCustomer(customer model.CustomerRequest) (string, string, string, *errors.Error)
	DeleteCustomer(id string) (string, *errors.Error)
	Customers() ([]CustomerResult, *errors.Error)
	CustomerById(customerID string) ([]CustomerResult, *errors.Error)
	CustomersByTag(tag, townId string) ([]CustomerTagResult, *errors.Error)
	CustomerAppointments(customerID string, startDate, endDate time.Time) ([]StatResult, *errors.Error)
	CustomerConsumedAppointments(customerID string, startDate, endDate time.Time) ([]StatResult, *errors.Error)
	CustomerCanceledAppointments(customerID string, startDate, endDate time.Time) ([]StatResult, *errors.Error)

	UpsertOffice(office OfficeDB) (string, *errors.Error)
	DeleteOffice(id string) (string, *errors.Error)
	DeleteOfficesByCustomer(customerId string) (string, *errors.Error)
	OfficesByCustomer(predicate string) ([]OfficeDB, *errors.Error)
	OfficesByIds(ids ...string) ([]OfficeDB, *errors.Error)

	UpsertService(service model.ServiceRequest) (string, *errors.Error)
	DeleteService(id string) (string, *errors.Error)
	DeleteServicesByCustomer(customerId string) (string, *errors.Error)
	ServicesByCustomer(predicate string) ([]ServiceResult, *errors.Error)
	ServicesByOffice(predicate string) ([]ServiceResult, *errors.Error)

	//retrieve appointments for given customer id between given dates
	AppointmentStreams(customerId string, startDate, endDate time.Time) ([]AppointmentStreamInfo, *errors.Error)

	OfficeRating(officeID string, startDate, endDate time.Time) ([]RatingResult, *errors.Error)
	CustomerRating(customerID string, startDate, endDate time.Time) ([]RatingResult, *errors.Error)

	//retrieve customer logo list
	Logos() []LogoResult

	//billing
	GetPlanDetail(code string) (PlanResult, *errors.Error)
	MakeOrder(order OrderInfo) *errors.Error
	CustomerOrders(customerID string) ([]OrderInfo, *errors.Error)
	AllOrders(startDate, endDate time.Time) ([]OrderInfo, *errors.Error)
	UpdateValue(customerID string, value int) *errors.Error
}

type repository struct {
	db *pgsql.DB
}

func NewCustomerRepository(db *pgsql.DB) CustomerRepository {
	return repository{db: db}
}
