package model

type Country struct {
	ID        string `json:"id"`
	Indicator string    `json:"indicator"`
	Name      string `json:"name"`
	IsoCode   string `json:"isoCode"`
	Towns     []Town `json:"towns"`
}

type Town struct {
	ID          string  `json:"id"`
	Name        string  `json:"name"`
	Longitude   float64 `json:"longitude"`
	Latitude    float64 `json:"latitude"`
	CountryID   string  `json:"countryId"`
	CountryName string  `json:"countryName"`
}

type District struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	TownID   string `json:"townId"`
	TownName string `json:"townName"`
}

type Location struct {
	ID       string `json:"id" db:"id"`
	Location string `json:"location" db:"location"`
	Country  string `json:"country" db:"code"`
}
