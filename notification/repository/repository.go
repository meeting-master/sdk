package repository

import (
	"gitlab.com/meeting-master/sdk/connector/pgsql"
	"gitlab.com/meeting-master/sdk/errors"
	"time"
)

type NotificationRepository interface {
	DayMessages()  ([]DayNotification, *errors.Error)
	HourMessages()  ([]DayNotification, *errors.Error)
	Announces(startDate time.Time)  ([]DayNotification, *errors.Error)
}

type repository struct {
	db *pgsql.DB
}

func NewNotificationRepository(db *pgsql.DB) NotificationRepository {
	return repository{db: db}
}

