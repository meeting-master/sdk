package model

import "time"

type Pub struct {
	ID         string    `json:"id"`
	Title      string    `json:"title"`
	Body       string    `json:"body"`
	Price      float32   `json:"price"`
	Published  bool      `json:"published"`
	Pictures   []string  `json:"pictures"`
	OwnerID    string    `json:"ownerId"`
	Details    string    `json:"details"`
	DistrictID string    `json:"districtId"`
	Location   string    `json:"location"`
	ExpireAt   time.Time `json:"expireAt"`
	CreatedAt  time.Time `json:"createdAt"`
}
