package repository

const (

	insertRating = `INSERT INTO common.rating(
							appointment_id,
							rating,
							comment)
					VALUES(	:appointmentid,
							:rating,
							:comment)
							RETURNING id`
	customerRating = `SELECT c.id, c.rating, c.total_rating
						from common.customer c
						inner join common.office o on c.id = o.customer_id
						inner join common.time_slots ts on o.id = ts.office_id
						inner join common.appointment a on ts.id = a.time_slot_id
						where a.id::text = $1 AND a.canceled_at ISNULL`

	updateCustomerRating = `UPDATE common.customer SET rating = :rating, total_rating = total_rating + 1 WHERE id = :id`


)
