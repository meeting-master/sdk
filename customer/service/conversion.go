package service

import (
	"gitlab.com/meeting-master/sdk/customer/model"
	"gitlab.com/meeting-master/sdk/customer/repository"
)

func officeDBToOffice(rawDatas []repository.OfficeDB) []model.Office {

	offices := make([]model.Office, len(rawDatas))
	for index, rawData := range rawDatas {
		offices[index] = model.Office{
			Id:            rawData.Id,
			Alias:         rawData.Alias,
			Name:          rawData.Name,
			StreetNumber:  rawData.StreetNumber,
			StreetName:    rawData.StreetName,
			ZipCode:       rawData.ZipCode,
			PhoneNumber:   rawData.PhoneNumber,
			WebSite:       rawData.WebSite,
			Email:         rawData.Email,
			Longitude:     rawData.Longitude,
			Latitude:      rawData.Latitude,
			Localization:  geoPointConverter(rawData.Latitude, rawData.Longitude),
			DistrictId:    rawData.DistrictId,
			CustomerId:    rawData.CustomerId,
			CustomerName:  rawData.CustomerName,
			CustomerAlias: rawData.CustomerAlias,
			IsArchived:    rawData.IsArchived,
			Location:      rawData.Location,
			ServiceIDs:    rawData.ServiceIDs,
		}
	}
	return offices
}


func geoPointConverter(latitude float64, longitude float64) model.GeoPoint {
	return model.GeoPoint{
		Lon: longitude,
		Lat: latitude,
	}
}


func officeToOfficeDB(rawData model.OfficeRequest) repository.OfficeDB {
		return repository.OfficeDB{
			Id:            rawData.Id,
			Alias:         rawData.Alias,
			Name:          rawData.Name,
			StreetNumber:  rawData.StreetNumber,
			StreetName:    rawData.StreetName,
			ZipCode:       rawData.ZipCode,
			PhoneNumber:   rawData.PhoneNumber,
			WebSite:       rawData.WebSite,
			Email:         rawData.Email,
			Longitude:     rawData.Longitude,
			Latitude:      rawData.Latitude,
			DistrictId:    rawData.DistrictId,
			CustomerId:    rawData.CustomerId,
			IsArchived:    rawData.IsArchived,
			ServiceIDs:    rawData.ServiceIDs,
		}
}