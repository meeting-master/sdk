package engine

import (
	"gitlab.com/meeting-master/sdk/errors"
	"gitlab.com/meeting-master/sdk/notification/model"
	"gitlab.com/meeting-master/sdk/notification/repository"
)

type NotificationEngine interface {
	DayMessages() ([]model.Message, *errors.Error)
	HourMessages() ([]model.Message, *errors.Error)
	Announces() ([]model.Message, *errors.Error)
}

type engine struct {
	repository repository.NotificationRepository
}

func Init(repo repository.NotificationRepository) NotificationEngine {
	return &engine{repository: repo}
}