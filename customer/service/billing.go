package service

import (
	"context"
	appContext "gitlab.com/meeting-master/sdk/context"
	"gitlab.com/meeting-master/sdk/customer/model"
	"gitlab.com/meeting-master/sdk/customer/repository"
	"gitlab.com/meeting-master/sdk/errors"
	"gitlab.com/meeting-master/sdk/utils"
	"time"
)

func (cs *customerService) AllOrders(ctx context.Context, startDate, endDate time.Time) ([]model.Order, *errors.Error) {
	if  !cs.authService.CanManage(ctx) {
		return nil, errors.Unauthorized()
	}
	if startDate.IsZero() || endDate.IsZero() || startDate.After(endDate) {
		return nil, errors.InvalidDateFormat()
	}
	data, err := cs.repository.AllOrders(startDate, endDate)
	if err != nil {
		return nil, err
	}

	res := make([]model.Order, len(data))
	for k, v := range data {
		res[k] = orderInfoToOrder(v)
	}

	return res, nil
}

func (cs *customerService) CustomerOrders(ctx context.Context, customerID string) ([]model.Order, *errors.Error) {
	if customerID == "" {
		return nil, errors.InvalidRequestData()
	}

	if  !cs.authService.CanManage(ctx) {
		return nil, errors.Unauthorized()
	}

	data, err := cs.repository.CustomerOrders(customerID)
	if err != nil {
		return nil, err
	}

	res := make([]model.Order, len(data))
	for k, v := range data {
		res[k] = orderInfoToOrder(v)
	}

	return res, nil
}


func (cs *customerService) CreateOrder(ctx context.Context, request model.OrderRequest) *errors.Error {
	if !request.IsValid() {
		return errors.InvalidRequestData()
	}

	if request.IsAdmin && !cs.authService.CanManage(ctx) {
		return errors.Unauthorized()
	}

	ctxValue := appContext.ContextKeys(ctx)
	if ctxValue.UserId != "" {
		request.UserID = ctxValue.UserId
	}

	plan, err := cs.repository.GetPlanDetail(request.PlanCode)
	if err != nil {
		return err
	}

	if plan.Code == "" {
		return nil
	}

	order := repository.OrderInfo{
		Id:         utils.GenerateCode(true),
		Value:      plan.Value,
		Price:      plan.Price,
		CustomerID: request.CustomerID,
		CreatedBy:  request.UserID,
		PlanCode:   plan.Code,
	}

	return cs.repository.MakeOrder(order)
}

func (cs *customerService) UpdateAvailableAppointment(customerID string, value int) *errors.Error {
	if customerID == "" || value <= 0 {
		return errors.InvalidRequestData()
	}

	return cs.repository.UpdateValue(customerID, value)
}


func orderInfoToOrder (info repository.OrderInfo) model.Order {
	return model.Order{
		Id:           info.Id,
		Value:        info.Value,
		Price:        info.Price,
		CreatedAt:    info.CreatedAt,
		CustomerID:   info.CustomerID,
		CustomerName: info.CustomerName,
		CreatedBy:    info.CreatedBy,
		PlanCode:     info.PlanCode,
	}
}