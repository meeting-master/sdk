package service

import (
	"context"
	"gitlab.com/meeting-master/sdk/auth/service"
	"gitlab.com/meeting-master/sdk/calendar/model"
	"gitlab.com/meeting-master/sdk/calendar/repository"
	"gitlab.com/meeting-master/sdk/errors"
	"gitlab.com/meeting-master/sdk/messaging/engine"
	"time"
)

//the context may have all needed keys to perform authorization operation
//see context package for keys

//function are sorted alphabetically
type CalendarService interface {
	//cancel an appointment
	//id: appointment id
	//owner: appointment owner set while creating
	//An appointment can be canceled by the owner or authorized user
	CancelAppointment(ctx context.Context, id, owner string, ) *errors.Error

	//consume an appointment
	//id: appointment id
	//An appointment can be set consumed by the owner or authorized user
	ConsumeAppointment(ctx context.Context, id string, ) *errors.Error

	//Appointment creation
	//return id of created appointment or error
	CreateAppointment(ctx context.Context, request model.AppointmentRequest) (string, *errors.Error)

	//time slot creation for a service(mandatory)
	//if provided userid will be linked to the created slots
	//for more detail see the request struct
	//return nil if achieved without error
	CreateTimeSlot(ctx context.Context, request model.TimeSlotRequest) *errors.Error

	//define schedule for an item (a combination of office and service)
	InsertSchedule(ctx context.Context, request model.ScheduleRequest) *errors.Error

	//define schedule for an item (a combination of office and service)
	UpdateSchedule(ctx context.Context, request model.ScheduleUpdate) ([]model.ScheduleResponse, *errors.Error)

	//update or cancel a time slot
	//update will change scheduled appointments
	//cancel will invalidate the time slot
	UpdateTimeSlot(ctx context.Context, request model.TimeSlotUpdateRequest) *errors.Error

	TimeSlotsByDateRange(officeID, serviceID string, startDate, endDate time.Time) ([]model.TimeSlot, *errors.Error)

	TimeSlotsByDates(officeID, serviceID string,  dates ...time.Time) ([]model.TimeSlot, *errors.Error)

	//return schedule config for a given item
	Schedule(ctx context.Context, officeID, serviceID string) ([]model.ScheduleResponse, *errors.Error)

	//return ts generation history
	CalendarCreationHistory(officeID, serviceID string) ([]model.CalendarHistory, *errors.Error)

	//return a calendar
	Calendar(request model.CalendarRequest) ([]model.Calendar, *errors.Error)

	//for a given end user id, retrieve his appointment's summaries
	AppointmentHistory(ownerID string)([]model.AppointmentSummary, *errors.Error)

	TimeSlotAppointments(timeSlotIDs ...string)([]model.Appointment, *errors.Error)
	//insert rating for appointment service
	InsertRating(request model.RatingRequest) (string, *errors.Error)
	//insert rating for appointment service
	InsertDevice(request model.DeviceRequest) *errors.Error
}
type calendarService struct {
	repository  repository.CalendarRepository
	authService service.AuthService
	mailEngine  engine.MessageEngine
}


func NewCalendarService(repo repository.CalendarRepository, auth service.AuthService) CalendarService {
	return InitWithEngine(repo, auth, nil)
}

func InitWithEngine(repo repository.CalendarRepository, auth service.AuthService, engine engine.MessageEngine) CalendarService {
	return &calendarService{
		repository:  repo,
		authService: auth,
		mailEngine:  engine}
}
