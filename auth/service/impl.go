package service

import (
	"context"
	"gitlab.com/meeting-master/sdk/auth/model"
	"gitlab.com/meeting-master/sdk/errors"
	"log"
	"os"

	appContext "gitlab.com/meeting-master/sdk/context"
)

const (
	MANAGE  = "MA"
	OFFICE  = "OM"
	SERVICE = "SM"
	ADMIN   = "AD"
	PARTNER = "PA"
)

func (as authService) CanManage(ctx context.Context) bool {
	ctxValues := appContext.ContextKeys(ctx)

	if ctxValues.UserName == os.Getenv("SU") {
		return true
	}

	if !as.isActive(ctxValues.UserId) {
		return false
	}
	privileges, err := as.privileges(ctxValues.AdminUser, ctxValues.UserId, ctxValues.ResourceId)
	if err != nil {
		log.Println(err)
		return false
	}

	for _, pv := range privileges {
		if pv.AccountId == ctxValues.ResourceId && (pv.RoleCode == MANAGE || pv.RoleCode == ADMIN) {
			return true
		}

	}
	return false
}

func (as authService) CanCreateOffice(ctx context.Context) bool {
	return as.CanManage(ctx)
}

func (as authService) CanCreateService(ctx context.Context) bool {
	ctxValues := appContext.ContextKeys(ctx)

	if ctxValues.UserName == os.Getenv("SU") {
		return true
	}
	if !as.isActive(ctxValues.UserId) {
		return false
	}
	privileges, err := as.privileges(ctxValues.AdminUser, ctxValues.UserId, ctxValues.ResourceId)

	if err != nil {
		log.Println(err)
		return false
	}
	for _, pv := range privileges {
		if pv.AccountId == ctxValues.ResourceId && pv.RoleCode == MANAGE {
			return true
		}
		if pv.AccountId == ctxValues.ResourceId && pv.OfficeId == ctxValues.OfficeId && pv.RoleCode == OFFICE {
			return true
		}

	}
	return false
}

func (as authService) CanCreateCalendar(ctx context.Context) bool {
	ctxValues := appContext.ContextKeys(ctx)
	if ctxValues.UserName == os.Getenv("SU") {
		return true
	}
	if !as.isActive(ctxValues.UserId) {
		return false
	}

	privileges, err := as.privileges(ctxValues.AdminUser, ctxValues.UserId, ctxValues.ResourceId)

	if err != nil {
		log.Println(err)
		return false
	}
	for _, pv := range privileges {
		if pv.AccountId == ctxValues.ResourceId && pv.RoleCode == MANAGE {
			return true
		}
		if pv.AccountId == ctxValues.ResourceId && pv.OfficeId == ctxValues.OfficeId && pv.RoleCode == OFFICE {
			return true
		}

		if pv.AccountId == ctxValues.ResourceId && pv.OfficeId == ctxValues.OfficeId && pv.ServiceId == ctxValues.ServiceId && pv.RoleCode == SERVICE {
			return true
		}
	}
	return false
}

func (as authService) Privileges(ctx context.Context, userID string) ([]model.Privilege, *errors.Error) {
	if !as.CanManage(ctx) {
		return nil, nil
	}
	ctxValues := appContext.ContextKeys(ctx)

	return as.privileges(ctxValues.ResourceId == "", userID, ctxValues.ResourceId)
}

func (as authService) isActive(userID string) bool {
	user := as.repository.UserByID(userID)
	return user.IsActive
}

func (as authService) privileges(isAdmin bool, userId, accountId string) ([]model.Privilege, *errors.Error) {
	if isAdmin && accountId != "" {
		return nil, nil
	}
	res, err := as.repository.UserPrivileges(isAdmin, userId, accountId)
	if err != nil {
		return nil, err
	}
	pvs := make([]model.Privilege, len(res))
	for k, v := range res {
		pvs[k] = model.Privilege{
			RoleCode:  v.RoleCode,
			AccountId: v.AccountId.String,
			OfficeId:  v.OfficeId.String,
			ServiceId: v.ServiceId.String,
		}
	}
	return pvs, nil
}
