package repository

import (
	"fmt"
	"gitlab.com/meeting-master/sdk/calendar/model"
	"gitlab.com/meeting-master/sdk/errors"
	"log"
)


func (r repository) InsertSchedule(request []ScheduleDB) *errors.Error {
	if len(request) == 0 {
		return errors.InvalidRequestData()
	}
	_, err := r.db.NamedExec(insertSchedule, request)
	if err != nil {
		return errors.DBError(err)
	}
	return nil
}

func (r repository) ItemSchedule(officeID, serviceID string) ([]ScheduleDB, *errors.Error) {
	fmt.Printf("ItemSchedule %s %s\n", officeID, serviceID)
	if officeID == "" ||  serviceID == ""{
		return nil, errors.InvalidRequestData()
	}
	var result []ScheduleDB
	if err := r.db.Select(&result, getSchedule, officeID, serviceID); err != nil {
		log.Println(err)
		return nil, errors.DBError(err)
	}
	return result, nil
}

func (r repository) UpdateSchedule(request []ScheduleDB, officeID, serviceID string) *errors.Error {
	if len(request) == 0 {
		return errors.InvalidRequestData()
	}
	tx := r.db.MustBegin()
	tx.MustExec(deleteSchedule, officeID, serviceID)
	_, err := tx.NamedExec(insertSchedule, request)
	if err != nil {
		_ = tx.Rollback()
		return errors.DBError(err)
	}
	if err := tx.Commit(); err != nil {
		return errors.DBError(err)
	}
	return nil
}



func (r repository) Calendar(request model.CalendarRequest) ([]CalendarResult, *errors.Error) {
	var result []CalendarResult
	err := r.db.Select(&result, calendar, request.StartDate, request.EndDate, request.OfficeID, request.ServiceID)
	return result, errors.DBError(err)
}


