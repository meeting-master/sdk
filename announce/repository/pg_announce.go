package repository

import (
	"gitlab.com/meeting-master/sdk/errors"
	"strings"
	"time"
)

func (r repository) Upsert(request AnnounceDBRequest) (string, *errors.Error) {
	query := insertAnnounce
	if request.ID != "" {
		query = updateAnnounce
	}

	row, err := r.db.NamedQuery(query, request)

	if err != nil {
		return "", errors.DBError(err)
	}
	var insertID string
	if row.Next() {
		row.Scan(&insertID)
	}
	return insertID, errors.DBError(err)
}

func (r repository) Delete(announceID string) *errors.Error {
	if announceID == "" {
		return errors.InvalidRequestData()
	}
	_, err := r.db.Exec(deleteAnnounce, announceID)
	if err != nil {
		return errors.DBError(err)
	}

	return nil
}

func (r repository) AnnounceById(announceID string) (AnnounceDB, *errors.Error) {
	if announceID == "" {
		return AnnounceDB{}, errors.InvalidRequestData()
	}
	var res []AnnounceDB

	err := r.db.Select(&res, getAnnounceById, announceID)
	if err != nil {
		return AnnounceDB{}, errors.DBError(err)
	}
	if len(res) == 0 {
		return AnnounceDB{}, nil
	}
	return res[0], nil
}

func (r repository) AnnouncesByOwner(ownerID string) ([]AnnounceDB, *errors.Error) {
	if ownerID == "" {
		return nil, errors.InvalidRequestData()
	}
	var res []AnnounceDB

	err := r.db.Select(&res, getAnnouncesByOwner, ownerID)
	if err != nil {
		return nil, errors.DBError(err)
	}

	return res, nil
}

/*
the use of the expression | is to set pg to search tags by OR operator
so to do it, we join tags by the |
*/
func (r repository) AnnouncesByTag(tags, townID string) ([]AnnounceDB, *errors.Error) {
	var err error
	var res []AnnounceDB
	if tags == "" && townID == "" {
		start := time.Now().UTC().AddDate(0, 0, -7)
		err = r.db.Select(&res, getAnnouncesSince, start)
	}

	if tags == "" && townID != "" {
		err = r.db.Select(&res, getAnnouncesByTown, townID)
	}

	if tags != "" {
		tagValues := strings.Join(strings.Split(tags, " "), string('|'))
		if len(tags) != 0 && townID == "" {
			err = r.db.Select(&res, getAnnouncesByTag, tagValues)
		}

		if len(tags) != 0 && townID != "" {
			err = r.db.Select(&res, getAnnouncesByTagAndTown, tagValues, townID)
		}
	}

	if err != nil {
		return nil, errors.DBError(err)
	}

	return res, nil
}

func (r repository) AnnouncesSince(date time.Time) ([]AnnounceDB, *errors.Error) {
	if date.IsZero() {
		return nil, errors.InvalidRequestData()
	}
	var res []AnnounceDB

	err := r.db.Select(&res, getAnnouncesSince, date)
	if err != nil {
		return nil, errors.DBError(err)
	}

	return res, nil
}

func (r repository) AnnouncesToValidate() ([]AnnounceDB, *errors.Error) {
	var res []AnnounceDB

	err := r.db.Select(&res, getAnnouncesToValidate)
	if err != nil {
		return nil, errors.DBError(err)
	}

	return res, nil
}

func (r repository) Validate(valid bool, announceID, comment, validatedBy string) *errors.Error {
	status := "1"
	if !valid {
		status = "-1"
	}
	args := map[string]interface{}{
		"status":     status,
		"announceID": announceID,
		"comment":    comment,
		"userID":     validatedBy,
	}
	if _, err := r.db.NamedQuery(validateAnnounce, args); err != nil {
		return errors.DBError(err)
	}
	return nil
}
