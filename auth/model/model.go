package model

type Privilege struct {
	RoleCode  string  `json:"roleCode"`
	AccountId string  `json:"accountId"`
	OfficeId  string `json:"officeId"`
	ServiceId string `json:"serviceId"`
}
