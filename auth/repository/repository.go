package repository

import (
	"gitlab.com/meeting-master/sdk/connector/pgsql"
	"gitlab.com/meeting-master/sdk/errors"
	ur "gitlab.com/meeting-master/sdk/user/repository"
)


type AuthRepository interface {
	UserPrivileges(isAdmin bool, userID, AccountId string) ([]PrivilegeResult, *errors.Error)
	UserByID(userID string) ur.UserResult
}

type repository struct {
	db *pgsql.DB
}

func NewAuthRepository(db *pgsql.DB) AuthRepository {
	return repository{db: db}
}