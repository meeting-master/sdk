package utils

import (
	"gitlab.com/meeting-master/sdk/errors"
	"golang.org/x/crypto/bcrypt"
	"math/rand"
	"time"
)

func HashPassword(password string) (string, *errors.Error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return string(bytes), errors.DBError(err)
}

func GenerateCode(max bool) int {
	rand.Seed(time.Now().UnixNano())
	if max {
		return 10000000 + rand.Intn(99999999-10000000)
	}
	return 10000 + rand.Intn(99999-10000)
}