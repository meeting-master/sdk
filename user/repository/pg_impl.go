package repository

import (
	"database/sql"
	"log"

	"github.com/google/uuid"

	"gitlab.com/meeting-master/sdk/errors"
	"gitlab.com/meeting-master/sdk/user/model"
)

func (r repository) Active(userId string) *errors.Error {
	if len(userId) == 0 {
		return errors.InvalidRequestData()
	}
	_, err := r.db.Exec(activeUser, userId)
	return errors.DBError(err)
}

func (r repository) ChangePassword(userId, newPassword string) *errors.Error {
	if len(userId) == 0 || len(newPassword) == 0 {
		return errors.InvalidRequestData()
	}
	_, err := r.db.Exec(changePassword, userId, newPassword)
	return errors.DBError(err)
}

func (r repository) CreateUser(request UserCreateRequest) (string, *errors.Error) {
	if !request.IsValid() {
		return "", errors.InvalidRequestData()
	}
	row, err := r.db.NamedQuery(insertNewUser, request)
	if err != nil {
		return "", errors.DBError(err)
	}
	var ID uuid.UUID
	if row.Next() {
		row.Scan(&ID)
	}

	return ID.String(), errors.DBError(err)
}

func (r repository) EndSession(sessionId string) {
	return
}

func (r repository) GrantUser(request model.GrantRequest) *errors.Error {
	pv := make([]PrivilegeRequest, len(request.Privileges))
	if _, err := uuid.Parse(request.UserId); err != nil {
		return errors.InvalidRequestData()
	}
	userId := sql.NullString{
		String: request.UserId,
		Valid:  true,
	}
	for i, p := range request.Privileges {
		of := sql.NullString{
			String: "",
			Valid:  false,
		}
		if v, err := uuid.Parse(p.OfficeId); err == nil {
			of.String = v.String()
			of.Valid = true
		}
		ser := sql.NullString{
			String: "",
			Valid:  false,
		}
		if v, err := uuid.Parse(p.ServiceId); err == nil {
			ser.String = v.String()
			ser.Valid = true
		}

		if _, err := uuid.Parse(p.ResourceId); err != nil {
			return errors.InvalidRequestData()
		}

		resID := sql.NullString{
			String: p.ResourceId,
			Valid:  true,
		}

		pr := PrivilegeRequest{
			RoleCode:   p.RoleCode,
			ResourceId: resID,
			OfficeId:   of,
			ServiceId:  ser,
			UserId:     userId,
		}
		pv[i] = pr
	}
	_, err := r.db.NamedQuery(InsertPrivilege, pv)
	return errors.DBError(err)
}

func (r repository) InsertActivationCode(userId, code string) *errors.Error {
	if len(userId) == 0 {
		return errors.InvalidRequestData()
	}
	_, err := r.db.Exec(activationCode, userId, code)
	return errors.DBError(err)
}

func (r repository) Login(request model.LoginRequest) LoginResult {
	if !request.HasValidLogin() {
		return LoginResult{}
	}
	var result []LoginResult
	if err := r.db.Select(&result, userByName, request.UserName); err != nil {
		return LoginResult{}
	}
	if len(result) == 0 {
		return LoginResult{}
	}
	return result[0]
}

func (r repository) StartSession(sessionId string) *errors.Error {
	return nil
}

func (r repository) UserByID(userId string) (UserResult, *errors.Error) {
	var result []UserResult
	if err := r.db.Select(&result, UserByID, userId); err != nil {
		log.Println(err)
		return UserResult{}, errors.DBError(err)
	}
	if len(result) == 0 {
		return UserResult{}, nil
	}
	return result[0], nil
}

func (r repository) Users(resourceId string) ([]UserResult, *errors.Error) {
	var result []UserResult
	var err error
	if resourceId != "" {
		err = r.db.Select(&result, usersByResource, resourceId)
	} else {
		err = r.db.Select(&result, usersWithoutResource)
	}

	return result, errors.DBError(err)
}


func (r repository) ChangeUserInfo(request UserCreateRequest) *errors.Error {
	_, err := r.db.NamedExec(changeUser, request)
	return errors.DBError(err)
}