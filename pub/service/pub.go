package service

import (
	"context"
	"gitlab.com/meeting-master/sdk/errors"
	"gitlab.com/meeting-master/sdk/pub/model"
)

func (p pubService) Upsert(request model.PubRequest) (string, *errors.Error) {
	if !request.IsValid() {
		return "", errors.InvalidRequestData()
	}
	dbRequest := pubRequestToPubDB(request)
	return p.repo.Upsert(dbRequest)
}

func (p pubService) Delete(pubID, ownerID string) *errors.Error {
	panic("implement me")
}

func (p pubService) PubById(ID string) (model.Pub, *errors.Error) {
	panic("implement me")
}

func (p pubService) PubsByOwner(ownerID string) ([]model.Pub, *errors.Error) {
	panic("implement me")
}

func (p pubService) PubsToValidate(ctx context.Context) ([]model.Pub, *errors.Error) {
	panic("implement me")
}

func (p pubService) Validate(ctx context.Context, request model.ValidationRequest) ([]model.Pub, *errors.Error) {
	panic("implement me")
}

func (p pubService) PubsForLocations(locationIDs ...string) ([]model.Pub, *errors.Error) {
	panic("implement me")
}