package repository

const (
	insertAnnounce = `INSERT INTO common.announce (
			 title, body, price, owner_id, published, pictures, expire_at, details, district_id)
	VALUES (:title, :body, :price, :owner_id, :published, :pictures, :expire_at, :details, :district_id) 
	RETURNING id`

	updateAnnounce = `UPDATE common.announce SET 
								title = :title, 
								body = :body, 
								price = :price,
								published = :published,
								pictures = :pictures,
								district_id = :district_id
						WHERE id = :id`


	getAnnounceById = `SELECT a.id, a.title, a.body, a.price, a.owner_id, a.published, a.pictures, a.expire_at, a.created_at, a.district_id, CONCAT(t.name, '-', d.name) as location
						FROM common.announce a
						LEFT JOIN location.district d ON d.id = a.district_id
						LEFT JOIN location.town t on t.id = d.town_id 
						WHERE a.id = $1`

	getAnnouncesByOwner = `SELECT id, title, body, price, owner_id, details, published, pictures, expire_at, created_at, district_id
							FROM common.announce
							WHERE owner_id = $1 AND deleted_at ISNULL`

	deleteAnnounce = `UPDATE common.announce SET deleted_at = NOW() WHERE id = $1`


	getAnnouncesByTag = `SELECT a.id, a.title, a.price, a.owner_id, a.published, a.pictures, a.expire_at, a.created_at, CONCAT(t.name, '-', d.name) as location
						FROM common.announce a
						LEFT JOIN location.district d ON d.id = a.district_id
						LEFT JOIN location.town t on t.id = d.town_id 
						WHERE to_tsvector(coalesce(title,'') || ' ' || coalesce(body,'')) @@ to_tsquery($1)
							AND published = true
							AND a.deleted_at ISNULL`

	getAnnouncesByTagAndTown = `SELECT a.id, a.title, a.price, a.owner_id, a.published, a.pictures, a.expire_at, a.created_at, CONCAT(t.name, '-', d.name) as location
						FROM common.announce a
						LEFT JOIN location.district d ON d.id = a.district_id
						LEFT JOIN location.town t on t.id = d.town_id 
						WHERE to_tsvector(coalesce(title,'') || ' ' || coalesce(body,'')) @@ to_tsquery($1)
							AND published = true
							AND t.id = $2 AND a.deleted_at ISNULL`

	getAnnouncesByTown = `SELECT a.id, a.title, a.price, a.owner_id, a.published, a.pictures, a.expire_at, a.created_at, CONCAT(t.name, '-', d.name) as location
						FROM common.announce a
						LEFT JOIN location.district d ON d.id = a.district_id
						LEFT JOIN location.town t on t.id = d.town_id 
						WHERE 
							 published = true
							AND t.id = $1 AND a.deleted_at ISNULL`


	getAnnouncesSince = `SELECT a.id, a.title, a.price, a.owner_id, a.published, a.pictures, a.expire_at, a.created_at, CONCAT(t.name, '-', d.name) as location
				FROM common.announce a
				LEFT JOIN location.district d ON d.id = a.district_id
				LEFT JOIN location.town t on t.id = d.town_id 
				WHERE created_at >= $1 AND published = true AND a.deleted_at ISNULL`

)

