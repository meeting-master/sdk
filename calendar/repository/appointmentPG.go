package repository

import (
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	repository2 "gitlab.com/meeting-master/sdk/customer/repository"
	"log"

	"gitlab.com/meeting-master/sdk/calendar/model"
	"gitlab.com/meeting-master/sdk/errors"
)

func (r repository) AppointmentById(ID string) (AppointmentInfo, *errors.Error) {
	if ID == "" {
		return AppointmentInfo{}, errors.MissingMandatory()
	}
	var result []AppointmentInfo
	if err := r.db.Select(&result, getAppointmentById, ID); err != nil {
		return AppointmentInfo{}, errors.DBError(err)
	}
	if len(result) == 0 {
		return AppointmentInfo{}, nil
	}
	return result[0], nil
}

func (r repository) AppointmentsByTimeSlot(timeSlotIDs ...string) ([]AppointmentInfo, *errors.Error) {
	if len(timeSlotIDs) == 0 {
		return nil, errors.MissingMandatory()
	}
	var result []AppointmentInfo
	query, args, err := sqlx.In(getAppointmentByTsId, timeSlotIDs)

	if err != nil {
		return nil, errors.DBError(err)
	}

	query = r.db.Rebind(query)
	err = r.db.Select(&result, query, args...)
	return result, errors.DBError(err)
}

func (r repository) AppointmentSummary(userID string) ([]AppointmentSummaryInfo, *errors.Error) {
	if userID == "" {
		return nil, errors.MissingMandatory()
	}
	var result []AppointmentSummaryInfo
	if err := r.db.Select(&result, appointmentHistory, userID); err != nil {
		return nil, errors.DBError(err)
	}

	return result, nil
}

func (r repository) CancelAppointment(appointmentID, timeSlotID, canceler, customerID string, place int) *errors.Error {
	if appointmentID == "" || len(timeSlotID) == 0 || len(canceler) == 0 || place == 0 {
		return errors.MissingMandatory()
	}

	tx := r.db.MustBegin()
	_, err := tx.Exec(cancelAppointment, canceler, appointmentID)
	if err != nil {
		log.Println(err)
		return errors.DBError(err)
	}
	tsArg := map[string]interface{}{
		"id":    timeSlotID,
		"value": -place,
	}
	_, err = tx.NamedExec(updateTsByAppointment, tsArg)
	if err != nil {
		log.Println(err)
		_ = tx.Rollback()
		return errors.DBError(err)
	}

	_, err = tx.Exec(repository2.UpdateValue, place, customerID)
	if err != nil {
		log.Println(err)
		_ = tx.Rollback()
		return errors.DBError(err)
	}

	if err := tx.Commit(); err != nil {
		log.Println(err)
		return errors.DBError(err)
	}

	return nil
}

func (r repository) ConsumeAppointment(ID string) *errors.Error {
	if ID == "" {
		return errors.MissingMandatory()
	}

	_, err := r.db.Exec(consumedAppointment, ID)
	if err != nil {
		log.Println(err)
		return errors.DBError(err)
	}

	return nil
}

func (r repository) InsertAppointment(userID, customerID string, request model.AppointmentRequest) (string, *errors.Error) {
	if !request.IsValid() {
		return "", errors.InvalidRequestData()
	}
	arg := AppointmentInfo{
		TimeSlotId: request.TimeSlotId,
		Place:      request.PlaceNumber,
		Details:    request.Details,
		CreatedBy:  userID,
		Owner:      request.Owner,
	}

	tx := r.db.MustBegin()
	row, err := tx.NamedQuery(insertAppointment, arg)
	if err != nil {
		return "", errors.DBError(err)
	}
	var ID uuid.UUID
	if row.Next() {
		_ = row.Scan(&ID)
	}
	_ = row.Close()
	tsArg := map[string]interface{}{
		"id":    request.TimeSlotId,
		"value": request.PlaceNumber,
	}
	_, err = tx.NamedExec(updateTsByAppointment, tsArg)
	if err != nil {
		_ = tx.Rollback()
		return "", errors.DBError(err)
	}

	_, err = tx.Exec(repository2.UpdateValue, -request.PlaceNumber, customerID)
	if err != nil {
		log.Println(err)
		_ = tx.Rollback()
		return "", errors.DBError(err)
	}

	if err := tx.Commit(); err != nil {
		return "", errors.DBError(err)
	}

	return ID.String(), nil
}

func (r repository) UpsertRating(request model.RatingRequest, customerID string, customerRating float32) (string, *errors.Error) {
	if !request.IsValid() || customerID == "" {
		return "", errors.InvalidRequestData()
	}
	tx := r.db.MustBegin()
	row, err := tx.NamedQuery(insertRating, request)
	if err != nil {
		return "", errors.DBError(err)
	}
	var ID uuid.UUID
	if row.Next() {
		_ = row.Scan(&ID)
	}
	_ = row.Close()
	args := map[string]interface{}{
		"id":     customerID,
		"rating": customerRating,
	}
	_, err = tx.NamedExec(updateCustomerRating, args)
	if err != nil {
		_ = tx.Rollback()
		return "", errors.DBError(err)
	}

	if err := tx.Commit(); err != nil {
		return "", errors.DBError(err)
	}

	return ID.String(), nil
}

func (r repository) CustomerRating(appointmentID string) (CustomerRatingResult, *errors.Error) {
	if appointmentID == "" {
		return CustomerRatingResult{}, errors.MissingMandatory()
	}
	var result []CustomerRatingResult
	if err := r.db.Select(&result, customerRating, appointmentID); err != nil {
		return CustomerRatingResult{}, errors.DBError(err)
	}
	if len(result) == 0 {
		return CustomerRatingResult{}, nil
	}
	return result[0], nil
}

func (r repository) AppointmentNotification(ID string) ([]AppointmentSummaryInfo, *errors.Error) {
	if ID == "" {
		return nil, errors.MissingMandatory()
	}
	var result []AppointmentSummaryInfo
	if err := r.db.Select(&result, appointmentNotif, ID); err != nil {
		log.Println(err)
		return nil, errors.DBError(err)
	}

	return result, nil
}
