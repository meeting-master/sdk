// Code generated by mockery v1.0.0. DO NOT EDIT.

package mocks

import errors "gitlab.com/meeting-master/sdk/errors"
import mock "github.com/stretchr/testify/mock"
import model "gitlab.com/meeting-master/sdk/user/model"
import repository "gitlab.com/meeting-master/sdk/user/repository"

// UserRepository is an autogenerated mock type for the UserRepository type
type UserRepository struct {
	mock.Mock
}

// Active provides a mock function with given fields: UserId
func (_m *UserRepository) Active(UserId string) *errors.Error {
	ret := _m.Called(UserId)

	var r0 *errors.Error
	if rf, ok := ret.Get(0).(func(string) *errors.Error); ok {
		r0 = rf(UserId)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*errors.Error)
		}
	}

	return r0
}

// ChangePassword provides a mock function with given fields: userId, newPassword
func (_m *UserRepository) ChangePassword(userId string, newPassword string) *errors.Error {
	ret := _m.Called(userId, newPassword)

	var r0 *errors.Error
	if rf, ok := ret.Get(0).(func(string, string) *errors.Error); ok {
		r0 = rf(userId, newPassword)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*errors.Error)
		}
	}

	return r0
}

// CreateUser provides a mock function with given fields: request
func (_m *UserRepository) CreateUser(request model.UserCreateRequest) (string, *errors.Error) {
	ret := _m.Called(request)

	var r0 string
	if rf, ok := ret.Get(0).(func(model.UserCreateRequest) string); ok {
		r0 = rf(request)
	} else {
		r0 = ret.Get(0).(string)
	}

	var r1 *errors.Error
	if rf, ok := ret.Get(1).(func(model.UserCreateRequest) *errors.Error); ok {
		r1 = rf(request)
	} else {
		if ret.Get(1) != nil {
			r1 = ret.Get(1).(*errors.Error)
		}
	}

	return r0, r1
}

// EndSession provides a mock function with given fields: sessionId
func (_m *UserRepository) EndSession(sessionId string) {
	_m.Called(sessionId)
}

// GrantUser provides a mock function with given fields: request
func (_m *UserRepository) GrantUser(request model.GrantRequest) *errors.Error {
	ret := _m.Called(request)

	var r0 *errors.Error
	if rf, ok := ret.Get(0).(func(model.GrantRequest) *errors.Error); ok {
		r0 = rf(request)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*errors.Error)
		}
	}

	return r0
}

// InsertActivationCode provides a mock function with given fields: userId, code
func (_m *UserRepository) InsertActivationCode(userId string, code string) *errors.Error {
	ret := _m.Called(userId, code)

	var r0 *errors.Error
	if rf, ok := ret.Get(0).(func(string, string) *errors.Error); ok {
		r0 = rf(userId, code)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*errors.Error)
		}
	}

	return r0
}

// Login provides a mock function with given fields: request
func (_m *UserRepository) Login(request model.LoginRequest) repository.LoginResult {
	ret := _m.Called(request)

	var r0 repository.LoginResult
	if rf, ok := ret.Get(0).(func(model.LoginRequest) repository.LoginResult); ok {
		r0 = rf(request)
	} else {
		r0 = ret.Get(0).(repository.LoginResult)
	}

	return r0
}

// StartSession provides a mock function with given fields: sessionId
func (_m *UserRepository) StartSession(sessionId string) *errors.Error {
	ret := _m.Called(sessionId)

	var r0 *errors.Error
	if rf, ok := ret.Get(0).(func(string) *errors.Error); ok {
		r0 = rf(sessionId)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*errors.Error)
		}
	}

	return r0
}

// UserByID provides a mock function with given fields: userId
func (_m *UserRepository) UserByID(userId string) repository.UserResult {
	ret := _m.Called(userId)

	var r0 repository.UserResult
	if rf, ok := ret.Get(0).(func(string) repository.UserResult); ok {
		r0 = rf(userId)
	} else {
		r0 = ret.Get(0).(repository.UserResult)
	}

	return r0
}
