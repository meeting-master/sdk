package service

import (
	"context"
	"gitlab.com/meeting-master/sdk/customer/model"
	"gitlab.com/meeting-master/sdk/customer/repository"
	"gitlab.com/meeting-master/sdk/errors"
	"math"
	"time"

	"github.com/umahmood/haversine"
)

const dateLayout = "2006-01-02"

func (cs *customerService) UpsertOffice(ctx context.Context, request model.OfficeRequest) (string, *errors.Error) {
	if !request.IsValid() {
		return "", errors.InvalidRequestData()
	}
	if !cs.authService.CanCreateOffice(ctx) {
		return "", errors.Unauthorized()
	}
	return cs.repository.UpsertOffice(officeToOfficeDB(request))
}

func (cs *customerService) DeleteOffice(id string) (string, *errors.Error) {
	return cs.repository.DeleteOffice(id)
}

func (cs *customerService) DeleteOfficesByCustomer(customerId string) (string, *errors.Error) {
	return cs.repository.DeleteOfficesByCustomer(customerId)
}

func (cs *customerService) OfficesByCustomer(customerId string) ([]model.Office, *errors.Error) {
	if len(customerId) == 0 {
		return nil, errors.InvalidRequestData()
	}

	result, err := cs.repository.OfficesByCustomer(customerId)
	if err != nil {
		return nil, err
	}
	if result == nil || len(result) == 0 {
		return nil, nil
	}
	return officeDBToOffice(result), nil
}

func (cs *customerService) OfficesByIds(ids ...string) ([]model.Office, *errors.Error) {
	if len(ids) == 0 {
		return nil, errors.InvalidRequestData()
	}

	result, err := cs.repository.OfficesByIds(ids...)
	if err != nil {
		return nil, err
	}
	if result == nil || len(result) == 0 {
		return nil, nil
	}
	return officeDBToOffice(result), nil
}

func (cs *customerService) OfficesByTag(tag string, townId string, lat, lon float64) ([]model.Office, *errors.Error) {
	tagResult, err := cs.repository.CustomersByTag(tag, townId)
	if err != nil {
		return nil, err
	}
	if len(tagResult) == 0 {
		return nil, nil
	}
	ids := make([]string, len(tagResult))
	resultMap := make(map[string]repository.CustomerTagResult)
	for k, v := range tagResult {
		ids[k] = v.OfficeId
		resultMap[v.OfficeId] = v
	}

	offices, err := cs.OfficesByIds(ids...)
	if err != nil {
		return nil, err
	}

	for i, office := range offices {
		customer := resultMap[office.Id]
		office.Rating = customer.Rating
		office.LogoUrl = customer.LogoUrl.String
		office.Activities = customer.Activities
		office.Distance = Distance(lat, lon, office.Latitude, office.Longitude)
		offices[i] = office
	}
	return offices, nil
}

func Distance(sLat, sLon, dLat, dLon float64) float64 {
	if sLat == 0 || sLon == 0 || dLat == 0 || dLon == 0 {
		return 0
	}
	s := haversine.Coord{Lat: sLat, Lon: sLon}
	d := haversine.Coord{Lat: dLat, Lon: dLon}
	_, km := haversine.Distance(s, d)
	return math.Round(km)
}

func (cs *customerService) OfficesAppointmentStream(_ context.Context, request model.StreamRequest) ([]model.Office, *errors.Error) {
	if !request.IsValid() {
		return nil, errors.InvalidRequestData()
	}
	startDate, err := time.Parse(time.RFC3339, request.StartDate)
	if err != nil {
		return nil, errors.InvalidDateFormat()
	}

	endDate, err := time.Parse(time.RFC3339, request.EndDate)
	if err != nil {
		return nil, errors.InvalidDateFormat()
	}

	if startDate.After(endDate) {
		return nil, errors.InvalidDateOrder()
	}

	res, er := cs.repository.AppointmentStreams(request.CustomerId, startDate, endDate)
	if er != nil {
		return nil, er
	}
	if len(res) == 0 {
		return nil, nil
	}
	resOffices := make(map[string][]repository.AppointmentStreamInfo)
	for _, value := range res {
		officeId := value.OfficeId
		current := resOffices[officeId]
		current = append(current, value)
		resOffices[officeId] = current
	}
	results := make([]model.Office, len(resOffices))
	i := 0
	for k, v := range resOffices {
		_, stats := convertToService(v)
		results[i] = model.Office{
			Id: k,
			//Services:   services,
			Statistics: stats,
		}
		i++
	}
	return results, nil
}

func convertToService(raws []repository.AppointmentStreamInfo) ([]model.Service, map[string]int) {
	rawServices := make(map[string][]repository.AppointmentStreamInfo)
	stats := make(map[string]int)
	for _, v := range raws {
		stats["count"] = stats["count"] + 1
		serviceId := v.ServiceId
		current := rawServices[serviceId]
		current = append(current, v)
		rawServices[serviceId] = current
		if v.CanceledAt != nil && v.ConsumedAt == nil {
			stats["canceled"] = stats["canceled"] + 1
		}
		if v.ConsumedAt != nil && v.CanceledAt == nil {
			stats["consumed"] = stats["consumed"] + 1
		}
	}

	services := make([]model.Service, len(rawServices))
	serviceIndex := 0
	for _, v := range rawServices {
		dates := make(map[string]int)
		for i := range v {
			cDate := v[i].CreatedAt.Format(dateLayout)
			dates[cDate] = dates[cDate] + 1
		}
		streams := make([]model.AppointmentStream, len(dates))
		j := 0
		for d, count := range dates {
			streams[j] = model.AppointmentStream{
				Date:  d,
				Count: count,
			}
			j++
		}
		services[serviceIndex] = model.Service{
			Id:                 v[0].ServiceId,
			Name:               v[0].ServiceName,
			AppointmentStreams: streams,
		}
		serviceIndex++
	}
	return services, stats
}

func (cs *customerService) OfficeRating(officeID, startDate, endDate string) ([]model.Rating, *errors.Error) {
	if officeID == "" {
		return nil, errors.InvalidRequestData()
	}
	rawEndDate := time.Now().UTC()
	if ed, err := time.Parse(time.RFC3339, endDate); err == nil {
		rawEndDate = ed
	}
	rawStartDate := time.Now().UTC().AddDate(0, 0, -15)
	if sd, err := time.Parse(time.RFC3339, startDate); err == nil {
		rawStartDate = sd
	}
	if rawStartDate.After(rawEndDate) {
		return nil, errors.InvalidDateOrder()
	}

	result, err := cs.repository.OfficeRating(officeID, rawStartDate, rawEndDate)
	if err != nil {
		return nil, err
	}

	res := make([]model.Rating, len(result))
	for k, v := range result {
		res[k] = model.Rating{
			Id:        v.Id,
			Value:     v.Rating,
			Comment:   v.Comment,
			CreatedAt: v.CreatedAt,
		}
	}
	return res, nil
}
