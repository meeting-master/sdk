package model

import (
	"os"
	"regexp"
)

type UserCreateRequest struct {
	UserName    string `json:"userName"`
	Password    string `json:"password"`
	PhoneNumber string `json:"phoneNumber"`
	FirstName   string `json:"firstName"`
	LastName    string `json:"lastName"`
	Language    string `json:"language"`
	AdminUser   bool   `json:"adminUser"`
}

type Privilege struct {
	RoleCode   string `json:"roleCode"`
	ResourceId string `json:"resourceId"`
	OfficeId   string `json:"officeId"`
	ServiceId  string `json:"serviceId"`
}

type GrantRequest struct {
	UserId     string      `json:"userId"`
	Privileges []Privilege `json:"privileges"`
}

var rxEmail = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

func (us *UserCreateRequest) IsValid() bool {
	//if us.Email == os.Getenv("SU") {
	//	return true
	//}

	if  !rxEmail.MatchString(us.UserName) || len(us.Password) == 0 || len(us.FirstName) == 0 || len(us.LastName) == 0 {
		return false
	}

	return true
}

type LoginRequest struct {
	UserName    string `json:"userName"`
	Password    string `json:"password"`
	NewPassword string `json:"newPassword"`
	IsAdmin     bool   `json:"isAdmin"`
}

func (us *LoginRequest) HasValidLogin() bool {
	if us.UserName == os.Getenv("SU") {
		return true
	}
	return us.UserName != "" && us.Password != ""
}

func (us *LoginRequest) HasValidPasswordChange() bool {
	return us.NewPassword != "" && us.Password != ""
}

type UserUpdateRequest struct {
	ID          string `json:"id"`
	UserName    string `json:"userName"`
	PhoneNumber string `json:"phoneNumber"`
	FirstName   string `json:"firstName"`
	LastName    string `json:"lastName"`
}

func (up *UserUpdateRequest) IsValid() bool {

	return  rxEmail.MatchString(up.UserName) && up.FirstName != ""

}