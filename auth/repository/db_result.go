package repository

import (
	"database/sql"
	"github.com/google/uuid"
)

type PrivilegeResult struct {
	RoleCode  string         `db:"role_code"`
	AccountId sql.NullString `db:"account_id"`
	OfficeId  sql.NullString `db:"office_id"`
	ServiceId sql.NullString `db:"service_id"`
}

type PrivilegeRequest struct {
	RoleID         int       `json:"role_id"`
	ResourceTypeID int       `json:"resource_type_id"`
	ResourceID     uuid.UUID `json:"resource_id"`
	UserID         uuid.UUID `json:"user_id"`
}
