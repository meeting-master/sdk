package service

import (
	"gitlab.com/meeting-master/sdk/pub/model"
	"gitlab.com/meeting-master/sdk/pub/repository"
)
/*
func announceDbToAnnounce(raws ...repository.AnnounceDB) []model.Announce {
	results := make([]model.Announce, len(raws))
	for k, raw := range raws {
		results[k] = model.Announce{
			ID:         raw.ID,
			Title:      raw.Title,
			Body:       raw.Body.String,
			Price:      raw.Price,
			Published:  raw.Published,
			Pictures:   raw.Pictures,
			OwnerID:    raw.OwnerID,
			Details:    raw.Details.String,
			DistrictID: raw.DistrictId.String,
			ExpireAt:   raw.ExpireAt,
			CreatedAt:  raw.CreatedAt,
			Location:   raw.Location.String,
		}
	}
	return results
}
*/
func pubRequestToPubDB(pub model.PubRequest) repository.DBRequest {
	result := repository.DBRequest{
		ID:         pub.ID,
		Url:        pub.Url,
		Picture:    pub.Picture,
		OwnerID:    pub.OwnerID,
		LocationID: pub.LocationID,
		StartAt:    pub.StartDate,
		EndAt:      pub.StartDate.AddDate(0, 0, pub.Duration),
	}

	return result
}
