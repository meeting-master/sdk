package repository

const (
	insertAnnounce = `INSERT INTO common.announce (
			 title, body, price, owner_id, published, pictures, expire_at, details, district_id, phone_number)
	VALUES (:title, :body, :price, :owner_id, :published, :pictures, :expire_at, :details, :district_id, :phone_number) 
	RETURNING id`

	updateAnnounce = `UPDATE common.announce SET 
								title = :title, 
								body = :body, 
								price = :price,
								published = :published,
								pictures = :pictures,
								district_id = :district_id,
								phone_number = :phone_number,
								status = '0'
						WHERE id = :id`

	getAnnounceById = `SELECT a.id, a.title, a.body, a.price, a.owner_id, a.phone_number, a.published, a.pictures, a.expire_at, a.created_at, a.district_id, CONCAT(t.name, '-', d.name) as location
						FROM common.announce a
						LEFT JOIN location.district d ON d.id = a.district_id
						LEFT JOIN location.town t on t.id = d.town_id 
						WHERE a.id = $1`

	getAnnouncesByOwner = `SELECT a.id, a.title, a.body, a.price, a.owner_id, a.phone_number, a.details, a.published, a.pictures, a.expire_at, created_at, a.district_id, a.status, a.comment, CONCAT(t.name, '-', d.name) as location
							FROM common.announce a
							LEFT JOIN location.district d ON d.id = a.district_id
						    LEFT JOIN location.town t on t.id = d.town_id 
							WHERE a.owner_id = $1 AND a.deleted_at ISNULL`

	deleteAnnounce = `UPDATE common.announce SET deleted_at = NOW() WHERE id = $1`

	getAnnouncesByTag = `SELECT a.id, a.title, a.price, a.owner_id, a.published, a.phone_number, a.pictures, a.expire_at, a.created_at, CONCAT(t.name, '-', d.name) as location
						FROM common.announce a
						LEFT JOIN location.district d ON d.id = a.district_id
						LEFT JOIN location.town t on t.id = d.town_id 
						WHERE to_tsvector(coalesce(title,'') || ' ' || coalesce(body,'')) @@ to_tsquery($1)
							AND published = true
							AND a.deleted_at ISNULL
							AND a.status = '1' AND a.expire_at >= NOW()`

	getAnnouncesByTagAndTown = `SELECT a.id, a.title, a.price, a.owner_id, a.published, a.phone_number, a.pictures, a.expire_at, a.created_at, CONCAT(t.name, '-', d.name) as location
						FROM common.announce a
						LEFT JOIN location.district d ON d.id = a.district_id
						LEFT JOIN location.town t on t.id = d.town_id 
						WHERE to_tsvector(coalesce(title,'') || ' ' || coalesce(body,'')) @@ to_tsquery($1)
							AND published = true
							AND t.id = $2 AND a.deleted_at ISNULL
							AND a.status = '1' AND a.expire_at >= NOW()`

	getAnnouncesByTown = `SELECT a.id, a.title, a.price, a.owner_id, a.published, a.pictures, a.phone_number, a.expire_at, a.created_at, CONCAT(t.name, '-', d.name) as location
						FROM common.announce a
						LEFT JOIN location.district d ON d.id = a.district_id
						LEFT JOIN location.town t on t.id = d.town_id 
						WHERE 
							 published = true
							AND t.id = $1 AND a.deleted_at ISNULL
							AND a.status = '1' AND a.expire_at >= NOW()`

	getAnnouncesSince = `SELECT a.id, a.title, a.price, a.owner_id, a.published, a.pictures, a.phone_number, a.expire_at, a.created_at, CONCAT(t.name, '-', d.name) as location
				FROM common.announce a
				LEFT JOIN location.district d ON d.id = a.district_id
				LEFT JOIN location.town t on t.id = d.town_id 
				WHERE created_at >= $1 AND published = true AND a.deleted_at ISNULL
						AND a.status = '1'`

	getAnnouncesToValidate = `SELECT a.id, a.title, a.price, a.body, a.published, a.pictures
				FROM common.announce a
				WHERE  a.deleted_at ISNULL AND a.status = '0'`

	validateAnnounce = `UPDATE common.announce 
						SET status = :status, comment = :comment, validated_at = NOW(), validated_by = :userID
						WHERE  id = :announceID`
)
