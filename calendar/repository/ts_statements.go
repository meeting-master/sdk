package repository

const (

	insertTs = `INSERT INTO common.time_slots(office_id, service_id, scheduled_appointments, start_time, end_time, user_id, created_by)
					VALUES(:office_id, :service_id, :scheduled_appointments, :start_time, :end_time, :user_id, :created_by)`

	insertHistory = `INSERT INTO common.calendar_history(office_id, service_id, start_date, end_date, created_by)
					VALUES(:office_id, :service_id, :start_date, :end_date, :created_by)`

	getTsById = `SELECT ts.id, ts.scheduled_appointments, ts.consumed_appointments, ts.start_time, ts.user_id, c.id as customer_id 
					FROM common.time_slots ts
					INNER JOIN common.service s ON ts.service_id = s.id
					INNER JOIN common.customer c ON s.customer_id = c.id
					WHERE ts.id::TEXT = $1`

	updateTs = `UPDATE common.time_slots SET scheduled_appointments = :value, last_updated_by = :user WHERE id = :id`

	cancelTs = `UPDATE common.time_slots SET is_canceled = true, last_updated_by = :user WHERE id = :id`

	countTimeSlots = `SELECT count(id) FROM common.time_slots
						WHERE office_id = $1 AND service_id = $2
						AND is_archived = false AND is_canceled = false
						AND start_time between $3 and $4`

	getHistory = `SELECT *  FROM common.calendar_history WHERE office_id = $1 AND service_id = $2`

	getTimeSlotsByRange = `SELECT ts.id, ts.start_time, ts.end_time, ts.scheduled_appointments,
								ts.consumed_appointments, ts.user_id
							FROM common.time_slots ts
							WHERE ts.is_archived = false AND ts.is_canceled = false
							AND  ts.office_id = $1 AND ts.service_id = $2
							AND ts.start_time::DATE between $3::DATE and $4::DATE
							ORDER BY ts.start_time`

	getTimeSlotsByDates = `SELECT ts.id, ts.start_time, ts.end_time, ts.scheduled_appointments,
								ts.consumed_appointments, ts.user_id
							FROM common.time_slots ts
							WHERE ts.is_archived = false AND ts.is_canceled = false
							AND  ts.office_id = $1 AND ts.service_id = $2
							AND ts.start_time::DATE = ANY ($3)
							ORDER BY ts.start_time`
)
