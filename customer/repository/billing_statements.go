package repository

const (
	insertOrder = `INSERT INTO common.order (id, value, price, plan_code, created_by, customer_id)
					VALUES (:id, :value, :price, :plan_code, :created_by, :customer_id);`

	getPlan = `SELECT code, name, value, price, started_at, end_at FROM common.plan WHERE code = $1`

	getCustomerOrders = `SELECT o.id, o.value, o.price, o.plan_code, o.created_at, o.customer_id, 
								c.name as "customer_name", concat(u.first_name , ' ', u.last_name ) as created_by 
						FROM common."order" o
						INNER JOIN common.customer c on c.id = o.customer_id 
						INNER JOIN common.user u on u.id = o.created_by  
						WHERE o.customer_id = $1`

	getOrdersByDate = `SELECT o.id, o.value, o.price, o.plan_code, o.created_at, o.customer_id, 
								c.name as "customer_name", concat(u.first_name , ' ', u.last_name ) as created_by 
						FROM common."order" o
						INNER JOIN common.customer c on c.id = o.customer_id 
						INNER JOIN common.user u on u.id = o.created_by  
						WHERE o.created_at BETWEEN $1 AND $2`

	UpdateValue = `UPDATE common.customer SET value = value + $1 WHERE id = $2`
)
