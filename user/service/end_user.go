package service

import (
	"gitlab.com/meeting-master/sdk/errors"
	"gitlab.com/meeting-master/sdk/user/model"
)

func (us userService) UpsertEndUser(request model.EndUser) *errors.Error {
	if request.ID == "" || request.Detail == "" {
		return errors.InvalidRequestData()
	}
	return us.repository.InsertEndUser(request.ID, request.Detail)
}

func (us userService) EndUser(ID string) (model.EndUser, *errors.Error) {
	if ID == "" {
		return model.EndUser{}, errors.InvalidRequestData()
	}
	detail, err := us.repository.EndUserByID(ID)

	return model.EndUser{ID: ID, Detail: detail}, err
}
