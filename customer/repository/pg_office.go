package repository

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/meeting-master/sdk/customer/model"
	"gitlab.com/meeting-master/sdk/errors"
	"time"
)

func (r repository) UpsertOffice(office OfficeDB) (string, *errors.Error) {
	var upsertOffice = insertNewOffice
	if office.Id != "" {
		upsertOffice = updateOffice
	}
	row, err := r.db.NamedQuery(upsertOffice, office)
	if err != nil {
		return "", errors.DBError(err)
	}
	var insertID string
	if row.Next() {
		row.Scan(&insertID)
	}
	return insertID, errors.DBError(err)
}

func (r repository) DeleteOffice(id string) (string, *errors.Error) {
	request := model.DeleteRequest{id}
	row, err := r.db.NamedQuery(deleteOffice, request)
	if err != nil {
		return "", errors.DBError(err)
	}
	var deletedId string
	if row.Next() {
		row.Scan(&deletedId)
	}
	return deletedId, errors.DBError(err)
}

func (r repository) DeleteOfficesByCustomer(customerId string) (string, *errors.Error) {
	request := model.DeleteRequest{customerId}
	row, err := r.db.NamedQuery(deleteOfficesByCustomer, request)
	if err != nil {
		return "", errors.DBError(err)
	}
	var insertID string
	if row.Next() {
		row.Scan(&insertID)
	}
	return insertID, errors.DBError(err)
}

func (r repository) OfficesByCustomer(predicate string) ([]OfficeDB, *errors.Error) {
	if len(predicate) == 0 {
		return nil, errors.InvalidRequestData()
	}
	var result []OfficeDB

	err := r.db.Select(&result, officesByCustomer, predicate)
	return result, errors.DBError(err)
}

func (r repository) OfficesByIds(ids ...string) ([]OfficeDB, *errors.Error) {
	if len(ids) == 0 {
		return nil, errors.InvalidRequestData()
	}
	var result []OfficeDB
	query, args, err := sqlx.In(officesByIds, ids)

	if err != nil {
		return nil, errors.DBError(err)
	}

	query = r.db.Rebind(query)
	err = r.db.Select(&result, query, args...)
	return result, errors.DBError(err)
}

func (r repository) OfficeRating(officeID string, startDate, endDate time.Time) ([]RatingResult, *errors.Error) {
	if officeID == "" || startDate.IsZero() || endDate.IsZero() || startDate.After(endDate){
		return nil, errors.InvalidRequestData()
	}

	var result []RatingResult
	if err := r.db.Select(&result, officeRating, officeID, startDate, endDate); err != nil {
		return nil, errors.DBError(err)
	}

	return result, nil
}
