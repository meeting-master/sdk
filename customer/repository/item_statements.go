package repository

const (
	insertNewItem = `INSERT INTO common.item (office_id, service_id) VALUES (:officeid, :serviceid)`

	deleteItem = `UPDATE common.item SET is_archived = true WHERE id = :id`

	deleteItemsByOffice = `UPDATE common.item SET is_archived = true WHERE office_id = :id`
)
