package repository

import "time"

type ScheduleDB struct {
	Id        string  `db:"id"`
	OfficeID  string  `db:"office_id"`
	ServiceID string  `db:"service_id"`
	WorkDay   int     `db:"work_day"`
	StartTime int     `db:"start_time"`
	EndTime   int     `db:"end_time"`
	Duration  int     `db:"duration"`
	Place     int     `db:"place"`
	CreatedBy *string `db:"created_by"`
}

type CalendarHistoryDB struct {
	Id        string    `db:"id"`
	OfficeID  string    `db:"office_id"`
	ServiceID string    `db:"service_id"`
	CreatedBy string    `db:"created_by"`
	CreatedAt time.Time `db:"created_at"`
	StartDate time.Time `db:"start_date"`
	EndDate   time.Time `db:"end_date"`
}

type TimeSlotInfo struct {
	OfficeID  string    `db:"office_id"`
	ServiceID string    `db:"service_id"`
	Scheduled int       `db:"scheduled_appointments"`
	StartTime time.Time `db:"start_time"`
	EndTime   time.Time `db:"end_time"`
	UserId    string    `db:"user_id"`
	CreatedBy string    `db:"created_by"`
}

type AppointmentInfo struct {
	Id         string  `db:"id"`
	TimeSlotId string  `db:"time_slot_id"`
	Place      int     `db:"place_number"`
	Details    string  `db:"details"`
	CreatedBy  string  `db:"created_by"`
	Owner      string  `db:"owner"`
	CreatedAt  string  `db:"created_at"`
	ConsumedAt *string `db:"consumed_at"`
	CustomerID string  `db:"customer_id"`
}
