package repository

import (
	"gitlab.com/meeting-master/sdk/errors"
	"gitlab.com/meeting-master/sdk/messaging/model"
)

func (r repository) InsertMessage(message model.Message) (string, *errors.Error) {
	row, err := r.db.NamedQuery(insertMessage, message)
	if err != nil {
		return "", errors.DBError(err)
	}
	var ID string
	if row.Next() {
		row.Scan(&ID)
	}
	return ID, errors.DBError(err)
}

func (r repository) MessagesToSent() ([]model.Message, *errors.Error) {
	var msg []model.Message

	err := r.db.Select(&msg, getMessagesToSent)
	return msg, errors.DBError(err)
}

func (r repository) UpdateMessage(messages []model.Message) *errors.Error {
	var err error
	for _, message := range messages {
		_, err = r.db.NamedExec(update, message)
	}
	return errors.DBError(err)
}
