package repository

const (
	insertMessage = `INSERT INTO common.message(message_from, message_to, subject, body)
					VALUES(:message_from, :message_to, :subject, :body) 
					RETURNING id`

	getDayNotification = `select ts.start_time, c.name, ud.token, ud.lang
						from common.time_slots ts
						inner join common.office o on ts.office_id = o.id 
						inner join common.customer c on o.customer_id = c.id 
						inner join common.appointment a on a.time_slot_id = ts.id 
						inner join common.user_device ud on ud.user_id = a."owner"
						WHERE ts.start_time::DATE = 'tomorrow'::DATE`

	getHourNotification = `select ts.start_time, c.name, ud.token, ud.lang
						from common.time_slots ts
						inner join common.office o on ts.office_id = o.id 
						inner join common.customer c on o.customer_id = c.id 
						inner join common.appointment a on a.time_slot_id = ts.id 
						inner join common.user_device ud on ud.user_id = a."owner"
						where ts.start_time - NOW() >=  INTERVAL '2 hours'
						and ts.start_time - NOW() <  INTERVAL '3 hours'`

	getAnnounceNotification = `select ud.token, ud.lang
						from common.announce a
						inner join common.user_device ud on ud.user_id = a.owner_id
						where a.status != '0'
						and a.validated_at >= $1`

)
