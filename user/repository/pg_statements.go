package repository

const (
	insertNewUser = `INSERT INTO common.user(
							username,
							phone_number,
							password ,
							first_name,
							last_name,
							created_by,
							activation_code,
							customer_id,
							business_code)
					VALUES(	:username,
							:phonenumber,
							:password ,
							:firstname,
							:lastname,
							:createdby,
							:activationcode,
							:resourceid,
							:businesscode)
					RETURNING id`

	InsertPrivilege = `INSERT INTO common.privilege(
							user_id,
							role_code,
							resource_id,
							office_id,
							service_id)
					VALUES(	:user_id,
							:role_code,
							:resource_id,
							:office_id,
							:service_id)`

	userByName = `SELECT id, password, first_name, last_name, customer_id, active, business_code FROM common.user WHERE username = $1`

	UserByID = `SELECT id, username, first_name, last_name, password, activation_code, active, phone_number FROM common.user WHERE id::TEXT = $1 OR username=$1`

	changePassword = `UPDATE common.user SET  password = $2 WHERE id::TEXT = $1`

	activeUser = `UPDATE common.user SET  active = true WHERE id::TEXT = $1`

	activationCode = `UPDATE common.user SET  activation_code = $2, active = false WHERE id::TEXT = $1`

	UserPrivileges = `SELECT role_code, resource_id as "account_id", office_id, service_id FROM common.privilege p
						WHERE p.user_id::TEXT = $1 and p.resource_id::TEXT = $2`

	AdminPrivileges = `SELECT role_code FROM common.privilege p
						WHERE p.user_id::TEXT = $1`

	usersByResource = `SELECT id, username, first_name, last_name, phone_number, active FROM common.user WHERE customer_id::TEXT = $1`

	usersWithoutResource = `SELECT id, username, first_name, last_name, active, phone_number FROM common.user WHERE customer_id ISNULL`

	changeUser = `UPDATE common.user SET  username = :username, phone_number = :phonenumber, first_name = :firstname, last_name = :lastname
				  WHERE id = :id`

)
