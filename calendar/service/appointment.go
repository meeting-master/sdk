package service

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/meeting-master/sdk/calendar/model"
	"gitlab.com/meeting-master/sdk/calendar/repository"
	appContext "gitlab.com/meeting-master/sdk/context"
	"gitlab.com/meeting-master/sdk/errors"
	mailEngineModel "gitlab.com/meeting-master/sdk/messaging/model"
)

//An appointment can be created or canceled by owner(service customer) or system user
//if system user is provided, it  will be in context values

func (cs calendarService) CancelAppointment(ctx context.Context, id, owner string) *errors.Error {
	if id == "" {
		return errors.MissingMandatory()
	}

	app, err := cs.repository.AppointmentById(id)
	if err != nil {
		return err
	}
	
	if len(app.Id) == 0 || app.Id != id {
		return errors.Unknown()
	}

	if owner != app.Owner && !cs.authService.CanCreateService(ctx) {
		return errors.Unauthorized()
	}
	canceler := owner
	if len(canceler) == 0 {
		canceler = appContext.ContextKeys(ctx).UserId
	}

	err = cs.repository.CancelAppointment(id, app.TimeSlotId, canceler, app.CustomerID, app.Place)
	if err == nil {
		if cs.mailEngine != nil {
			data := cs.notificationInfo(id)
			if data.To != "" {
				_, err = cs.mailEngine.CancelAppointmentNotification(data)
				println(err)
			}

		}
	}

	return err
}

func (cs calendarService) ConsumeAppointment(ctx context.Context, id string) *errors.Error {
	if len(id) == 0 {
		return errors.MissingMandatory()
	}

	app, err := cs.repository.AppointmentById(id)
	if err != nil {
		return err
	}
	if len(app.Id) == 0 || app.Id != id {
		return errors.Unknown()
	}

	if !cs.authService.CanCreateService(ctx) {
		return errors.Unauthorized()
	}

	return cs.repository.ConsumeAppointment(id)
}

func (cs calendarService) CreateAppointment(ctx context.Context, request model.AppointmentRequest) (string, *errors.Error) {
	if !request.IsValid() {
		return "", errors.InvalidRequestData()
	}

	ts, err := cs.repository.TimeSlotById(request.TimeSlotId)
	if err != nil {
		return "", err
	}

	if ts.Id != request.TimeSlotId {
		return "", errors.UnknownData()
	}

	if ts.Consumed >= ts.Scheduled {
		return "", errors.Unavailable()
	}

	if request.PlaceNumber > (ts.Scheduled - ts.Consumed) {
		return "", errors.Enough()
	}

	userId := appContext.ContextKeys(ctx).UserId
	if userId == "" {
		userId = uuid.New().String()
	}

	id, err := cs.repository.InsertAppointment(userId, *ts.CustomerID, request)
	if err != nil {
		return "", err
	}

	if cs.mailEngine != nil {
		data := cs.notificationInfo(id)
		if data.To != "" {
			_, _ = cs.mailEngine.AppointmentNotification(data)
		}
	}

	return id, nil
}

func (cs calendarService) notificationInfo(id string) mailEngineModel.AppointmentNotification {
	app, err := cs.repository.AppointmentNotification(id)
	if err == nil && app != nil && len(app) != 0 {
		info := summaryInfoToSummary(app[0])
		details := model.Details{}
		_ = json.Unmarshal([]byte(info.Details), &details)

		data := mailEngineModel.AppointmentNotification{
			Name:      details.FirstName,
			StartDate: info.StartDate,
			StartHour: info.StartTime,
			EndDate:   info.EndDate,
			EndHour:   info.EndTime,
			Company:   info.CustomerName,
			Service:   info.ServiceName,
			Place:     info.PlaceNumber,
			Address:   fmt.Sprintf("%v %v %v. Tel : %v", info.StreetNumber, info.StreetName, info.ZipCode, info.PhoneNumber),
			To:        details.Email,
		}
		return data
	}
	return mailEngineModel.AppointmentNotification{}
}

func (cs calendarService) AppointmentHistory(ownerID string) ([]model.AppointmentSummary, *errors.Error) {
	if ownerID == "" {
		return nil, errors.InvalidRequestData()
	}
	summary, err := cs.repository.AppointmentSummary(ownerID)
	if err != nil {
		return nil, err
	}

	res := make([]model.AppointmentSummary, len(summary))
	for k, v := range summary {
		res[k] = summaryInfoToSummary(v)
	}

	return res, nil
}

func summaryInfoToSummary(info repository.AppointmentSummaryInfo) model.AppointmentSummary {
	return model.AppointmentSummary{
		CustomerName:  info.CustomerName,
		OfficeId:      info.OfficeId,
		StreetNumber:  info.StreetNumber,
		StreetName:    info.StreetName,
		ZipCode:       info.ZipCode,
		PhoneNumber:   info.PhoneNumber,
		StartTime:     info.StartTime.Format(timeLayout),
		StartDate:     info.StartTime.Format(dateLayout),
		EndTime:       info.EndTime.Time.Format(timeLayout),
		EndDate:       info.EndTime.Time.Format(dateLayout),
		CreatedAt:     info.CreatedAt,
		Owner:         info.Owner,
		AppointmentId: info.AppointmentId,
		PlaceNumber:   info.PlaceNumber,
		IsRated:       info.RatingId != sql.NullString{},
		ServiceName:   info.ServiceName,
		Details:       info.Details,
	}
}

func (cs calendarService) TimeSlotAppointments(timeSlotIDs ...string) ([]model.Appointment, *errors.Error) {
	if len(timeSlotIDs) == 0 {
		return nil, errors.InvalidRequestData()
	}

	res, err := cs.repository.AppointmentsByTimeSlot(timeSlotIDs...)
	if err != nil {
		return nil, err
	}

	if len(res) == 0 {
		return nil, nil
	}

	response := make([]model.Appointment, len(res))

	for k, v := range res {
		response[k] = model.Appointment{
			Id:          v.Id,
			CreatedAt:   v.CreatedAt,
			ConsumedAt:  v.ConsumedAt,
			PlaceNumber: v.Place,
			OwnerId:     v.Owner,
			Details:     v.Details,
			TimeSlotId:  v.TimeSlotId,
		}
	}

	return response, nil
}

func (cs *calendarService) InsertRating(request model.RatingRequest) (string, *errors.Error) {
	if !request.IsValid() {
		return "", errors.InvalidRequestData()
	}
	customerInfo, err := cs.repository.CustomerRating(request.AppointmentId)
	if err != nil {
		return "", err
	}
	if customerInfo.Id == "" {
		return "", nil
	}

	customerNewRating := calculateRating(float32(customerInfo.TotalRating), customerInfo.Rating, request.Rating)
	return cs.repository.UpsertRating(request, customerInfo.Id, customerNewRating)
}

func calculateRating(totalRating, allRating, rating float32) float32 {
	return ((allRating * totalRating) + rating) / (totalRating + 1)
}
