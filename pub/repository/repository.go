package repository

import (
	"gitlab.com/meeting-master/sdk/connector/pgsql"
	"gitlab.com/meeting-master/sdk/errors"
)

type PubRepository interface {
	Upsert(request DBRequest) (string, *errors.Error)
	Delete(ID string) *errors.Error
	PubById(ID string) (DBResponse, *errors.Error)
	PubsByOwner(ownerID string) ([]DBResponse, *errors.Error)
	PubsByLocations(locationIds ...string) ([]DBResponse, *errors.Error)
	PubsForValidation() ([]DBResponse, *errors.Error)
	Validation(valid bool, validatedBy, comment string) *errors.Error
}

type repository struct {
	db *pgsql.DB
}

func NewPubRepository(db *pgsql.DB) PubRepository {
	return repository{db}
}
