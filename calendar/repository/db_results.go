package repository

import (
	"database/sql"
	"time"
)

type CalendarResult struct {
	Id                    string       `db:"id"`
	StartTime             time.Time    `db:"start_time"`
	EndTime               sql.NullTime `db:"end_time"`
	ScheduledAppointments int          `db:"scheduled_appointments"`
	ConsumedAppointments  int          `db:"consumed_appointments"`
	UserId                *string      `db:"user_id"`
	ServiceId             string       `db:"item_id"`
	ServiceName           string       `db:"service_name"`
}

type AppointmentSummaryInfo struct {
	CustomerName  string         `db:"customer_name"`
	OfficeId      string         `db:"office_name"`
	StreetNumber  string         `db:"street_number"`
	StreetName    string         `db:"street_name"`
	ZipCode       string         `db:"zip_code"`
	PhoneNumber   string         `db:"phone_number"`
	StartTime     time.Time      `db:"start_time"`
	EndTime       sql.NullTime   `db:"end_time"`
	CreatedAt     string         `db:"created_at"`
	Owner         string         `db:"owner"`
	AppointmentId string         `db:"appointment_id"`
	PlaceNumber   int            `db:"place_number"`
	RatingId      sql.NullString `db:"rating_id"`
	ServiceName   string         `db:"service_name"`
	Details       string         `db:"details"`
}

type CustomerRatingResult struct {
	Id          string  `db:"id"`
	Rating      float32 `db:"rating"`
	TotalRating int     `db:"total_rating"`
}

type TimeSlotResult struct {
	Id                    string    `db:"id"`
	Scheduled             int       `db:"scheduled_appointments"`
	Consumed              int       `db:"consumed_appointments"`
	StartTime             time.Time `db:"start_time"`
	EndTime               time.Time `db:"end_time"`
	UserId                *string   `db:"user_id"`
	CreatedBy             string    `db:"created_by"`
	CustomerID            *string   `db:"customer_id"`
}
