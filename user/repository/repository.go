package repository

import (
	"gitlab.com/meeting-master/sdk/connector/pgsql"
	"gitlab.com/meeting-master/sdk/errors"
	"gitlab.com/meeting-master/sdk/user/model"
)


type UserRepository interface {
	//User activation
	Active(UserId string) *errors.Error

	InsertActivationCode(userId, code string) *errors.Error

	ChangePassword(userId, newPassword string) *errors.Error

	CreateUser(request UserCreateRequest) (string, *errors.Error)

	ChangeUserInfo(request UserCreateRequest) *errors.Error

	EndSession(sessionId string)

	GrantUser(request model.GrantRequest) *errors.Error

	Login(request model.LoginRequest) LoginResult

	StartSession(sessionId string) *errors.Error

	UserByID(userId string) (UserResult, *errors.Error)

	Users(resourceId string) ([]UserResult, *errors.Error)

	InsertEndUser(ID, detail string)  *errors.Error

	EndUserByID(ID string) (string, *errors.Error)

}

type repository struct {
	db *pgsql.DB
}

func NewUserRepository(db *pgsql.DB) UserRepository {
	return repository{db: db}
}