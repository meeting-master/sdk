package service

import (
	"gitlab.com/meeting-master/sdk/announce/model"
	"gitlab.com/meeting-master/sdk/announce/repository"
	"time"
)

func announceDbToAnnounce(raws ...repository.AnnounceDB) []model.Announce {
	results := make([]model.Announce, len(raws))
	for k, raw := range raws {
		results[k] = model.Announce{
			ID:                raw.ID,
			Title:             raw.Title,
			Body:              raw.Body.String,
			Price:             raw.Price,
			Published:         raw.Published,
			Pictures:          raw.Pictures,
			OwnerID:           raw.OwnerID,
			Details:           raw.Details.String,
			DistrictID:        raw.DistrictId.String,
			ExpireAt:          raw.ExpireAt,
			CreatedAt:         raw.CreatedAt,
			Location:          raw.Location.String,
			Status:            raw.Status,
			ValidationComment: raw.Comment.String,
			PhoneNumber:       raw.PhoneNumber.String,
		}
	}
	return results
}

func announceRequestToAnnounceDB(announce model.AnnounceRequest) repository.AnnounceDBRequest {
	result := repository.AnnounceDBRequest{
		ID:          announce.ID,
		Title:       announce.Title,
		Body:        announce.Body,
		Price:       announce.Price,
		Published:   announce.Published,
		Pictures:    announce.Pictures,
		OwnerID:     announce.OwnerID,
		Details:     announce.Details,
		DistrictId:  announce.DistrictID,
		PhoneNumber: announce.PhoneNumber,
		ExpireAt:    time.Now().UTC().AddDate(0, 0, 30),
	}

	return result
}
