package model

import "time"

type ScheduleResponse struct {
	WorkDay   int `json:"workDay"`
	StartTime int `json:"startTime"`
	EndTime   int `json:"endTime"`
	Duration  int `json:"duration"`
	Place     int `json:"place"`
}

type Calendar struct {
	Date          string     `json:"date"`
	TimeZoneCode  string     `json:"timeZoneCode"`
	IsAvailable   bool       `json:"isAvailable"`
	CalendarSlots []TimeSlot `json:"timeSlots"`
}

type TimeSlot struct {
	Id            string    `json:"id"`
	StartTime     string    `json:"startTime"`
	StartDateTime time.Time `json:"startDateTime"`
	EndTime       string    `json:"endTime"`
	EndDateTime   time.Time `json:"endDateTime"`
	IsAvailable   bool      `json:"isAvailable"`
	IsPassed      bool      `json:"isPassed"`
	Available     int       `json:"available"`
	Consumed      int       `json:"consumed"`
}

type AppointmentSummary struct {
	CustomerName  string `json:"customerName"`
	OfficeId      string `json:"officeName"`
	StreetNumber  string `json:"streetNumber"`
	StreetName    string `json:"streetName"`
	ZipCode       string `json:"zipCode"`
	PhoneNumber   string `json:"phoneNumber"`
	StartTime     string `json:"startTime"`
	StartDate     string `json:"startDate"`
	EndTime       string `json:"endTime"`
	EndDate       string `json:"endDate"`
	CreatedAt     string `json:"createdAt"`
	Owner         string `json:"owner"`
	AppointmentId string `json:"appointmentId"`
	PlaceNumber   int    `json:"placeNumber"`
	IsRated       bool   `json:"isRated"`
	ServiceName   string `json:"serviceName"`
	Details       string `json:"details"`
}

type Appointment struct {
	Id          string  `json:"id"`
	CreatedAt   string  `json:"createdAt"`
	ConsumedAt  *string `json:"consumedAt"`
	PlaceNumber int     `json:"placeNumber"`
	OwnerId     string  `json:"ownerId"`
	Details     string  `json:"details"`
	TimeSlotId  string  `json:"timeSlotId"`
}

type Details struct {
	FirstName   string `json:"firstName"`
	LastName    string `json:"lastName"`
	PhoneNumber string `json:"phoneNumber"`
	Email       string `json:"email"`
}

type CalendarHistory struct {
	Id        string    `json:"id"`
	OfficeID  string    `json:"officeId"`
	ServiceID string    `json:"serviceId"`
	CreatedBy string    `json:"createdBy"`
	CreatedAt time.Time `json:"createdAt"`
	StartDate time.Time `json:"startDate"`
	EndDate   time.Time `json:"endDate"`
}
