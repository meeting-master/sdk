package repository

import (
	"fmt"
	"github.com/lib/pq"
	"gitlab.com/meeting-master/sdk/calendar/model"
	"gitlab.com/meeting-master/sdk/errors"
	"log"
	"time"
)

func (r repository) InsertTimeSlots(ts []TimeSlotInfo, history []CalendarHistoryDB) *errors.Error {
	fmt.Printf("InsertTimeSlots %v %v\n", len(ts), len(history))
	if ts == nil {
		return errors.InvalidRequestData()
	}
	tx := r.db.MustBegin()
	_, err := tx.NamedExec(insertTs, ts)
	if err != nil {
		_ = tx.Rollback()
		return errors.DBError(err)
	}
	_, err = tx.NamedExec(insertHistory, history)
	if err != nil {
		_ = tx.Rollback()
		return errors.DBError(err)
	}
	if err := tx.Commit(); err != nil {
		return errors.DBError(err)
	}
	return nil
}

func (r repository) TimeSlotById(id string) (ts TimeSlotResult, err *errors.Error) {
	if len(id) == 0 {
		err = errors.MissingMandatory()
		return
	}
	var result []TimeSlotResult
	er := r.db.Select(&result, getTsById, id)
	err = errors.DBError(er)
	if result != nil {
		ts = result[0]
	}
	return
}

func (r repository) UpdateTimeSlot(request model.TimeSlotUpdateRequest, userId string) *errors.Error {
	query := updateTs
	args := map[string]interface{}{
		"id":    request.Id,
		"value": request.Value,
		"user":  userId,
	}
	if request.Cancelled {
		query = cancelTs
	}
	_, err := r.db.NamedExec(query, args)
	return errors.DBError(err)
}

func (r repository) CountTimeSlot(officeID, serviceID string, startDate, endDate time.Time) (int, *errors.Error) {
	row, err := r.db.Query(countTimeSlots, officeID, serviceID, startDate, endDate)
	if err != nil {
		return 0, errors.DBError(err)
	}

	var count int
	if row.Next() {
		row.Scan(&count)
	}
	return count, nil
}

func (r repository) CalendarHistory(officeID, serviceID string) ([]CalendarHistoryDB, *errors.Error) {
	if officeID == "" || serviceID == "" {
		return nil, errors.InvalidRequestData()
	}
	var result []CalendarHistoryDB
	if err := r.db.Select(&result, getHistory, officeID, serviceID); err != nil {
		log.Println(err)
		return nil, errors.DBError(err)
	}
	return result, nil
}

func (r repository) TimeSlotsByRange(officeID, serviceID string, startDate, endDate time.Time) ([]TimeSlotResult, *errors.Error) {
	if officeID == "" || serviceID == "" || startDate.IsZero() || endDate.IsZero() || startDate.After(endDate) {
		return nil, errors.InvalidRequestData()
	}
	var result []TimeSlotResult
	if err := r.db.Select(&result, getTimeSlotsByRange, officeID, serviceID, startDate, endDate); err != nil {
		log.Println(err)
		return nil, errors.DBError(err)
	}
	return result, nil
}

func (r repository) TimeSlotsByDates(officeID, serviceID string, dates ...time.Time) ([]TimeSlotResult, *errors.Error) {
	if officeID == "" || serviceID == "" || len(dates) == 0 {
		return nil, errors.InvalidRequestData()
	}
	dateArgs := make(pq.StringArray, len(dates))
	for k, d := range dates {
		dateArgs[k] = d.Format("2006-01-02")
	}
	var result []TimeSlotResult
	if err := r.db.Select(&result, getTimeSlotsByDates, officeID, serviceID, dateArgs); err != nil {
		log.Println(err)
		return nil, errors.DBError(err)
	}
	return result, nil
}
