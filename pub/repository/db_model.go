package repository

import (
	"database/sql"
	"github.com/lib/pq"
	"time"
)

type DBResponse struct {
	ID         string         `db:"id"`
	Title      string         `db:"title"`
	Body       sql.NullString `db:"body"`
	Price      float32        `db:"price"`
	Published  bool           `db:"published"`
	Pictures   pq.StringArray `db:"pictures"`
	OwnerID    string         `db:"owner_id"`
	Details    sql.NullString `db:"details"`
	DistrictId sql.NullString `db:"district_id"`
	Location   sql.NullString `db:"location"`
	ExpireAt   time.Time      `db:"expire_at"`
	CreatedAt  time.Time      `db:"created_at"`
}

type DBRequest struct {
	ID         string    `db:"id"`
	Url        string    `db:"title"`
	Picture    string    `db:"picture"`
	OwnerID    string    `db:"owner"`
	LocationID string    `db:"location_id"`
	StartAt    time.Time `db:"start_date"`
	EndAt      time.Time `db:"end_date"`
}
