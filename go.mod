module gitlab.com/meeting-master/sdk

go 1.13

require (
	github.com/appleboy/go-fcm v0.1.5
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/elastic/go-elasticsearch/v8 v8.0.0-20200322093924-a7287c67b91f
	github.com/google/uuid v1.1.1
	github.com/jmoiron/sqlx v1.2.1-0.20200324155115-ee514944af4b
	github.com/joho/godotenv v1.3.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/lib/pq v1.5.2
	github.com/sethvargo/go-password v0.2.0
	github.com/stretchr/objx v0.1.1 // indirect
	github.com/stretchr/testify v1.4.0
	github.com/umahmood/haversine v0.0.0-20151105152445-808ab04add26
	golang.org/x/crypto v0.0.0-20200406173513-056763e48d71
	google.golang.org/appengine v1.6.5 // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
