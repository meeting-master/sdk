package service

import (
	"context"
	"gitlab.com/meeting-master/sdk/customer/model"
	"gitlab.com/meeting-master/sdk/customer/repository"
	"gitlab.com/meeting-master/sdk/errors"
	"log"
	"os"
	"strings"
	"time"
)

const tagSeparator = "|"

func (cs *customerService) UpsertCustomer(request model.CustomerRequest) (string, string, *errors.Error) {
	if !request.IsValid() {
		return "", "", errors.InvalidRequestData()
	}
	if len(request.Tags) > 0 && !request.IsTagsValid() {
		return "", "", errors.InvalidTags()
	}
	request.TagString = strings.Join(request.Tags, tagSeparator)
	customerID, code, userID, err := cs.repository.UpsertCustomer(request)
	if err != nil {
		return "", "", err
	}
	_ = cs.CreateOrder(context.Background(), model.OrderRequest{
		CustomerID: customerID,
		UserID:     userID,
		PlanCode:   os.Getenv("INIT_PLAN"),
		IsAdmin:    false,
	})
	if cs.mailEngine != nil && code != "" {
		_, err = cs.mailEngine.ActivationMessage(request.Name, request.UserName, code, request.Language)
		if err != nil {
			log.Println(err)
		}
	}

	return customerID, code, nil
}

func (cs *customerService) Customers() ([]model.Customer, *errors.Error) {
	result, err := cs.repository.Customers()
	if err != nil {
		return nil, err
	}
	if result == nil || len(result) == 0 {
		return nil, nil
	}
	return cs.customerResultsToCustomers(result), nil
}

func (cs *customerService) CustomerById(customerID string) (model.Customer, *errors.Error) {
	if customerID == "" {
		return model.Customer{}, errors.InvalidRequestData()
	}

	result, err := cs.repository.CustomerById(customerID)
	if err != nil {
		return model.Customer{}, err
	}
	if result == nil {
		return model.Customer{}, nil
	}
	return cs.customerResultToCustomer(result[0]), nil
}

func (cs *customerService) DeleteCustomer(ID string) (string, *errors.Error) {
	return cs.repository.DeleteCustomer(ID)
}

func (cs *customerService) customerResultsToCustomers(rawDatas []repository.CustomerResult) []model.Customer {
	customers := make([]model.Customer, len(rawDatas))
	for index, rawData := range rawDatas {
		customers[index] = cs.customerResultToCustomer(rawData)
	}
	return customers
}

func (cs *customerService) customerResultToCustomer(rawData repository.CustomerResult) model.Customer {
	return model.Customer{
		Id:          rawData.Id,
		Alias:       rawData.Alias.String,
		Name:        rawData.Name,
		PhoneNumber: rawData.PhoneNumber.String,
		WebSite:     rawData.WebSite.String,
		Rating:      rawData.Rating,
		Tags:        strings.Split(rawData.Tags.String, tagSeparator),
		LogoUrl:     rawData.LogoUrl.String,
		Value:       rawData.Value,
		Activities: rawData.Activities,
	}
}

func (cs *customerService) CustomerRating(customerID, startDate, endDate string) ([]model.Rating, *errors.Error) {
	panic("implement me")
}

func (cs *customerService) Logos() []model.Logo {
	data := cs.repository.Logos()
	if data == nil || len(data) == 0 {
		return nil
	}
	res := make([]model.Logo, len(data))
	for k, v := range data {
		res[k] = model.Logo{Name: v.Name, Url: v.Url}
	}

	return res
}

func (cs *customerService) CustomerStats(customerID, startDate, endDate string) (model.Stats, *errors.Error) {
	res := model.Stats{}
	sd, err := time.Parse(time.RFC3339, startDate)
	if err != nil {
		return res, errors.InvalidDateFormat()
	}

	ed, err := time.Parse(time.RFC3339, endDate)
	if err != nil {
		return res, errors.InvalidDateFormat()
	}

	if sd.After(ed) {
		return res, errors.InvalidDateOrder()
	}

	if result, er := cs.repository.CustomerAppointments(customerID, sd, ed); er == nil {
		values := make([]model.StatValues, len(result))
		for k, v := range result {
			values[k] = model.StatValues{
				OfficeId: v.OfficeID,
				Value:    v.Count,
			}
		}
		res.Appointment = values
	}

	if result, er := cs.repository.CustomerCanceledAppointments(customerID, sd, ed); er == nil {
		values := make([]model.StatValues, len(result))
		for k, v := range result {
			values[k] = model.StatValues{
				OfficeId: v.OfficeID,
				Value:    v.Count,
			}
		}
		res.Canceled = values
	}

	if result, er := cs.repository.CustomerConsumedAppointments(customerID, sd, ed); er == nil {
		values := make([]model.StatValues, len(result))
		for k, v := range result {
			values[k] = model.StatValues{
				OfficeId: v.OfficeID,
				Value:    v.Count,
			}
		}
		res.Consumed = values
	}

	return res, nil
}
