package model

import "time"

type Announce struct {
	ID                string    `json:"id"`
	Title             string    `json:"title"`
	Body              string    `json:"body"`
	Price             float32   `json:"price"`
	PhoneNumber       string    `json:"phoneNumber"`
	Published         bool      `json:"published"`
	Pictures          []string  `json:"pictures"`
	OwnerID           string    `json:"ownerId"`
	Details           string    `json:"details"`
	DistrictID        string    `json:"districtId"`
	Location          string    `json:"location"`
	ExpireAt          time.Time `json:"expireAt"`
	CreatedAt         time.Time `json:"createdAt"`
	Status            string    `json:"status"`
	ValidationComment string    `json:"validationComment"`
}
