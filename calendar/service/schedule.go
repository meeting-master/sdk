package service

import (
	"context"
	appContext "gitlab.com/meeting-master/sdk/context"
	"strconv"
	"strings"
	"time"

	"gitlab.com/meeting-master/sdk/calendar/model"
	"gitlab.com/meeting-master/sdk/calendar/repository"
	"gitlab.com/meeting-master/sdk/errors"
)

const dateLayout = "2006-01-02"
const timeLayout = "15:04"

const timeSplitter = ":"

func (cs calendarService) InsertSchedule(ctx context.Context, request model.ScheduleRequest) *errors.Error {
	if !request.IsValid() {
		return errors.InvalidRequestData()
	}
	if !cs.authService.CanCreateService(ctx) {
		return errors.Unauthorized()
	}
	userId := appContext.ContextKeys(ctx).UserId
	var paramsRequest []repository.ScheduleDB
	for _, wd := range request.WorkDays {
		if wd < 0 || wd > 6 {
			return errors.Unknown()
		}
		for _, tf := range request.TimeFrames {

			if tf.StartTime >= tf.EndTime {
				return errors.Unknown()
			}
			pr := repository.ScheduleDB{
				OfficeID:  request.OfficeID,
				ServiceID: request.ServiceID,
				WorkDay:   wd,
				StartTime: tf.StartTime,
				EndTime:   tf.EndTime,
				Duration:  tf.Duration,
				Place:     tf.Place,
				CreatedBy: &userId,
			}
			paramsRequest = append(paramsRequest, pr)
		}
	}
	return cs.repository.InsertSchedule(paramsRequest)
}

/*
func timeFrameParseInt(tf model.TimeFrame) (int, int, *errors.Error) {
	start, _ := tfParser(tf.StartTime)
	end, _ := tfParser(tf.EndTime)
	if start >= end {
		return 0, 0, errors.Unknown()
	}
	return start, end, nil
} */

func tfParser(s string) (int, *errors.Error) {
	value := strings.Split(s, timeSplitter)
	if len(value) != 2 {
		return 0, errors.InvalidRequestData()
	}
	hour, err := strconv.Atoi(value[0])
	if err != nil {
		return 0, errors.InvalidRequestData()
	}
	min, err := strconv.Atoi(value[1])
	if err != nil {
		return 0, errors.InvalidRequestData()
	}
	if hour < 0 || hour > 23 || min < 0 || min > 59 {
		return 0, errors.InvalidRequestData()
	}
	return hour*60 + min, nil
}


func (cs calendarService) Schedule(ctx context.Context, officeID, serviceID string) ([]model.ScheduleResponse, *errors.Error) {
	if !cs.authService.CanCreateService(ctx) {
		return nil, errors.Unauthorized()
	}
	if officeID == "" || serviceID == "" {
		return nil, errors.InvalidRequestData()
	}
	schedules, err := cs.repository.ItemSchedule(officeID, serviceID)

	if err != nil {
		return nil, err
	}
	res := make([]model.ScheduleResponse, len(schedules))

	for k, v := range schedules {
		res[k] = model.ScheduleResponse{
			WorkDay:   v.WorkDay,
			StartTime: v.StartTime,
			EndTime:   v.EndTime,
			Duration:  v.Duration,
			Place:     v.Place,
		}
	}
	return res, nil
}

func (cs calendarService) UpdateSchedule(ctx context.Context, request model.ScheduleUpdate) ([]model.ScheduleResponse, *errors.Error) {
	if !request.IsValid() {
		return nil, errors.InvalidRequestData()
	}
	if !cs.authService.CanCreateService(ctx) {
		return nil,  errors.Unauthorized()
	}
	userId := appContext.ContextKeys(ctx).UserId
	paramsRequest := make([]repository.ScheduleDB, len(request.TimeFrames))
	for k, tf := range request.TimeFrames {

		if tf.StartTime >= tf.EndTime {
			return nil, errors.Unknown()
		}
		paramsRequest[k] = repository.ScheduleDB{
			OfficeID:  request.OfficeID,
			ServiceID: request.ServiceID,
			WorkDay:   tf.WorkDay,
			StartTime: tf.StartTime,
			EndTime:   tf.EndTime,
			Duration:  tf.Duration,
			Place:     tf.Place,
			CreatedBy: &userId,
		}
	}
	if err := cs.repository.UpdateSchedule(paramsRequest, request.OfficeID, request.ServiceID); err != nil {
		return nil, err
	}

	return cs.Schedule(ctx, request.OfficeID, request.ServiceID)
}

func (cs calendarService) Calendar(request model.CalendarRequest) ([]model.Calendar, *errors.Error) {
	if !request.IsValid() {
		return nil, errors.InvalidRequestData()
	}
	result, err := cs.repository.Calendar(request)
	if err != nil {
		return nil, err
	}
	if result == nil || len(result) == 0 {
		return nil, nil
	}

	return cs.calendarResultToCalendar(result)
}

func (cs calendarService) calendarResultToCalendar(rawDatas []repository.CalendarResult) ([]model.Calendar, *errors.Error) {

	timeSlotsByDays := make(map[string][]model.TimeSlot)
	for _, cr := range rawDatas {
		crDate := cr.StartTime.Format(dateLayout)
		crTime := cr.StartTime.Format(timeLayout)
		endTime := cr.EndTime.Time

		timeSlotsResult := model.TimeSlot{
			Id:            cr.Id,
			StartTime:     crTime,
			StartDateTime: cr.StartTime,
			EndTime:       endTime.Format(timeLayout),
			EndDateTime:   endTime,
			IsPassed:      cr.StartTime.Before(time.Now()),
			IsAvailable:   cr.ConsumedAppointments < cr.ScheduledAppointments,
			Available:     cr.ScheduledAppointments - cr.ConsumedAppointments,
			Consumed:      cr.ConsumedAppointments,
		}

		timeSlots := timeSlotsByDays[crDate]
		timeSlots = append(timeSlots, timeSlotsResult)
		timeSlotsByDays[crDate] = timeSlots
	}

	calendar := make([]model.Calendar, len(timeSlotsByDays))
	index := 0
	for key, element := range timeSlotsByDays {
		calendar[index] = model.Calendar{
			Date:          key,
			TimeZoneCode:  "",
			IsAvailable:   cs.checkCalendarDayAvailability(element),
			CalendarSlots: element,
		}
		index++
	}
	return calendar, nil

}

func (cs calendarService) checkCalendarDayAvailability(rawDatas []model.TimeSlot) bool {
	for _, cts := range rawDatas {
		if cts.IsAvailable {
			return true
		}
	}
	return false
}

func int2Tf(value int) string {
	m := value % 60
	h := (value - m) / 60
	hs := strconv.Itoa(h)
	if h < 10 {
		hs = "0" + hs
	}
	ms := strconv.Itoa(m)
	if m < 10 {
		ms = "0" + ms
	}
	return hs + timeSplitter + ms
}
