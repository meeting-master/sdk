package model

import (
	"database/sql"
	"github.com/lib/pq"
	"time"
)

type Message struct {
	Id           string         `db:"id"`
	CreatedAt    time.Time      `db:"created_at"`
	ValidatedAt  sql.NullTime   `db:"validated_at"`
	From         string         `db:"message_from"`
	To           string         `db:"message_to"`
	Body         string         `db:"body"`
	Subject      string         `db:"subject"`
	Retry        int            `db:"retry"`
	ErrorMessage pq.StringArray `db:"error_message"`
}

type AccountNotification struct {
	Name        string
	MailAddress string
	Code        string
}

type AppointmentNotification struct {
	Name      string
	StartDate string
	StartHour string
	EndDate   string
	EndHour   string
	Company   string
	Service   string
	Address   string
	Place     int
	To        string
}
