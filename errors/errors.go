package errors

type Error struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

func (e Error) Error() string {
	return e.Message
}

func InvalidValidToken() *Error {
	return &Error{
		Code:    "1000",
		Message: "invalid token",
	}
}

func InvalidRequestData() *Error {
	return &Error{
		Code:    "1001",
		Message: "invalid request data",
	}
}

func EmptyResultData() *Error {
	return &Error{
		Code:    "1002",
		Message: "empty data",
	}
}

func Unavailable() *Error {
	return &Error{
		Code:    "1003",
		Message: "unvailable request provided data",
	}
}

func MissingMandatory() *Error {
	return &Error{
		Code:    "1004",
		Message: "missing mandatory field",
	}
}

func Enough() *Error {
	return &Error{
		Code:    "1005",
		Message: "request is too heavy than available",
	}
}

func TsAlready() *Error {
	return &Error{
		Code:    "1006",
		Message: "existing TS for the range of dates",
	}
}


func Unauthorized() *Error {
	return &Error{
		Code:    "2001",
		Message: "access denied for this resource",
	}
}

func DuplicateUser() *Error {
	return &Error{
		Code:    "2002",
		Message: "user already exists",
	}
}

func ForgotPassword() *Error {
	return &Error{
		Code:    "2003",
		Message: "user already exists",
	}
}


func Unknown() *Error {
	return &Error{
		Code:    "3001",
		Message: "unknown error",
	}
}

func UnknownData() *Error {
	return &Error{
		Code:    "3002",
		Message: "unknown provided data",
	}
}

func DBError(err error) *Error {
	if err == nil {
		return nil
	}
	return &Error{
		Code:    "3002",
		Message: err.Error(),
	}
}

func InvalidDateFormat() *Error {
	return &Error{
		Code:    "4001",
		Message: "invalid date format",
	}
}

func InvalidDateOrder() *Error {
	return &Error{
		Code:    "4002",
		Message: "invalid date order",
	}
}

func InvalidTags() *Error {
	return &Error{
		Code:    "5000",
		Message: "invalid tag",
	}
}


