package service

import (
	"gitlab.com/meeting-master/sdk/errors"
	"gitlab.com/meeting-master/sdk/location/model"
	"gitlab.com/meeting-master/sdk/location/repository"
)

type LocationService interface {
	UpsertCountry(request model.CountryRequest) (string, *errors.Error)

	UpsertTown(request model.TownRequest) (string, *errors.Error)

	UpsertDistrict(request model.DistrictRequest) (string, *errors.Error)

	Countries() ([]model.Country, *errors.Error)

	Towns(countryID string) ([]model.Town, *errors.Error)

	Districts(townID string) ([]model.District, *errors.Error)

	LocationByDistrict() ([]model.Location, *errors.Error)

	LocationByTown() ([]model.Location, *errors.Error)
}

type locationService struct {
	repository repository.LocationRepository
}

func NewLocationService(repo repository.LocationRepository) LocationService {
	return &locationService{repository: repo}
}
