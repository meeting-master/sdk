package repository

import (
	"gitlab.com/meeting-master/sdk/errors"
)

func (r repository) Upsert(request DBRequest) (string, *errors.Error) {
	query := insertAnnounce
	if request.ID != "" {
		query = updateAnnounce
	}

	row, err := r.db.NamedQuery(query, request)

	if err != nil {
		return "", errors.DBError(err)
	}
	var insertID string
	if row.Next() {
		row.Scan(&insertID)
	}
	return insertID, errors.DBError(err)
}

func (r repository) Delete(announceID string) *errors.Error {
	if announceID == "" {
		return errors.InvalidRequestData()
	}
	_, err := r.db.Exec(deleteAnnounce, announceID)
	if err != nil {
		return errors.DBError(err)
	}

	return nil
}
func (r repository) PubById(ID string) (DBResponse, *errors.Error) {

	if ID == "" {
		return DBResponse{}, errors.InvalidRequestData()
	}
	var res []DBResponse

	err := r.db.Select(&res, getAnnounceById, ID)
	if err != nil {
		return DBResponse{}, errors.DBError(err)
	}
	if len(res) == 0 {
		return DBResponse{}, nil
	}
	return res[0], nil
}
func (r repository) PubsByOwner(ownerID string) ([]DBResponse, *errors.Error) {
	if ownerID == "" {
		return nil, errors.InvalidRequestData()
	}
	var res []DBResponse

	err := r.db.Select(&res, getAnnouncesByOwner, ownerID)
	if err != nil {
		return nil, errors.DBError(err)
	}

	return res, nil
}

func (r repository) PubsByLocations(locationIds ...string) ([]DBResponse, *errors.Error) {
	panic("implement me")
}

func (r repository) PubsForValidation() ([]DBResponse, *errors.Error) {
	panic("implement me")
}

func (r repository) Validation(valid bool, validatedBy, comment string) *errors.Error {
	panic("implement me")
}