package service

import (
	"gitlab.com/meeting-master/sdk/errors"
	"gitlab.com/meeting-master/sdk/location/model"
	"gitlab.com/meeting-master/sdk/location/repository"
	"strconv"
	"strings"
)

func (ls locationService) UpsertCountry(request model.CountryRequest) (string, *errors.Error) {
	if !request.IsValid() {
		return "", errors.InvalidRequestData()
	}

	return ls.repository.UpsertCountry(repository.Country{
		ID:        request.ID,
		Name:      strings.ToUpper(request.Name),
		IsoCode:   request.IsoCode,
		Indicator: request.Ind(),
	})
}

func (ls locationService) UpsertTown(request model.TownRequest) (string, *errors.Error) {
	if !request.IsValid() {
		return "", errors.InvalidRequestData()
	}

	return ls.repository.UpsertTown(request)
}

func (ls locationService) UpsertDistrict(request model.DistrictRequest) (string, *errors.Error) {
	if !request.IsValid() {
		return "", errors.InvalidRequestData()
	}

	return ls.repository.UpsertDistrict(request)
}

func (ls locationService) Countries() ([]model.Country, *errors.Error) {
	countryInfos, err := ls.repository.Countries()
	if err != nil {
		return nil, err
	}

	if len(countryInfos) == 0 {
		return nil, nil
	}

	countries := make([]model.Country, len(countryInfos))
	for index, info := range countryInfos {
		countries[index] = model.Country{
			ID:        info.ID,
			Name:      info.Name,
			IsoCode:   info.IsoCode,
			Indicator: strconv.Itoa(info.Indicator),
		}
	}
	return countries, err
}

func (ls locationService) Towns(countryID string) ([]model.Town, *errors.Error) {

	tws, err := ls.repository.Towns(countryID)

	if err != nil {
		return nil, err
	}

	if len(tws) == 0 {
		return nil, nil
	}

	return townResultToTowns(tws...), nil
}

func townResultToTowns(tws ...repository.Town) []model.Town {
	res := make([]model.Town, len(tws))
	for k, v := range tws {
		res[k] = model.Town{
			ID:          v.ID.String(),
			Name:        v.Name,
			Latitude:    v.Latitude.Float64,
			Longitude:   v.Longitude.Float64,
			CountryID:   v.CountryID.String(),
			CountryName: v.CountryName,
		}
	}
	return res
}

func (ls locationService) LocationByDistrict() ([]model.Location, *errors.Error) {
	rows, err := ls.repository.Localizations(false)
	if err != nil {
		return nil, err
	}

	if len(rows) == 0 {
		return nil, nil
	}

	return locationResultToLocation(rows), nil
}

func (ls locationService) LocationByTown() ([]model.Location, *errors.Error) {
	rows, err := ls.repository.Localizations(true)
	if err != nil {
		return nil, err
	}

	if len(rows) == 0 {
		return nil, nil
	}

	return locationResultToLocation(rows), nil
}

func locationResultToLocation(rows []repository.LocationResult) []model.Location {
	response := make([]model.Location, len(rows))
	for k, v := range rows {
		response[k] = model.Location{
			ID:       v.ID,
			Location: v.Location,
			Country:  v.Country,
		}
	}

	return response
}

func (ls locationService) Districts(townID string) ([]model.District, *errors.Error) {
	dis, err := ls.repository.Districts(townID)

	if err != nil {
		return nil, err
	}

	if len(dis) == 0 {
		return nil, nil
	}

	res := make([]model.District, len(dis))
	for k, v := range dis {
		res[k] = model.District{
			ID:       v.ID.String(),
			Name:     v.Name,
			TownName: v.TownName,
			TownID:   v.TownID.String(),
		}
	}
	return res, nil
}
