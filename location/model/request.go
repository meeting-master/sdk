package model

import "strconv"

type CountryRequest struct {
	ID        string `json:"id"`
	Indicator string `json:"indicator"`
	Name      string `json:"name"`
	IsoCode   string `json:"isoCode"`
}

func (c *CountryRequest) IsValid() bool {
	ind, err := strconv.Atoi(c.Indicator)
	if err != nil {
		return false
	}
	return ind > 0 && len(c.Name) > 0 && len(c.IsoCode) > 0
}

func (c *CountryRequest) Ind() int {
	ind, _ := strconv.Atoi(c.Indicator)
	return ind
}

type TownRequest struct {
	ID        string  `json:"id"`
	Name      string  `json:"name"`
	Longitude float64 `json:"longitude"`
	Latitude  float64 `json:"latitude"`
	CountryID string  `json:"countryId"`
}

func (c *TownRequest) IsValid() bool {
	return len(c.Name) > 0 && len(c.CountryID) > 0
}

type DistrictRequest struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	TownID string `json:"townId"`
}

func (c *DistrictRequest) IsValid() bool {
	return c.Name != "" && len(c.TownID) > 0
}
