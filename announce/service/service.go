package service

import (
	"context"
	"gitlab.com/meeting-master/sdk/announce/model"
	"gitlab.com/meeting-master/sdk/announce/repository"
	"gitlab.com/meeting-master/sdk/auth/service"
	"gitlab.com/meeting-master/sdk/errors"
	"gitlab.com/meeting-master/sdk/messaging/engine"
)

type AnnounceService interface {
	//insert or update an announce
	Upsert(request model.AnnounceRequest) (string, *errors.Error)

	//Delete an announce
	Delete(announceID, ownerID string) *errors.Error

	//return announce against the given ID
	AnnounceById(announceID string) (model.Announce, *errors.Error)

	//return list of announce which match the given tag
	AnnouncesByTag(tag string, townID string) ([]model.Announce, *errors.Error)

	//return list of announce against a given owner id
	AnnouncesByOwner(ownerID string) ([]model.Announce, *errors.Error)


	//return list of past seven days announces
	LastAnnounces() ([]model.Announce, *errors.Error)

	//return list of non validate announce
	AnnouncesToValidate(ctx context.Context) ([]model.Announce, *errors.Error)

	//return list of non validate pubs
	Validate(ctx context.Context, request model.ValidationRequest)  *errors.Error
}

type announceService struct {
	repo repository.AnnounceRepository
	authService service.AuthService
	mailEngine  engine.MessageEngine
}

func NewAnnounceService(repo repository.AnnounceRepository) AnnounceService {
	return InitWithEngine(repo, nil, nil)
}

func InitWithEngine(repo repository.AnnounceRepository, auth service.AuthService, engine engine.MessageEngine) AnnounceService {
	return &announceService{
		repo:  repo,
		authService: auth,
		mailEngine:  engine}
}

