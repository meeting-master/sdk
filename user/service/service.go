package service

import (
	"context"
	"gitlab.com/meeting-master/sdk/auth/service"
	"gitlab.com/meeting-master/sdk/errors"
	"gitlab.com/meeting-master/sdk/messaging/engine"
	"gitlab.com/meeting-master/sdk/user/model"
	"gitlab.com/meeting-master/sdk/user/repository"
)
//the context may have all needed keys to perform operation
//see context package for keys

//function are sorted alphabetically
type UserService interface {
	//active current user
	//activation code must be sent by the context
	Active(ctx context.Context) *errors.Error

	//change the current user password
	ChangePassword(ctx context.Context, request model.LoginRequest) *errors.Error

	//user creation
	//user id and activation code are returned
	Create(ctx context.Context, request model.UserCreateRequest) (string, string, *errors.Error)

	//change uer info
	UpdateInfo(ctx context.Context, request model.UserUpdateRequest) *errors.Error

	//grant privileges to the user
	Grant(ctx context.Context, request model.GrantRequest) *errors.Error

	//user login
	Login(request model.LoginRequest) (model.LoginResponse, *errors.Error)

	//user logout
	Logout(ctx context.Context)

	//generate a new activation code to the user
	GenerateCode(ctx context.Context) (string, *errors.Error)

	//get user info
	User(ctx context.Context) (model.User, *errors.Error)

	//retrieve users list of a resource
	Users(ctx context.Context) ([]model.User, *errors.Error)

	//retrieve users pv for a given resource
	Privileges(ctx context.Context, userID string) ([]model.Privilege, *errors.Error)

	ForgotPassword(email string) *errors.Error

	UpsertEndUser(request model.EndUser) *errors.Error

	EndUser(ID string) (model.EndUser, *errors.Error)
}

type userService struct {
	repository  repository.UserRepository
	authService service.AuthService
	mailEngine  engine.MessageEngine
}


func NewUserService(repo repository.UserRepository, auth service.AuthService) UserService {
	return InitWithEngine(repo, auth, nil)
}

func InitWithEngine(repo repository.UserRepository, auth service.AuthService, engine engine.MessageEngine) UserService {
	return &userService{
		repository:  repo,
		authService: auth,
		mailEngine:  engine}
}

