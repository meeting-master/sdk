package model

import (
	"time"
)

type ScheduleRequest struct {
	OfficeID   string      `json:"officeId"`
	ServiceID  string      `json:"serviceId"`
	WorkDays   []int       `json:"workDays"`
	TimeFrames []TimeFrame `json:"timeFrames"`
}

type ScheduleUpdate struct {
	OfficeID   string         `json:"officeId"`
	ServiceID  string         `json:"serviceId"`
	TimeFrames []DayTimeFrame `json:"timeFrames"`
}

type TimeFrame struct {
	StartTime int `json:"startTime"`
	EndTime   int `json:"endTime"`
	Duration  int `json:"duration"`
	Place     int `json:"place"`
}

type DayTimeFrame struct {
	WorkDay   int `json:"workDay"`
	StartTime int `json:"startTime"`
	EndTime   int `json:"endTime"`
	Duration  int `json:"duration"`
	Place     int `json:"place"`
}

func (cr *ScheduleRequest) IsValid() bool {
	return cr.OfficeID != "" && cr.ServiceID != "" && cr.WorkDays != nil && cr.TimeFrames != nil
}

func (su *ScheduleUpdate) IsValid() bool {
	return su.OfficeID != "" && su.ServiceID != "" && su.TimeFrames != nil
}

type TimeSlotRequest struct {
	OfficeID   string    `json:"officeID"`
	ServiceIDs []string  `json:"serviceIds"`
	StartDate  time.Time `json:"startDate"`
	EndDate    time.Time `json:"endDate"`
}

type CalendarRequest struct {
	ServiceID string    `json:"serviceId"`
	OfficeID  string    `json:"officeId"`
	StartDate time.Time `json:"startDate"`
	EndDate   time.Time `json:"endDate"`
}

func (ts *TimeSlotRequest) IsValid() bool {
	if ts.OfficeID == "" || len(ts.ServiceIDs) == 0 || ts.StartDate.IsZero() || ts.EndDate.IsZero() || ts.StartDate.After(ts.EndDate) {
		return false
	}
	return true
}

type TimeSlotUpdateRequest struct {
	Id        string `json:"id"`
	Value     int    `json:"value"`
	Cancelled bool   `json:"cancelled"`
}

func (tsu *TimeSlotUpdateRequest) IsValid() bool {
	return len(tsu.Id) != 0
}

func (cr *CalendarRequest) IsValid() bool {
	return cr.OfficeID != "" && cr.ServiceID != "" && !cr.StartDate.IsZero() && !cr.EndDate.IsZero()
}

type AppointmentRequest struct {
	TimeSlotId  string `json:"timeSlotId"`
	Details     string `json:"detail"`
	PlaceNumber int    `json:"placeNumber"`
	Owner       string `json:"owner"`
}

func (ap *AppointmentRequest) IsValid() bool {
	return len(ap.TimeSlotId) != 0 && ap.PlaceNumber >= 1
}

type DeviceRequest struct {
	UserID string `json:"userId"`
	Token  string `json:"token"`
	Lang   string `json:"lang"`
}

func (rq *DeviceRequest) IsValid() bool {
	return rq.UserID != "" && rq.Token != ""
}

type RatingRequest struct {
	AppointmentId string  `json:"appointmentId"`
	Rating        float32 `json:"rating"`
	Comment       string  `json:"comment"`
}

func (rq *RatingRequest) IsValid() bool {
	return rq.AppointmentId != "" && rq.Rating > 0
}
