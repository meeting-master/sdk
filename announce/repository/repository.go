package repository

import (
	"gitlab.com/meeting-master/sdk/connector/pgsql"
	"gitlab.com/meeting-master/sdk/errors"
	"time"
)

type AnnounceRepository interface {
	Upsert(request AnnounceDBRequest) (string, *errors.Error)
	Delete(announceID string) *errors.Error
	AnnounceById(announceID string) (AnnounceDB, *errors.Error)
	AnnouncesByOwner(ownerID string) ([]AnnounceDB, *errors.Error)
	AnnouncesByTag(tags, townID string) ([]AnnounceDB, *errors.Error)
	AnnouncesSince(date time.Time) ([]AnnounceDB, *errors.Error)
	AnnouncesToValidate() ([]AnnounceDB, *errors.Error)
	Validate(valid bool, announceID, comment, validatedBy string) *errors.Error
}

type repository struct {
	db *pgsql.DB
}

func NewAnnounceRepository(db *pgsql.DB) AnnounceRepository {
	return repository{db}
}
