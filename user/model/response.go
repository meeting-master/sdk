package model

type LoginResponse struct {
	Token        string
	FirstName    *string
	LastName     *string
	CustomerId   string
	UserName     string
	BusinessCode *string
	IsActive     bool
}

type User struct {
	ID          string  `json:"id"`
	UserName    string  `json:"userName"`
	FirstName   *string `json:"firstName"`
	LastName    *string `json:"lastName"`
	IsActive    bool    `json:"isActive"`
	PhoneNumber *string `json:"phoneNumber"`
}

type EndUser struct {
	ID     string `json:"id"`
	Detail string `json:"detail"`
}
