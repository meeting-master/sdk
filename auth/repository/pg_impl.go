package repository

import (
	"gitlab.com/meeting-master/sdk/errors"
	"log"

	ur "gitlab.com/meeting-master/sdk/user/repository"
)

func (r repository) UserPrivileges(isAdmin bool, userId, accountId string) ([]PrivilegeResult, *errors.Error) {
	var result []PrivilegeResult
	var err error
	if isAdmin {
		err = r.db.Select(&result, ur.AdminPrivileges, userId)
	} else {
		err = r.db.Select(&result, ur.UserPrivileges, userId, accountId)
	}
	if  err != nil {
		log.Println(err)
		return nil, errors.DBError(err)
	}
	return result, nil
}

func (r repository) UserByID(userID string) ur.UserResult {
	var result []ur.UserResult
	if err := r.db.Select(&result, ur.UserByID, userID); err != nil {
		log.Println(err)
		return ur.UserResult{}
	}
	if len(result) == 0 {
		return ur.UserResult{}
	}
	return result[0]
}
