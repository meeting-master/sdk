package service

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/meeting-master/sdk/calendar/model"
	mocks2 "gitlab.com/meeting-master/sdk/mocks/auth/service"
	mocks "gitlab.com/meeting-master/sdk/mocks/calendar/repository"
	"testing"
)

func setup() (CalendarService, *mocks.CalendarRepository) {
	r := &mocks.CalendarRepository{}
	auth := &mocks2.AuthService{}
	s := NewCalendarService(r, auth)
	return s, r
}

func TestCalendarService_PlanningParameters(t *testing.T) {
	t.Run("service id not provided", func(t *testing.T) {
		s, _ := setup()
		data := model.CalendarParameter{
			ServiceId:  "",
			Duration:   0,
			Parameters: nil,
		}
		er := s.PlanningParameters(data)
		assert.Equal(t, er, "invalid request data")
	})
	t.Run("service id not provided", func(t *testing.T) {
		s, _ := setup()
		data := model.CalendarParameter{
			ServiceId:  "",
			Duration:   0,
			Parameters: nil,
		}
		er := s.PlanningParameters(data)
		assert.EqualError(t, er, "invalid request data")
	})
	t.Run("duration not provided", func(t *testing.T) {
		s, _ := setup()
		data := model.CalendarParameter{
			ServiceId:  "fhjhfdjhfhdf",
			Duration:   0,
			Parameters: nil,
		}
		er := s.PlanningParameters(data)
		assert.EqualError(t, er, "invalid request data")
	})
	t.Run("params not provided", func(t *testing.T) {
		s, _ := setup()
		data := model.CalendarParameter{
			ServiceId:  "fhjhfdjhfhdf",
			Duration:   30,
			Parameters: nil,
		}
		er := s.PlanningParameters(data)
		assert.EqualError(t, er, "invalid request data")
	})
}