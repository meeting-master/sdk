package repository

import (
	"database/sql"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"gitlab.com/meeting-master/sdk/customer/model"
	"gitlab.com/meeting-master/sdk/errors"
	userRepository "gitlab.com/meeting-master/sdk/user/repository"
	"gitlab.com/meeting-master/sdk/utils"
)

func (r repository) UpsertCustomer(customer model.CustomerRequest) (string, string, string, *errors.Error) {
	if !customer.IsValid() {
		return "", "", "", errors.InvalidRequestData()
	}

	if customer.Id == "" {
		return r.insertNew(customer)
	}
	row, err := r.db.NamedQuery(updateCustomer, customer)

	if err != nil {
		return "", "", "", errors.DBError(err)
	}
	var insertID string
	if row.Next() {
		row.Scan(&insertID)
	}
	return insertID, "", "", errors.DBError(err)
}

//customer creation process is done in the following order
//1. insert customer
//2. insert user
//3. grant user with manager role
//
//process is done in transaction
func (r repository) insertNew(request model.CustomerRequest) (string, string, string, *errors.Error) {

	hashPass, _ := utils.HashPassword(request.Password)
	activationCode := strconv.Itoa(utils.GenerateCode(false))
	hashCode, _ := utils.HashPassword(activationCode)

	tx := r.db.MustBegin()
	//insert master office
	row, err := tx.NamedQuery(insertNewCustomer, request)
	if err != nil {
		log.Println(err)
		return "", "", "", errors.DBError(err)
	}

	var customerId string
	if row.Next() {
		row.Scan(&customerId)
	}
	_ = row.Close()
	userArgs := map[string]interface{}{
		"username":   request.UserName,
		"password":   hashPass,
		"code":       hashCode,
		"customerid": customerId,
	}
	//insert the first user
	row, err = tx.NamedQuery(insertNewUser, userArgs)
	if err != nil {
		log.Println(err)
		_ = tx.Rollback()
		return "", "", "", errors.DuplicateUser()
	}
	var userId string
	if row.Next() {
		row.Scan(&userId)
	}
	_ = row.Close()

	// grand the user
	pr := userRepository.PrivilegeRequest{
		RoleCode:   "MA",
		ResourceId: sql.NullString{String: customerId, Valid: true,},
		OfficeId:   sql.NullString{String: "", Valid: false,},
		ServiceId:  sql.NullString{String: "", Valid: false,},
		UserId:     sql.NullString{String: userId, Valid: true,},
	}

	row, err = tx.NamedQuery(userRepository.InsertPrivilege, pr)
	if err != nil {
		log.Println(err)
		_ = tx.Rollback()
		return "", "", "", errors.DBError(err)
	}
	_ = row.Close()
	if err := tx.Commit(); err != nil {
		log.Println(err)
		return "", "", "", errors.DBError(err)
	}

	return customerId, activationCode, userId, nil
}

func (r repository) Customers() ([]CustomerResult, *errors.Error) {
	var result []CustomerResult
	err := r.db.Select(&result, customers)
	return result, errors.DBError(err)
}

func (r repository) CustomerById(customerID string) ([]CustomerResult, *errors.Error) {
	if customerID == "" {
		return nil, errors.InvalidRequestData()
	}
	var result []CustomerResult

	err := r.db.Select(&result, customerById, customerID)
	return result, errors.DBError(err)
}

func (r repository) CustomersByTag(tag, townId string) ([]CustomerTagResult, *errors.Error) {
	var result []CustomerTagResult
	var err error
	if townId == "" {
		err = r.db.Select(&result, customersByTag, fmt.Sprintf("%%%v%%", strings.ToLower(tag)))
	} else {
		err = r.db.Select(&result, customersByTownAndTag, fmt.Sprintf("%%%v%%", strings.ToLower(tag)), townId)
	}

	return result, errors.DBError(err)
}

func (r repository) DeleteCustomer(id string) (string, *errors.Error) {
	request := model.DeleteRequest{id}
	row, err := r.db.NamedQuery(deleteCustomer, request)
	if err != nil {
		return "", errors.DBError(err)
	}
	var deletedId string
	if row.Next() {
		row.Scan(&deletedId)
	}
	return deletedId, errors.DBError(err)
}

func (r repository) AppointmentStreams(customerId string, startDate, endDate time.Time) ([]AppointmentStreamInfo, *errors.Error) {
	if customerId == "" || startDate.IsZero() || endDate.IsZero() || startDate.After(endDate) {
		return nil, errors.InvalidRequestData()
	}

	var result []AppointmentStreamInfo
	if err := r.db.Select(&result, appointmentStream, customerId, startDate, endDate); err != nil {
		return nil, errors.DBError(err)
	}

	return result, nil
}

func (r repository) CustomerRating(customerID string, startDate, endDate time.Time) ([]RatingResult, *errors.Error) {
	if customerID == "" || startDate.IsZero() || endDate.IsZero() || startDate.After(endDate) {
		return nil, errors.InvalidRequestData()
	}

	return nil, nil
}

func (r repository) Logos() []LogoResult {
	var res []LogoResult
	if err := r.db.Select(&res, logo); err != nil {
		log.Println(err)
		return nil
	}
	return res
}


func (r repository) CustomerAppointments(customerID string, startDate, endDate time.Time) ([]StatResult, *errors.Error) {
	var res []StatResult
	if err := r.db.Select(&res, appointments, customerID, startDate, endDate); err != nil {
		log.Println(err)
		return nil, errors.DBError(err)
	}
	return res, nil
}

func (r repository) CustomerConsumedAppointments(customerID string, startDate, endDate time.Time) ([]StatResult, *errors.Error) {
	var res []StatResult
	if err := r.db.Select(&res, consumedAppointments, customerID, startDate, endDate); err != nil {
		log.Println(err)
		return nil, errors.DBError(err)
	}
	return res, nil
}

func (r repository) CustomerCanceledAppointments(customerID string, startDate, endDate time.Time) ([]StatResult, *errors.Error) {
	var res []StatResult
	if err := r.db.Select(&res, canceledAppointments, customerID, startDate, endDate); err != nil {
		log.Println(err)
		return nil, errors.DBError(err)
	}
	return res, nil
}
