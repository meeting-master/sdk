package repository

const (
	insertMessage = `INSERT INTO common.message(message_from, message_to, subject, body)
					VALUES(:message_from, :message_to, :subject, :body) 
					RETURNING id`

	getMessagesToSent = `SELECT * FROM common.message WHERE validated_at ISNULL AND retry < 2`

	update = `UPDATE common.message 
				SET 
					retry = retry +1, 
					validated_at = :validated_at, 
					error_message = :error_message
				WHERE id = :id`
)
