package repository

import (
	"database/sql"
)

type LoginResult struct {
	ID           string  `db:"id"`
	UserName     string  `db:"username"`
	Password     string  `db:"password"`
	FirstName    *string `db:"first_name"`
	LastName     *string `db:"last_name"`
	IsActive     bool    `db:"active"`
	CustomerId   *string `db:"customer_id"`
	BusinessCode *string `db:"business_code"`
}

type PrivilegeResult struct {
	Code string `db:"code"`
	Name string `db:"name"`
}

type PrivilegeRequest struct {
	RoleCode   string         `db:"role_code"`
	ResourceId sql.NullString `db:"resource_id"`
	OfficeId   sql.NullString `db:"office_id"`
	ServiceId  sql.NullString `db:"service_id"`
	UserId     sql.NullString `db:"user_id"`
}

type UserResult struct {
	ID             string  `db:"id"`
	UserName       string  `db:"username"`
	FirstName      *string `db:"first_name"`
	LastName       *string `db:"last_name"`
	PhoneNumber    *string `db:"phone_number"`
	Password       string  `db:"password"`
	ActivationCode string  `db:"activation_code"`
	IsActive       bool    `db:"active"`
}

type UserCreateRequest struct {
	ID             string
	UserName       string
	Password       string
	PhoneNumber    string
	FirstName      string
	LastName       string
	CreatedBy      string
	ResourceID     sql.NullString
	ActivationCode string
	BusinessCode   string
}

func (us *UserCreateRequest) IsValid() bool {
	if us.UserName == "" || us.Password == "" || us.FirstName == "" || us.CreatedBy == "" || us.ActivationCode == "" {
		return false
	}

	return true
}
