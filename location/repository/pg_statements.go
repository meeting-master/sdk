package repository

const (
	insertNewCountry = `INSERT INTO location.country(indicator, name, iso_code)
					VALUES(:indicator, :name, :iso_code) RETURNING id`

	updateCountry = `UPDATE location.country set indicator = :indicator, name = :name, iso_code = :iso_code WHERE id = :id RETURNING id`

	insertNewTown = `INSERT INTO location.town(name, country_id, longitude, latitude)
					VALUES(:name, :countryid, :longitude, :latitude) 
					RETURNING id`

	updateTown = `UPDATE location.town set name = :name, country_id = :countryid, longitude = :longitude, latitude = :latitude
					WHERE id = :id
					RETURNING id`

	insertNewDistrict = `INSERT INTO location.district(name, town_id) VALUES(:name, :townid) RETURNING id`

	updateDistrict = `UPDATE location.district set name = :name, town_id = :townid where id = :id RETURNING id`

	getCountries = `SELECT * FROM location.country`

	getTowns = `select t.id as "id", t.name as "name", t.longitude, t.latitude, t.country_id as "country_id", c.name as "country_name"
				from location.town t
				inner join location.country c on t.country_id = c.id`

	getTownsByCountry = `select t.id as "id", t.name as "name", t.longitude, t.latitude
				from location.town t
				inner join location.country c on t.country_id = c.id
				where c.id = $1 order by t.name`

	getDistricts = `select d.id as "id", d.name as "name", t.id as "town_id", t.name "town_name"
				from location.district d
				inner join location.town t on t.id = d.town_id`

	getDistrictsByTown = `select d.id as "id", d.name as "name", t.id as "town_id", t.name "town_name"
							from location.district d
							inner join location.town t on t.id = d.town_id where t.id = $1 
							order by t.name, d.name`

	getDistrictLocation = `SELECT d.id, c.iso_code as "code", CONCAT(c.name, ' ', t.name, ' ', d.name) as location
						from location.district d
						inner join location.town t on t.id = d.town_id 
						inner join location.country c on c.id = t.country_id 
						order by c.name, t.name, d.name`

	getTownLocation = `SELECT t.id, c.iso_code as "code", CONCAT(t.name, ' ', c.name) as location
						from location.town t
						inner join location.country c on c.id = t.country_id
						order by c.name, t.name`
)
