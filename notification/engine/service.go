package engine

import (
	"gitlab.com/meeting-master/sdk/errors"
	"gitlab.com/meeting-master/sdk/notification/model"
	"os"
	"strconv"
	"time"
)

const timeLayout = "15:04"

func (e engine) DayMessages() ([]model.Message, *errors.Error) {
	data, err := e.repository.DayMessages()
	if err != nil {
		return nil, err
	}
	res := make([]model.Message, len(data))
	for k, v := range data {
		res[k] = model.Message{Token:v.Token, Lang: v.Lang, Location: v.Name, Hour: v.StartTime.Format(timeLayout)}
	}

	return res, nil
}

func (e engine) HourMessages() ([]model.Message, *errors.Error) {
	data, err := e.repository.HourMessages()
	if err != nil {
		return nil, err
	}
	res := make([]model.Message, len(data))
	for k, v := range data {
		res[k] = model.Message{Token:v.Token, Lang: v.Lang, Location: v.Name, Hour: v.StartTime.Format(timeLayout)}
	}

	return res, nil
}


func (e engine) Announces() ([]model.Message, *errors.Error) {
	delay, er := strconv.Atoi(os.Getenv("ANNOUNCE_NOTIF_DELAY"))
	if er != nil {
		return nil, errors.InvalidRequestData()
	}
	startDate := time.Now().UTC().Add(-time.Minute*time.Duration(delay))
	data, err := e.repository.Announces(startDate)
	if err != nil {
		return nil, err
	}
	res := make([]model.Message, len(data))
	for k, v := range data {
		res[k] = model.Message{Token:v.Token, Lang: v.Lang, Location: v.Name, Hour: v.StartTime.Format(timeLayout)}
	}
	return res, nil
}