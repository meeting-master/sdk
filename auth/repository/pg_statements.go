package repository

const (

	userPrivileges = `SELECT role_code, resource_id as "account_id", office_id, service_id FROM common.privilege p
						WHERE p.user_id::TEXT = $1 and p.resource_id::TEXT = $2`
)
