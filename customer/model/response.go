package model

import "time"

type Customer struct {
	Id          string    `json:"id"`
	Alias       string    `json:"alias"`
	Name        string    `json:"name"`
	PhoneNumber string    `json:"phoneNumber"`
	WebSite     string    `json:"webSite"`
	LogoUrl     string    `json:"logoUrl"`
	Rating      float32   `json:"rating"`
	Activities  string    `json:"activities"`
	Value       int32     `json:"value"`
	Tags        []string  `json:"tags"`
	Offices     []Office  `json:"offices"`
	Services    []Service `json:"services"`
}

type Office struct {
	Id            string      `json:"id"`
	Alias         string      `json:"alias"`
	Name          string      `json:"name"`
	StreetNumber  string      `json:"streetNumber"`
	StreetName    string      `json:"streetName"`
	ZipCode       string      `json:"zipCode"`
	PhoneNumber   string      `json:"phoneNumber"`
	WebSite       string      `json:"webSite"`
	Email         string      `json:"email"`
	Longitude     float64     `json:"longitude"`
	Latitude      float64     `json:"latitude"`
	Localization  GeoPoint    `json:"latLon"`
	DistrictId    string      `json:"districtId"`
	Location      string      `json:"location"`
	CustomerId    string      `json:"customerId"`
	CustomerName  string      `json:"customerName"`
	Activities    string      `json:"activities"`
	CustomerAlias string      `json:"customerAlias"`
	LogoUrl       string      `json:"logoUrl"`
	Rating        float32     `json:"rating"`
	IsArchived    bool        `json:"isArchived"`
	ServiceIDs    []string    `json:"serviceIds"`
	Statistics    interface{} `json:"statistics"`
	Distance      float64     `json:"distance"`
}

type Service struct {
	Id                 string              `json:"id"`
	Alias              string              `json:"alias"`
	IsArchived         bool                `json:"isArchived"`
	Name               string              `json:"name"`
	CustomerId         string              `json:"customerId"`
	CustomerName       string              `json:"customerName"`
	CustomerAlias      string              `json:"customerAlias"`
	AppointmentStreams []AppointmentStream `json:"AppointmentStreams"`
}

type Item struct {
	Id           string `json:"id"`
	OfficeId     string `json:"officeId"`
	ServiceId    string `json:"serviceId"`
	ServiceName  string `json:"serviceName"`
	ServiceAlias string `json:"serviceAlias"`
}

type GeoPoint struct {
	Lon float64 `json:"lon"`
	Lat float64 `json:"lat"`
}

type AppointmentStream struct {
	Date  string `json:"date"`
	Count int    `json:"count"`
}

type Rating struct {
	Id        string  `json:"id"`
	Value     float32 `json:"value"`
	Comment   string  `json:"comment"`
	CreatedAt string  `json:"createdAt"`
}

type Logo struct {
	Name string `json:"name"`
	Url  string `json:"url"`
}

type Order struct {
	Id           int        `db:"id"`
	Value        int        `db:"value"`
	Price        float32    `db:"price"`
	CreatedAt    *time.Time `db:"created_at"`
	CustomerID   string     `db:"customer_id"`
	CustomerName string     `db:"customer_name"`
	CreatedBy    string     `db:"created_by"`
	PlanCode     string     `db:"plan_code"`
}

type Stats struct {
	Appointment []StatValues `json:"appointment"`
	Canceled    []StatValues `json:"canceled"`
	Consumed    []StatValues `json:"consumed"`
}

type StatValues struct {
	OfficeId string  `json:"officeId"`
	Value    float32 `json:"value"`
}
