package repository

import (
	"gitlab.com/meeting-master/sdk/errors"
	"log"
	"time"
)

func (r repository) AllOrders(startDate, endDate time.Time) ([]OrderInfo, *errors.Error) {
	var result []OrderInfo
	if err := r.db.Select(&result, getOrdersByDate, startDate, endDate); err != nil {
		log.Println(err)
		return nil, errors.DBError(err)
	}

	if len(result) == 0 {
		return nil, nil
	}
	return result, nil
}


func (r repository) CustomerOrders(customerID string) ([]OrderInfo, *errors.Error) {
	var result []OrderInfo
	if err := r.db.Select(&result, getCustomerOrders, customerID); err != nil {
		log.Println(err)
		return nil, errors.DBError(err)
	}

	if len(result) == 0 {
		return nil, nil
	}
	return result, nil
}


func (r repository) GetPlanDetail(code string) (PlanResult, *errors.Error) {
	var result []PlanResult
	if err := r.db.Select(&result, getPlan, code); err != nil {
		log.Println(err)
		return PlanResult{}, errors.DBError(err)
	}

	if len(result) == 0 {
		return PlanResult{}, nil
	}
	return result[0], nil
}

func (r repository) MakeOrder(order OrderInfo) *errors.Error {
	if _, err := r.db.NamedQuery(insertOrder, order); err != nil {
		log.Println(err)
		return errors.DBError(err)
	}
	_, _ = r.db.Exec(UpdateValue, order.Value, order.CustomerID)
	return nil
}


func (r repository) UpdateValue(customerID string, value int) *errors.Error {
	_, err := r.db.Exec(UpdateValue, value, customerID)

	return errors.DBError(err)
}
