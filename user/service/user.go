package service

import (
	"context"
	"database/sql"
	"github.com/sethvargo/go-password/password"
	appContext "gitlab.com/meeting-master/sdk/context"
	"gitlab.com/meeting-master/sdk/errors"
	"gitlab.com/meeting-master/sdk/jwt"
	"gitlab.com/meeting-master/sdk/user/model"
	"gitlab.com/meeting-master/sdk/user/repository"
	"gitlab.com/meeting-master/sdk/utils"
	"golang.org/x/crypto/bcrypt"
	"log"
	"strconv"
)

func (us userService) Active(ctx context.Context) *errors.Error {
	ctxValue := appContext.ContextKeys(ctx)
	result, err := us.repository.UserByID(ctxValue.UserId)
	if err != nil {
		return err
	}
	if err := comparePassword(result.ActivationCode, ctxValue.ActivationCode); err != nil {
		return errors.DBError(err)
	}
	return us.repository.Active(ctxValue.UserId)
}

func (us userService) ChangePassword(ctx context.Context, request model.LoginRequest) *errors.Error {
	if !request.HasValidPasswordChange() {
		return errors.InvalidRequestData()
	}
	ctxValue := appContext.ContextKeys(ctx)
	result, err := us.repository.UserByID(ctxValue.UserId)
	if err != nil {
		return err
	}
	if err := comparePassword(result.Password, request.Password); err != nil {
		return errors.DBError(err)
	}
	hashNewPass, _ := utils.HashPassword(request.NewPassword)
	return us.repository.ChangePassword(ctxValue.UserId, hashNewPass)
}

func (us userService) Create(ctx context.Context, request model.UserCreateRequest) (string, string, *errors.Error) {
	if ok := us.authService.CanManage(ctx); !ok {
		return "", "", errors.Unauthorized()
	}
	if !request.IsValid() {
		return "", "", errors.InvalidRequestData()
	}
	ctxValue := appContext.ContextKeys(ctx)

	hashPass, _ := utils.HashPassword(request.Password)
	activationCode := strconv.Itoa(utils.GenerateCode(false))
	hashCode, _ := utils.HashPassword(activationCode)
	query := repository.UserCreateRequest{
		UserName:    request.UserName,
		Password:    hashPass,
		PhoneNumber: request.PhoneNumber,
		FirstName:   request.FirstName,
		LastName:    request.LastName,
		CreatedBy:   ctxValue.UserId,
		ResourceID: sql.NullString{
			String: ctxValue.ResourceId,
			Valid:  ctxValue.ResourceId != "",
		},
		ActivationCode: hashCode,
	}
	if request.AdminUser {
		query.BusinessCode = strconv.Itoa(utils.GenerateCode(false))
	}
	createdId, err := us.repository.CreateUser(query)
	if err != nil {
		return "", "", err
	}

	if us.mailEngine != nil {
		_, err = us.mailEngine.ActivationMessage(request.LastName, request.UserName, activationCode, request.Language)
		if err != nil {
			log.Println(err)
		}
	}

	return createdId, activationCode, nil
}

func (us userService) UpdateInfo(ctx context.Context, request model.UserUpdateRequest) *errors.Error {

	if !request.IsValid() {
		return errors.InvalidRequestData()
	}
	ctxValue := appContext.ContextKeys(ctx)

	if request.ID != ctxValue.UserId {
		return errors.Unauthorized()
	}
	query := repository.UserCreateRequest{
		UserName:    request.UserName,
		PhoneNumber: request.PhoneNumber,
		FirstName:   request.FirstName,
		LastName:    request.LastName,
		ID:          request.ID,
	}

	return  us.repository.ChangeUserInfo(query)
}

func (us userService) Grant(ctx context.Context, request model.GrantRequest) *errors.Error {
	if ok := us.authService.CanManage(ctx); !ok {
		return errors.Unauthorized()
	}
	return us.repository.GrantUser(request)
}

func (us userService) Login(request model.LoginRequest) (model.LoginResponse, *errors.Error) {
	if !request.HasValidLogin() {
		return model.LoginResponse{}, errors.InvalidRequestData()
	}
	result := us.repository.Login(request)

	if (result.CustomerId != nil && request.IsAdmin) || (result.CustomerId == nil && !request.IsAdmin) || comparePassword(result.Password, request.Password) != nil {
		return model.LoginResponse{}, errors.Unauthorized()
	}

	resource := ""
	if result.CustomerId != nil {
		resource = *result.CustomerId
	}

	tokenString, err := jwt.GenerateToken(result.ID, request.UserName, resource)
	if err != nil {
		return model.LoginResponse{}, errors.Unknown()
	}
	//if err = us.repository.StartSession(tokenString); err != nil {
	//	return model.LoginResponse{}, errors.InvalidRequestData()
	//}

	return model.LoginResponse{
		Token:        tokenString,
		FirstName:    result.FirstName,
		LastName:     result.LastName,
		UserName:     result.UserName,
		IsActive:     result.IsActive,
		CustomerId:   resource,
		BusinessCode: result.BusinessCode,
	}, nil
}

func (us userService) Logout(ctx context.Context) {
	ctxValues := appContext.ContextKeys(ctx)
	us.repository.EndSession(ctxValues.Token)
}

func (us userService) GenerateCode(ctx context.Context) (string, *errors.Error) {
	ctxValue := appContext.ContextKeys(ctx)
	result, _ := us.repository.UserByID(ctxValue.UserId)
	if len(result.UserName) == 0 {
		return "", errors.Unknown()
	}
	activationCode := strconv.Itoa(utils.GenerateCode(false))
	hashCode, _ := utils.HashPassword(activationCode)
	if err := us.repository.InsertActivationCode(ctxValue.UserId, hashCode); err != nil {
		return "", err
	}

	return activationCode, nil

}

func (us userService) User(ctx context.Context) (model.User, *errors.Error) {
	ctxValue := appContext.ContextKeys(ctx)
	v, err := us.repository.UserByID(ctxValue.UserId)
	if err != nil {
		return model.User{}, err
	}
	if v.UserName == "" {
		return model.User{}, nil
	}
	return model.User{
		ID:          v.ID,
		UserName:    v.UserName,
		FirstName:   v.FirstName,
		LastName:    v.LastName,
		IsActive:    v.IsActive,
		PhoneNumber: v.PhoneNumber,
	}, nil
}

func (us userService) Users(ctx context.Context) ([]model.User, *errors.Error) {
	if ok := us.authService.CanManage(ctx); !ok {
		return nil, errors.Unauthorized()
	}
	ctxValue := appContext.ContextKeys(ctx)

	result, err := us.repository.Users(ctxValue.ResourceId)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	if len(result) == 0 {
		return nil, nil
	}
	response := make([]model.User, len(result))
	for k, v := range result {
		response[k] = model.User{
			ID:          v.ID,
			UserName:    v.UserName,
			FirstName:   v.FirstName,
			LastName:    v.LastName,
			IsActive:    v.IsActive,
			PhoneNumber: v.PhoneNumber,
		}
	}
	return response, nil
}

func (us userService) Privileges(ctx context.Context, userID string) ([]model.Privilege, *errors.Error) {
	pvs, err := us.authService.Privileges(ctx, userID)
	if err != nil {
		return nil, err
	}
	res := make([]model.Privilege, len(pvs))
	for k, v := range pvs {
		res[k] = model.Privilege{
			RoleCode:   v.RoleCode,
			ResourceId: v.AccountId,
			OfficeId:   v.OfficeId,
			ServiceId:  v.ServiceId,
		}
	}
	return res, nil
}

func comparePassword(hashedPassword, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

func (us userService) ForgotPassword(email string) *errors.Error {
	result, _ := us.repository.UserByID(email)
	if result.UserName != email {
		return errors.ForgotPassword()
	}
	passwd, _ := password.Generate(6, 1, 0, false, false)
	hashPass, _ := utils.HashPassword(passwd)
	if us.mailEngine != nil {
		_, err := us.mailEngine.ForgotPassword(email, *result.FirstName, passwd)
		if err != nil {
			log.Println(err)
			return errors.ForgotPassword()
		}
		err = us.repository.ChangePassword(result.ID, hashPass)
		if err != nil {
			log.Println(err)
			return errors.ForgotPassword()
		}
	}
	return nil
}
