package main

import (
	"fmt"
	"github.com/appleboy/go-fcm"
	"github.com/joho/godotenv"
	"gitlab.com/meeting-master/sdk/connector/pgsql"
	"gitlab.com/meeting-master/sdk/errors"
	"gitlab.com/meeting-master/sdk/notification/engine"
	"gitlab.com/meeting-master/sdk/notification/model"
	"gitlab.com/meeting-master/sdk/notification/repository"
	"log"
	"os"
)

const (
	titleFr    = "Rappel de rendez-vous"
	bodyDayFr  = "Demain %v à %v"
	bodyHourFr = "Aujourd'hui %v à %v"
	titleEn    = "Appointment Recall"
	bodyDayEn  = "Tomorrow %v at %v"
	bodyHourEn = "Today %v at %v"
	titleAnFr  = "Action sur annonce"
	bodyAnFr   = "Une action a été ménée sur votre récente annonce. Allez dans mes annonces pour voir le status"
	titleAnEn  = "Action on announce"
	bodyAnEn   = "Action has been taken on your recent announce. Go to my announce to see the status"
)

func main() {
	if err := godotenv.Load(); err != nil {
		panic(err)
	}
	client, err := fcm.NewClient(os.Getenv("FCM_KEY"))
	if err != nil {
		panic(err)
	}

	pg, err := pgsql.Open()
	if err != nil {
		panic(err)
	}
	defer pg.Close()

	repo := repository.NewNotificationRepository(pg)

	notifEngine := engine.Init(repo)
	args := os.Args[1:]

	var messageToSend []model.Message
	var er *errors.Error

	if len(args) == 0 {
		log.Println("isAnnounce")
		an, er := notifEngine.Announces()
		if er != nil {
			log.Println(er)
			return
		}

		if an != nil {
			sendAnnounce(client, an)
		}
		return
	}

	isHour := len(args) != 0 && args[0] == "H"

	if isHour {
		messageToSend, er = notifEngine.HourMessages()
	} else {
		messageToSend, er = notifEngine.DayMessages()
	}

	if er != nil {
		panic(er)
	}

	if messageToSend != nil {
		send(client, messageToSend, isHour)
	}


}

func send(client *fcm.Client, messagesToSend []model.Message, isHour bool) {
	//results := make([]model.Message, len(messagesToSend))
	for _, m := range messagesToSend {
		msg := makeNotification(m, isHour)
		_, _ = client.Send(msg)
	}
}

func makeNotification(m model.Message, isHour bool) *fcm.Message {
	title := titleFr
	body := bodyDayFr

	if m.Lang != "fr" {
		title = titleEn
		body = bodyDayEn
	}

	if isHour {
		body = bodyHourFr
		if m.Lang != "fr" {
			body = bodyHourEn
		}
	}
	notif := fcm.Notification{
		Title: title,
		Body:  fmt.Sprintf(body, m.Hour, m.Location),
	}
	msg := &fcm.Message{
		To:           m.Token,
		Notification: &notif,
	}
	return msg
}

func sendAnnounce(client *fcm.Client, an []model.Message) {
	for _, m := range an {
		msg := announceNotification(m)
		_, _ = client.Send(msg)
	}
}

func announceNotification(m model.Message) *fcm.Message {
	title := titleAnFr
	body := bodyAnFr

	if m.Lang != "fr" {
		title = titleAnEn
		body = bodyAnEn
	}

	notif := fcm.Notification{
		Title: title,
		Body:  body,
	}
	msg := &fcm.Message{
		To:           m.Token,
		Notification: &notif,
	}
	return msg
}
