package repository

import (
	"gitlab.com/meeting-master/sdk/connector/pgsql"
	"gitlab.com/meeting-master/sdk/errors"
	"gitlab.com/meeting-master/sdk/location/model"
)

type LocationRepository interface {
	UpsertCountry(request Country) (string, *errors.Error)

	UpsertTown(request model.TownRequest) (string, *errors.Error)

	UpsertDistrict(request model.DistrictRequest) (string, *errors.Error)

	Countries() ([]Country, *errors.Error)
	
	Towns(countryID string) ([]Town, *errors.Error)

	Districts(townID string) ([]District, *errors.Error)

	Localizations(byTown bool) ([]LocationResult, *errors.Error)
}

type repository struct {
	db *pgsql.DB
}


func NewLocationRepository(db *pgsql.DB) LocationRepository {
	return repository{db: db}
}
