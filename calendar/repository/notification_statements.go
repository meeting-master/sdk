package repository

const (
	insertDevice = `INSERT INTO common.user_device(
							user_id,
							token,
							lang)
					VALUES(	:userID,
							:token,
							:lang)`

	updateDevice = `UPDATE common.user_device SET token = :token, lang = :lang WHERE user_id = :userID `

	getDeviceToken = `SELECT token FROM common.user_device WHERE user_id = $1`
)
