package repository

import (
	"gitlab.com/meeting-master/sdk/connector/pgsql"
	"gitlab.com/meeting-master/sdk/errors"
	"gitlab.com/meeting-master/sdk/messaging/model"
)

type MessagingRepository interface {
	InsertMessage(message model.Message)  (string, *errors.Error)
	MessagesToSent()  ([]model.Message, *errors.Error)
	UpdateMessage(messages []model.Message) *errors.Error
}

type repository struct {
	db *pgsql.DB
}


func NewMessagingRepository(db *pgsql.DB) MessagingRepository {
	return repository{db: db}
}

