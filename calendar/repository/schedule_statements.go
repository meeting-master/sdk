package repository

const (
	insertSchedule = `INSERT INTO common.schedule(office_id, service_id, work_day, start_time, end_time, duration, place, created_by)
					VALUES(:office_id, :service_id, :work_day, :start_time, :end_time, :duration, :place, :created_by)`

	deleteSchedule = `Delete FROM common.schedule WHERE office_id = $1 AND service_id = $2`

	getSchedule = `SELECT id, office_id, service_id, work_day, start_time, end_time, duration, place FROM common.schedule 
					WHERE office_id::TEXT = $1 AND service_id::TEXT = $2
					ORDER BY work_day, start_time `


	calendar = `SELECT ts.id, 
				ts.start_time,
				ts.end_time, 
				ts.scheduled_appointments,
				ts.consumed_appointments,
				ts.user_id,
				s."name" as "service_name"
				FROM common.time_slots ts
				INNER JOIN common.service s on s.id = ts.service_id
				WHERE ts.is_archived = false AND ts.is_canceled = false
				and ts.start_time between $1 and $2
				and ts.office_id = $3 AND ts.service_id = $4
				ORDER BY ts.start_time`
)
