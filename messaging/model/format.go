package model

import (
	"bytes"
	"html/template"
)

func  ParseActivationTemplate(lang string, data interface{}) (string, string, error) {
	t, err := template.New("easy").Parse(ActivationMessage)
	if err != nil {
		return "" ,"", err
	}
	buf := new(bytes.Buffer)
	if err = t.Execute(buf, data); err != nil {
		return "", "", err
	}

	return buf.String(), ActivationMessageSubject, nil
}

func  ParseAppointmentTemplate(data interface{}) (string, string, error) {
	t, err := template.New("appointment").Parse(AppointmentMessage)
	if err != nil {
		return "" ,"", err
	}
	buf := new(bytes.Buffer)
	if err = t.Execute(buf, data); err != nil {
		return "", "", err
	}

	return buf.String(), AppointmentMessageSubject, nil
}

func  ParseCancelTemplate(data interface{}) (string, string, error) {
	t, err := template.New("cancelTemplate").Parse(CancelMessage)
	if err != nil {
		return "" ,"", err
	}
	buf := new(bytes.Buffer)
	if err = t.Execute(buf, data); err != nil {
		return "", "", err
	}

	return buf.String(), CancelSubject, nil
}

func  ParseForgotPasswordTemplate(data interface{}) (string, string, error) {
	t, err := template.New("forgot").Parse(ForgotMessage)
	if err != nil {
		return "" ,"", err
	}
	buf := new(bytes.Buffer)
	if err = t.Execute(buf, data); err != nil {
		return "", "", err
	}

	return buf.String(), ForgotSubject, nil
}