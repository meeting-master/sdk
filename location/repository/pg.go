package repository

import (
	"gitlab.com/meeting-master/sdk/errors"
	"gitlab.com/meeting-master/sdk/location/model"
)

func (r repository) UpsertCountry(request Country) (string, *errors.Error) {
	query := insertNewCountry
	if request.ID != "" {
		query = updateCountry
	}
	row, err := r.db.NamedQuery(query, request)
	if err != nil {
		return "", errors.DBError(err)
	}

	var ID string
	if row.Next() {
		row.Scan(&ID)
	}
	return ID, errors.DBError(err)
}

func (r repository) UpsertTown(request model.TownRequest) (string, *errors.Error) {
	if !request.IsValid() {
		return "", errors.InvalidRequestData()
	}
	query := insertNewTown
	if request.ID != "" {
		query = updateTown
	}
	row, err := r.db.NamedQuery(query, request)
	if err != nil {
		return "", errors.DBError(err)
	}
	var ID string
	if row.Next() {
		row.Scan(&ID)
	}
	return ID, errors.DBError(err)
}

func (r repository) UpsertDistrict(request model.DistrictRequest) (string, *errors.Error) {
	if !request.IsValid() {
		return "", errors.InvalidRequestData()
	}
	query := insertNewDistrict
	if request.ID != "" {
		query = updateDistrict
	}

	row, err := r.db.NamedQuery(query, request)
	if err != nil {
		return "", errors.DBError(err)
	}
	var ID string
	if row.Next() {
		row.Scan(&ID)
	}
	return ID, errors.DBError(err)
}

func (r repository) Countries() ([]Country, *errors.Error) {
	var result []Country
	err := r.db.Select(&result, getCountries)
	return result, errors.DBError(err)
}


func (r repository) Towns(countryID string) ([]Town, *errors.Error) {
	var result []Town
	var err error
	if countryID != "" {
		err = r.db.Select(&result, getTownsByCountry, countryID)
	} else {
		err = r.db.Select(&result, getTowns)
	}
	return result, errors.DBError(err)
}


func (r repository) Localizations(byTown bool) ([]LocationResult, *errors.Error) {
	var result []LocationResult
	query := getDistrictLocation
	if byTown {
		query = getTownLocation
	}
	err := r.db.Select(&result, query)
	return result, errors.DBError(err)
}

func (r repository) Districts(townID string) ([]District, *errors.Error) {
	var result []District
	var err error
	if townID != "" {
		err = r.db.Select(&result, getDistrictsByTown, townID)
	} else {
		err = r.db.Select(&result, getDistricts)
	}
	return result, errors.DBError(err)
}
