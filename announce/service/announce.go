package service

import (
	"context"
	"gitlab.com/meeting-master/sdk/announce/model"
	appContext "gitlab.com/meeting-master/sdk/context"
	"gitlab.com/meeting-master/sdk/errors"
	"time"
)

func (a announceService) Upsert(request model.AnnounceRequest) (string, *errors.Error) {
	if !request.IsValid() {
		return "", errors.InvalidRequestData()
	}
	dbRequest := announceRequestToAnnounceDB(request)
	return a.repo.Upsert(dbRequest)
}

func (a announceService) Delete(announceID, ownerID string) *errors.Error {
	if announceID == "" || ownerID == "" {
		return errors.InvalidRequestData()
	}

	announce, err := a.repo.AnnounceById(announceID)
	if err != nil {
		return err
	}

	if announce.OwnerID != ownerID {
		return errors.Unauthorized()
	}

	return a.repo.Delete(announceID)
}

func (a announceService) AnnounceById(announceID string) (model.Announce, *errors.Error) {
	if announceID == "" {
		return model.Announce{}, errors.InvalidRequestData()
	}

	res, err := a.repo.AnnounceById(announceID)
	if err != nil {
		return model.Announce{}, err
	}
	return announceDbToAnnounce(res)[0], nil
}

func (a announceService) AnnouncesByTag(tags string, townID string) ([]model.Announce, *errors.Error) {
	res, err := a.repo.AnnouncesByTag(tags, townID)
	if err != nil {
		return nil, err
	}

	return announceDbToAnnounce(res...), nil
}

func (a announceService) AnnouncesByOwner(ownerID string) ([]model.Announce, *errors.Error) {
	if ownerID == "" {
		return nil, errors.InvalidRequestData()
	}

	res, err := a.repo.AnnouncesByOwner(ownerID)
	if err != nil {
		return nil, err
	}

	return announceDbToAnnounce(res...), nil
}

func (a announceService) LastAnnounces() ([]model.Announce, *errors.Error) {
	start := time.Now().UTC().AddDate(0, 0, -7)
	res, err := a.repo.AnnouncesSince(start)
	if err != nil {
		return nil, err
	}

	return announceDbToAnnounce(res...), nil
}

func (a announceService) AnnouncesToValidate(ctx context.Context) ([]model.Announce, *errors.Error) {
	if a.authService == nil || !a.authService.CanManage(ctx) {
		return nil, errors.Unauthorized()
	}

	res, err := a.repo.AnnouncesToValidate()
	if err != nil {
		return nil, err
	}

	return announceDbToAnnounce(res...), nil
}

func (a announceService) Validate(ctx context.Context, request model.ValidationRequest) *errors.Error {
	if a.authService == nil || !a.authService.CanManage(ctx) {
		return errors.Unauthorized()
	}
	userID := appContext.ContextKeys(ctx).UserId
	return a.repo.Validate(request.Valid, request.AnnounceID, request.Comment, userID)
}
