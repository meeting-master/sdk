package service

import (
	"context"
	"gitlab.com/meeting-master/sdk/calendar/repository"
	"time"

	"gitlab.com/meeting-master/sdk/calendar/model"
	appContext "gitlab.com/meeting-master/sdk/context"
	"gitlab.com/meeting-master/sdk/errors"
)

func (cs calendarService) CreateTimeSlot(ctx context.Context, request model.TimeSlotRequest) *errors.Error {
	if !request.IsValid() {
		return errors.MissingMandatory()
	}
	if !cs.authService.CanCreateOffice(ctx) {
		return errors.Unauthorized()
	}
	createdBy := appContext.ContextKeys(ctx).UserId
	var timeSlots []repository.TimeSlotInfo
	calendarHistory := make([]repository.CalendarHistoryDB, len(request.ServiceIDs))
	for k, serviceID := range request.ServiceIDs {
		ts, err := cs.processTS(request.OfficeID, serviceID, createdBy, request.StartDate, request.EndDate)
		if err != nil {
			return err
		}
		timeSlots = append(timeSlots, ts...)
		calendarHistory[k] = repository.CalendarHistoryDB{
			OfficeID:  request.OfficeID,
			ServiceID: serviceID,
			CreatedBy: createdBy,
			StartDate: request.StartDate,
			EndDate:   request.EndDate,
		}
	}

	return cs.repository.InsertTimeSlots(timeSlots, calendarHistory)
}

func (cs calendarService) processTS(officeID, serviceID, createdBy string, StartDate, EndDate time.Time) ([]repository.TimeSlotInfo, *errors.Error) {
	count, err := cs.repository.CountTimeSlot(officeID, serviceID, StartDate, EndDate)
	if err != nil {
		return nil, err
	}
	if count != 0 {
		return nil, errors.TsAlready()
	}
	schedules, err := cs.repository.ItemSchedule(officeID, serviceID)
	if err != nil {
		return nil, err
	}
	var timeSlots []repository.TimeSlotInfo
	daySchedule := filterByDay(schedules)
	currentDate := time.Date(StartDate.Year(), StartDate.Month(), StartDate.Day(), 0, 0, 0, 0, StartDate.Location())
	//generate ts
	//date is incremented and ts are created by gathering week day number of the date
	for currentDate.Before(EndDate) {
		currentDay := int(currentDate.Weekday())
		frames := daySchedule[currentDay]
		for _, f := range frames {
			currentTimer := f.StartTime
			for currentTimer < f.EndTime {
				timeSlot := repository.TimeSlotInfo{
					OfficeID:  officeID,
					ServiceID: serviceID,
					Scheduled: f.Place,
					StartTime: currentDate.Add(time.Minute * time.Duration(currentTimer)),
					EndTime:   currentDate.Add(time.Minute * time.Duration(currentTimer+f.Duration)),
					UserId:    "",
					CreatedBy: createdBy,
				}
				timeSlots = append(timeSlots, timeSlot)
				currentTimer += f.Duration
			}
		}
		currentDate = currentDate.AddDate(0, 0, 1)
	}
	return timeSlots, nil
}

func filterByDay(schedules []repository.ScheduleDB) map[int][]model.TimeFrame {
	daysConfig := make(map[int][]model.TimeFrame)
	for _, v := range schedules {
		tf := model.TimeFrame{
			StartTime: v.StartTime,
			EndTime:   v.EndTime,
			Duration:  v.Duration,
			Place:     v.Place,
		} //intParseTimeFrame(v.StartTime, v.EndTime)
		config := daysConfig[v.WorkDay]
		config = append(config, tf)
		daysConfig[v.WorkDay] = config
	}
	return daysConfig
}

/*
func (cs calendarService) CreateTimeSlotR(ctx context.Context, request model.TimeSlotRequest) *errors.Error {
	if !request.IsValid() {
		return errors.InvalidRequestData()
	}
	config, duration, err := cs.itemSchedule(request.ItemID)
	if err != nil {
		return err
	}
	var timeSlots []time.Time
	currentDate := request.StartDate
	for currentDate.Before(request.EndDate) {
		currentDay := int(currentDate.Weekday())
		ts, _ := createTS(currentDate, config[currentDay], duration)
		timeSlots = append(timeSlots, ts...)
		currentDate = currentDate.AddDate(0, 0, 1)
	}
	return nil
}
*/
//if ts si linked to the user, he can modify it other wise error is return
func (cs calendarService) UpdateTimeSlot(ctx context.Context, request model.TimeSlotUpdateRequest) *errors.Error {
	if !request.IsValid() {
		return errors.InvalidRequestData()
	}
	ts, err := cs.repository.TimeSlotById(request.Id)
	if err != nil {
		return err
	}
	ctxValue := appContext.ContextKeys(ctx)
	if (ts.UserId != nil && *ts.UserId != ctxValue.UserId) || !cs.authService.CanCreateCalendar(ctx) {
		return errors.Unauthorized()
	}
	return cs.repository.UpdateTimeSlot(request, ctxValue.UserId)
}

func (cs calendarService) CalendarCreationHistory(officeID, serviceID string) ([]model.CalendarHistory, *errors.Error) {
	if officeID == "" || serviceID == "" {
		return nil, errors.InvalidRequestData()
	}
	history, err := cs.repository.CalendarHistory(officeID, serviceID)

	if err != nil {
		return nil, err
	}
	res := make([]model.CalendarHistory, len(history))

	for k, v := range history {
		res[k] = model.CalendarHistory{
			OfficeID:  v.OfficeID,
			ServiceID: v.ServiceID,
			StartDate: v.StartDate,
			EndDate:   v.EndDate,
			CreatedAt: v.CreatedAt,
		}
	}
	return res, nil
}

func (cs calendarService) TimeSlotsByDateRange(officeID, serviceID string, startDate, endDate time.Time) ([]model.TimeSlot, *errors.Error) {
	if  officeID == "" || serviceID == "" || startDate.IsZero() || endDate.IsZero() || startDate.After(endDate) {
		return nil, errors.InvalidRequestData()
	}
	result, err := cs.repository.TimeSlotsByRange(officeID, serviceID, startDate, endDate)
	if err != nil {
		return nil, err
	}
	if result == nil || len(result) == 0 {
		return nil, nil
	}
	return convertToTs(result), nil
}

func (cs calendarService) TimeSlotsByDates(officeID, serviceID string, dates ...time.Time) ([]model.TimeSlot, *errors.Error) {
	if  officeID == "" || serviceID == "" || len(dates) == 0 {
		return nil, errors.InvalidRequestData()
	}
	result, err := cs.repository.TimeSlotsByDates(officeID, serviceID, dates...)
	if err != nil {
		return nil, err
	}
	if result == nil || len(result) == 0 {
		return nil, nil
	}
	return convertToTs(result), nil

}

func convertToTs(result []repository.TimeSlotResult) []model.TimeSlot {
	timeSlots := make([]model.TimeSlot, len(result))
	for k, v := range result {
		ts := model.TimeSlot{
			Id:            v.Id,
			StartTime:     v.StartTime.Format(timeLayout),
			StartDateTime: v.StartTime,
			EndTime:       v.EndTime.Format(timeLayout),
			EndDateTime:   v.EndTime,
			IsPassed:      v.StartTime.Before(time.Now()),
			IsAvailable:   v.Consumed < v.Scheduled,
			Available:     v.Scheduled - v.Consumed,
			Consumed:      v.Consumed,
		}
		timeSlots[k] = ts
	}

	return timeSlots
}


