package repository

import (
	"database/sql"
	"github.com/lib/pq"
	"time"
)

type CustomerResult struct {
	Id          string         `db:"id"`
	Alias       sql.NullString `db:"alias"`
	Name        string         `db:"name"`
	Activities  string         `db:"activities"`
	PhoneNumber sql.NullString `db:"phone_number"`
	WebSite     sql.NullString `db:"web_site"`
	Tags        sql.NullString `db:"tags"`
	Rating      float32        `db:"rating"`
	Value       int32          `db:"value"`
	LogoUrl     sql.NullString `db:"logo_url"`
}

type CustomerTagResult struct {
	CustomerId string         `db:"customer_id"`
	Activities string         `db:"activities"`
	OfficeId   string         `db:"office_id"`
	Longitude  float64        `db:"longitude"`
	Latitude   float64        `db:"latitude"`
	DistrictId sql.NullString `db:"district_id"`
	Rating     float32        `db:"rating"`
	LogoUrl    sql.NullString `db:"logo_url"`
}

type OfficeDB struct {
	Id            string         `db:"id"`
	Alias         string         `db:"alias"`
	Name          string         `db:"name"`
	StreetNumber  string         `db:"street_number"`
	StreetName    string         `db:"street_name"`
	ZipCode       string         `db:"zip_code"`
	PhoneNumber   string         `db:"phone_number"`
	WebSite       string         `db:"web_site"`
	Email         string         `db:"email"`
	Longitude     float64        `db:"longitude"`
	Latitude      float64        `db:"latitude"`
	DistrictId    string         `db:"district_id"`
	Location      string         `db:"location"`
	CustomerId    string         `db:"customer_id"`
	CustomerName  string         `db:"customer_name"`
	CustomerAlias string         `db:"customer_alias"`
	IsArchived    bool           `db:"is_archived"`
	ServiceIDs    pq.StringArray `db:"service_ids"`
}

type ServiceResult struct {
	Id            string `db:"id"`
	Alias         string `db:"alias"`
	Name          string `db:"name"`
	IsArchived    bool   `db:"is_archived"`
	CustomerId    string `db:"customer_id"`
	CustomerName  string `db:"customer_name"`
	CustomerAlias string `db:"customer_alias"`
}

type OfficeServiceResult struct {
	Id           string `db:"id"`
	OfficeId     string `db:"office_id"`
	ServiceId    string `db:"service_id"`
	ServiceName  string `db:"service_name"`
	ServiceAlias string `db:"service_alias"`
}

type OfficeServiceInput struct {
	OfficeId  string
	ServiceId string
}

type AppointmentStreamInfo struct {
	Id          string     `db:"id"`
	CreatedAt   time.Time  `db:"created_at"`
	CanceledAt  *time.Time `db:"canceled_at"`
	ConsumedAt  *time.Time `db:"consumed_at"`
	ServiceId   string     `db:"service_id"`
	ServiceName string     `db:"service_name"`
	OfficeId    string     `db:"office_id"`
	OfficeName  string     `db:"office_name"`
}

type RatingResult struct {
	Id        string  `db:"id"`
	Rating    float32 `db:"rating"`
	CreatedAt string  `db:"created_at"`
	Comment   string  `db:"comment"`
}

type LogoResult struct {
	Name string `db:"name"`
	Url  string `db:"logo_url"`
}

type PlanResult struct {
	Code      string     `db:"code"`
	Name      string     `db:"name"`
	Value     int        `db:"value"`
	Price     float32    `db:"price"`
	StartedAt *time.Time `db:"started_at"`
	EndAt     *time.Time `db:"end_at"`
}

type OrderInfo struct {
	Id           int        `db:"id"`
	Value        int        `db:"value"`
	Price        float32    `db:"price"`
	CreatedAt    *time.Time `db:"created_at"`
	CustomerID   string     `db:"customer_id"`
	CustomerName string     `db:"customer_name"`
	CreatedBy    string     `db:"created_by"`
	PlanCode     string     `db:"plan_code"`
}

type StatResult struct {
	OfficeID string  `db:"office_id"`
	Count    float32 `db:"count"`
}
