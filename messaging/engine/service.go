package engine

import (
	"gitlab.com/meeting-master/sdk/errors"
	"gitlab.com/meeting-master/sdk/messaging/model"
	"log"
	"os"
	"regexp"
)
var rxEmail = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

func (e engine) ActivationMessage(name, to, code, lang string) (string, *errors.Error) {
	if !rxEmail.MatchString(to) {
		return "", errors.MissingMandatory()
	}
	data := model.AccountNotification{
		Name:        name,
		MailAddress: to,
		Code:        code,
	}
	body, subject, err := model.ParseActivationTemplate(lang, data)
	if err != nil {
		log.Println(err)
		return "", errors.DBError(err)
	}
	return e.persist(os.Getenv("SMTP_USER"), to, body, subject)
}

func (e engine) AppointmentNotification(data model.AppointmentNotification) (string, *errors.Error) {
	if !rxEmail.MatchString(data.To) {
		return "", errors.MissingMandatory()
	}
	body, subject, err := model.ParseAppointmentTemplate(data)
	if err != nil {
		log.Println(err)
		return "", errors.DBError(err)
	}

	return e.persist(os.Getenv("SMTP_USER"), data.To, body, subject)
}


func (e engine) CancelAppointmentNotification(data model.AppointmentNotification) (string, *errors.Error) {
	if !rxEmail.MatchString(data.To) {
		return "", errors.MissingMandatory()
	}
	body, subject, err := model.ParseCancelTemplate(data)
	if err != nil {
		log.Println(err)
		return "", errors.DBError(err)
	}
	return e.persist(os.Getenv("SMTP_USER"), data.To, body, subject)

}

func (e engine) persist(from, to, body, subject string) (string, *errors.Error) {
	msg := model.Message{
		From:    from,
		To:      to,
		Body:    body,
		Subject: subject,
	}

	return e.repository.InsertMessage(msg)
}


func (e engine) Messages() ([]model.Message, *errors.Error) {
	return e.repository.MessagesToSent()
}

func (e engine) UpdateMessage(messages []model.Message) *errors.Error {
	return e.repository.UpdateMessage(messages)
}

func (e engine) ForgotPassword(to, name, password string) (string, *errors.Error) {
	if !rxEmail.MatchString(to) {
		return "", errors.MissingMandatory()
	}
	data := model.AccountNotification{
		Name:        name,
		MailAddress: to,
		Code:        password,
	}
	body, subject, err := model.ParseForgotPasswordTemplate(data)
	if err != nil {
		log.Println(err)
		return "", errors.DBError(err)
	}
	return e.persist(os.Getenv("SMTP_USER"), to, body, subject)
}
