package repository

import (
	"database/sql"
	"github.com/lib/pq"
	"time"
)

type AnnounceDB struct {
	ID          string         `db:"id"`
	Title       string         `db:"title"`
	Body        sql.NullString `db:"body"`
	Price       float32        `db:"price"`
	Published   bool           `db:"published"`
	Pictures    pq.StringArray `db:"pictures"`
	OwnerID     string         `db:"owner_id"`
	Details     sql.NullString `db:"details"`
	DistrictId  sql.NullString `db:"district_id"`
	PhoneNumber sql.NullString `db:"phone_number"`
	Location    sql.NullString `db:"location"`
	ExpireAt    time.Time      `db:"expire_at"`
	CreatedAt   time.Time      `db:"created_at"`
	Status      string         `db:"status"`
	Comment     sql.NullString `db:"comment"`
}

type AnnounceDBRequest struct {
	ID          string         `db:"id"`
	Title       string         `db:"title"`
	Body        string         `db:"body"`
	Price       float32        `db:"price"`
	Published   bool           `db:"published"`
	Pictures    pq.StringArray `db:"pictures"`
	OwnerID     string         `db:"owner_id"`
	Details     string         `db:"details"`
	DistrictId  string         `db:"district_id"`
	PhoneNumber string         `db:"phone_number"`
	Location    string         `db:"location"`
	ExpireAt    time.Time      `db:"expire_at"`
	CreatedAt   time.Time      `db:"created_at"`
	Status      string         `db:"status"`
	Comment     string         `db:"comment"`
}
