package service

import (
	"context"
	"gitlab.com/meeting-master/sdk/auth/model"
	"gitlab.com/meeting-master/sdk/auth/repository"
	"gitlab.com/meeting-master/sdk/errors"
)

type AuthService interface {
	CanManage(ctx context.Context) bool
	CanCreateOffice(ctx context.Context) bool
	CanCreateService(ctx context.Context) bool
	CanCreateCalendar(ctx context.Context) bool
	Privileges(ctx context.Context, userID string) ([]model.Privilege, *errors.Error)
}

type authService struct {
	repository repository.AuthRepository
}

func NewAuthService(repo repository.AuthRepository) AuthService {
	return &authService{repository: repo}
}
