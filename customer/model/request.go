package model

const (
	tagsMaxCount = 5
	tagMaxLength = 20
)

type CustomerRequest struct {
	Id          string   `json:"id"`
	Alias       string   `json:"alias"`
	Name        string   `json:"name"`
	PhoneNumber string   `json:"phoneNumber"`
	WebSite     string   `json:"webSite"`
	LogoUrl     string   `json:"logoUrl"`
	Language    string   `json:"language"`
	UserName    string   `json:"userName"`
	Password    string   `json:"password"`
	Activities  string   `json:"activities"`
	Tags        []string `json:"tags"`
	TagString   string
}

func (r *CustomerRequest) IsValid() bool {
	return r.Name != "" && len(r.Tags) <= tagsMaxCount && r.Activities != ""
}

func (r *CustomerRequest) IsTagsValid() bool {
	for _, tag := range r.Tags {
		if len(tag) > tagMaxLength {
			return false
		}
	}
	return true
}

type OfficeRequest struct {
	Id           string   `json:"id"`
	Alias        string   `json:"alias"`
	Name         string   `json:"name"`
	StreetNumber string   `json:"streetNumber"`
	StreetName   string   `json:"streetName"`
	ZipCode      string   `json:"zipCode"`
	PhoneNumber  string   `json:"phoneNumber"`
	WebSite      string   `json:"webSite"`
	Email        string   `json:"email"`
	Longitude    float64  `json:"longitude"`
	Latitude     float64  `json:"latitude"`
	DistrictId   string   `json:"districtId"`
	CustomerId   string   `json:"customerId"`
	IsArchived   bool     `json:"isArchived"`
	ServiceIDs   []string `json:"serviceIds"`
}

func (or *OfficeRequest) IsValid() bool {
	return or.Name != "" &&
		or.DistrictId != "" &&
		or.CustomerId != "" &&
		len(or.ServiceIDs) > 0
}

type ServiceRequest struct {
	Id         string `json:"id"`
	Alias      string `json:"alias"`
	Name       string `json:"name"`
	IsArchived bool   `json:"isArchived"`
	CustomerId string `json:"customerId"`
}

func (sr *ServiceRequest) IsValid() bool {
	return len(sr.Name) > 0 && len(sr.CustomerId) > 0
}

type ItemRequest struct {
	OfficeId   string   `json:"officeId"`
	ServiceIds []string `json:"serviceIds"`
}

func (r *ItemRequest) IsValid() bool {
	return len(r.OfficeId) > 0 && len(r.ServiceIds) > 0
}

type DeleteRequest struct {
	Id string
}

type StreamRequest struct {
	CustomerId string `json:"customerId"`
	StartDate  string `json:"startDate"`
	EndDate    string `json:"endDate"`
}

func (sq *StreamRequest) IsValid() bool {
	return sq.CustomerId != "" && sq.StartDate != "" && sq.EndDate != ""
}

type OrderRequest struct {
	CustomerID string `json:"customerId"`
	UserID     string `json:"userId"`
	PlanCode   string `json:"endDate"`
	IsAdmin    bool   `json:"isAdmin"`
}

func (oq *OrderRequest) IsValid() bool {
	return oq.CustomerID != "" && oq.PlanCode != ""
}
