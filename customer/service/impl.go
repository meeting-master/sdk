package service

import (
	"context"
	"gitlab.com/meeting-master/sdk/customer/model"
	"gitlab.com/meeting-master/sdk/customer/repository"
	"gitlab.com/meeting-master/sdk/errors"
)

func (cs *customerService) UpsertService(ctx context.Context, request model.ServiceRequest) (string, *errors.Error) {
	if !request.IsValid() {
		return "", errors.InvalidRequestData()
	}
	if !cs.authService.CanCreateService(ctx) {
		return "", errors.Unauthorized()
	}
	return cs.repository.UpsertService(request)
}

func (cs *customerService) ServicesByCustomer(customerId string) ([]model.Service, *errors.Error) {
	if len(customerId) == 0 {
		return nil, errors.InvalidRequestData()
	}

	result, err := cs.repository.ServicesByCustomer(customerId)
	if err != nil {
		return nil, err
	}
	if result == nil || len(result) == 0 {
		return nil, nil
	}
	return cs.serviceResultToService(result), nil
}

func (cs *customerService) DeleteService(id string) (string, *errors.Error) {
	return cs.repository.DeleteService(id)
}

func (cs *customerService) DeleteServicesByCustomer(customerId string) (string, *errors.Error) {
	return cs.repository.DeleteServicesByCustomer(customerId)
}

func (cs *customerService) serviceResultToService(rawDatas []repository.ServiceResult) []model.Service {

	services := make([]model.Service, len(rawDatas))
	for index, rawData := range rawDatas {
		services[index] = model.Service{
			Id:            rawData.Id,
			Alias:         rawData.Alias,
			Name:          rawData.Name,
			CustomerId:    rawData.CustomerId,
			CustomerName:  rawData.CustomerName,
			CustomerAlias: rawData.CustomerAlias,
			IsArchived:    rawData.IsArchived,
		}
	}
	return services
}

func (cs *customerService) officeServiceResultToOfficeService(rawDatas []repository.OfficeServiceResult) []model.Item {

	items := make([]model.Item, len(rawDatas))
	for index, rawData := range rawDatas {
		items[index] = model.Item{
			Id:           rawData.Id,
			OfficeId:     rawData.OfficeId,
			ServiceId:    rawData.ServiceId,
			ServiceName:  rawData.ServiceName,
			ServiceAlias: rawData.ServiceAlias,
		}
	}
	return items
}

func (cs *customerService) ServicesByOffice(officeId string) ([]model.Service, *errors.Error) {
	if len(officeId) == 0 {
		return nil, errors.InvalidRequestData()
	}

	result, err := cs.repository.ServicesByOffice(officeId)
	if err != nil {
		return nil, err
	}
	if result == nil || len(result) == 0 {
		return nil, nil
	}
	services := make([]model.Service, len(result))
	for index, rawData := range result {
		services[index] = model.Service{
			Id:            rawData.Id,
			Alias:         rawData.Alias,
			Name:          rawData.Name,
			IsArchived:    rawData.IsArchived,
			CustomerId:    rawData.CustomerId,
			CustomerName:  rawData.CustomerName,
			CustomerAlias: rawData.CustomerAlias,
		}
	}
	return services, nil
}
