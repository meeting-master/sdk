package model

type AnnounceRequest struct {
	ID          string   `json:"id"`
	Title       string   `json:"title"`
	Body        string   `json:"body"`
	Price       float32  `json:"price"`
	Published   bool     `json:"published"`
	Pictures    []string `json:"pictures"`
	OwnerID     string   `json:"ownerId"`
	Details     string   `json:"details"`
	DistrictID  string   `json:"districtId"`
	PhoneNumber string   `json:"phoneNumber"`
}

func (ar AnnounceRequest) IsValid() bool {
	return ar.Title != "" && ar.OwnerID != ""
}

type ValidationRequest struct {
	AnnounceID string `json:"announceId"`
	Comment    string `json:"comment"`
	Valid      bool   `json:"valid"`
}

func (vr ValidationRequest) IsValid() bool {
	return vr.AnnounceID != ""
}
