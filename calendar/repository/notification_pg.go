package repository

import (
	"gitlab.com/meeting-master/sdk/errors"
)

func (r repository) GetUserDevice(userID string) (string, *errors.Error) {
	rows, err := r.db.Query(getDeviceToken, userID)
	if err != nil {
		return "", errors.DBError(err)
	}
	defer rows.Close()

	var token string

	for rows.Next() {
		if err := rows.Scan(&token); err != nil {
			return "", errors.DBError(err)
		}
	}

	return token, errors.DBError(err)
}

func (r repository) UpsertUserDevice(userID, token, lang string) *errors.Error {
	currentToken, er := r.GetUserDevice(userID)
	if er != nil {
		return er
	}
	req := insertDevice
	if currentToken != "" {
		req = updateDevice
	}
	args := map[string]interface{}{
		"userID": userID,
		"token": token,
		"lang": lang,
	}
	_, err := r.db.NamedQuery(req, args)
	if err != nil {
		return errors.DBError(err)
	}
	return nil
}
