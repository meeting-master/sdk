package repository

import (
	"gitlab.com/meeting-master/sdk/customer/model"
	"gitlab.com/meeting-master/sdk/errors"
)

func (r repository) UpsertService(service model.ServiceRequest) (string, *errors.Error) {
	if !service.IsValid() {
		return "", errors.InvalidRequestData()
	}
	var upsertService = insertNewService
	if service.Id != "" {
		upsertService = updateService
	}
	row, err := r.db.NamedQuery(upsertService, service)
	if err != nil {
		return "", errors.DBError(err)
	}
	var insertID string
	if row.Next() {
		row.Scan(&insertID)
	}
	return insertID, errors.DBError(err)
}

func (r repository) DeleteService(id string) (string, *errors.Error) {
	request := model.DeleteRequest{id}
	row, err := r.db.NamedQuery(deleteService, request)
	if err != nil {
		return "", errors.DBError(err)
	}
	var deletedId string
	if row.Next() {
		row.Scan(&deletedId)
	}
	return deletedId, errors.DBError(err)
}

func (r repository) DeleteServicesByCustomer(customerId string) (string, *errors.Error) {
	request := model.DeleteRequest{customerId}
	row, err := r.db.NamedQuery(deleteServicesByCustomer, request)
	if err != nil {
		return "", errors.DBError(err)
	}
	var deletedId string
	if row.Next() {
		row.Scan(&deletedId)
	}
	return deletedId, errors.DBError(err)
}

func (r repository) ServicesByCustomer(predicate string) ([]ServiceResult, *errors.Error) {
	if len(predicate) == 0 {
		return nil, errors.InvalidRequestData()
	}
	var result []ServiceResult

	err := r.db.Select(&result, servicesByCustomer, predicate)
	return result, errors.DBError(err)
}

func (r repository) ServicesByOffice(predicate string) ([]ServiceResult, *errors.Error) {
	if len(predicate) == 0 {
		return nil, errors.InvalidRequestData()
	}
	var result []ServiceResult

	err := r.db.Select(&result, servicesByOffice, predicate)
	return result, errors.DBError(err)
}


