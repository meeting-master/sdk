package service

import (
	"context"
	"gitlab.com/meeting-master/sdk/auth/service"
	"gitlab.com/meeting-master/sdk/errors"
	"gitlab.com/meeting-master/sdk/messaging/engine"
	"gitlab.com/meeting-master/sdk/pub/model"
	"gitlab.com/meeting-master/sdk/pub/repository"
)

type PubService interface {
	//insert or update an announce
	Upsert(request model.PubRequest) (string, *errors.Error)

	//Delete a pub
	Delete(pubID, ownerID string) *errors.Error

	//return pub against the given ID
	PubById(ID string) (model.Pub, *errors.Error)

	//return list of pubs against a given owner id
	PubsByOwner(ownerID string) ([]model.Pub, *errors.Error)

	//return list of non validate pubs
	PubsToValidate(ctx context.Context) ([]model.Pub, *errors.Error)

	//return list of non validate pubs
	Validate(ctx context.Context, request model.ValidationRequest) ([]model.Pub, *errors.Error)

	//return list of pubs against a given location ids (country, town or district)
	//all pub without ID (world's pub are explicit included)
	PubsForLocations(locationIDs ...string) ([]model.Pub, *errors.Error)
}

type pubService struct {
	repo repository.PubRepository
	authService service.AuthService
	mailEngine  engine.MessageEngine
}

func NewPubService(repo repository.PubRepository) PubService {
	return InitWithEngine(repo, nil, nil)
}


func InitWithEngine(repo repository.PubRepository, auth service.AuthService, engine engine.MessageEngine) PubService {
	return &pubService{
		repo:  repo,
		authService: auth,
		mailEngine:  engine}
}

