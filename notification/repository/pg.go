package repository

import (
	"gitlab.com/meeting-master/sdk/errors"
	"time"
)


func (r repository) DayMessages() ([]DayNotification, *errors.Error) {
	var msg []DayNotification

	err := r.db.Select(&msg, getDayNotification)
	return msg, errors.DBError(err)
}

func (r repository) HourMessages() ([]DayNotification, *errors.Error) {
	var msg []DayNotification

	err := r.db.Select(&msg, getHourNotification)
	return msg, errors.DBError(err)
}

func (r repository) Announces(startDate time.Time) ([]DayNotification, *errors.Error) {
	var msg []DayNotification

	err := r.db.Select(&msg, getAnnounceNotification, startDate)
	return msg, errors.DBError(err)
}