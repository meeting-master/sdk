package repository

import "time"

type DayNotification struct {
	Name      string    `db:"name"`
	StartTime time.Time `db:"start_time"`
	Token     string    `db:"token"`
	Lang      string    `db:"lang"`
}
