# SDK

meeting master SDK

# purpose

provide function to build schedule and appointment solution

# Installation 

`go get -u gitlab.com/meeting-master/sdk`

# Import

`import gitlab.com/meeting-master/sdk`


#Version

1.1.3 - 2021-03-30
- mobile user validation
- customer activities

1.1.4 - 2021-04-04
- fix town latitude