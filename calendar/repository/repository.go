package repository

import (
	"gitlab.com/meeting-master/sdk/calendar/model"
	"gitlab.com/meeting-master/sdk/connector/pgsql"
	"gitlab.com/meeting-master/sdk/errors"
	"time"
)

type CalendarRepository interface {
	AppointmentById(ID string) (AppointmentInfo, *errors.Error)

	AppointmentsByTimeSlot(timeSlotIDs ...string) ([]AppointmentInfo, *errors.Error)

	//retrieve appointments for given end user id
	AppointmentSummary(userID string) ([]AppointmentSummaryInfo, *errors.Error)

	//retrieve appointment detail for given id
	AppointmentNotification(ID string) ([]AppointmentSummaryInfo, *errors.Error)

	CancelAppointment(appointmentID, timeSlotID, canceler, customerID string, place int) *errors.Error

	ConsumeAppointment(ID string) *errors.Error

	InsertAppointment(userID, CustomerID string, request model.AppointmentRequest) (string, *errors.Error)

	InsertTimeSlots(ts []TimeSlotInfo, history []CalendarHistoryDB) *errors.Error

	UpdateTimeSlot(request model.TimeSlotUpdateRequest, userId string) *errors.Error

	//insert planning parameters
	InsertSchedule(request []ScheduleDB) *errors.Error

	//insert planning parameters
	UpdateSchedule(request []ScheduleDB, officeID, serviceID string) *errors.Error

	//return calendar config for a given service
	ItemSchedule(officeID, serviceID string) ([]ScheduleDB, *errors.Error)

	CalendarHistory(officeID, serviceID string) ([]CalendarHistoryDB, *errors.Error)

	TimeSlotById(ID string) (TimeSlotResult, *errors.Error)

	//return time slots again given params
	TimeSlotsByRange(officeID, serviceID string, startDate, endDate time.Time) ([]TimeSlotResult, *errors.Error)

	//return time slots again given params
	TimeSlotsByDates(officeID, serviceID string, dates ...time.Time) ([]TimeSlotResult, *errors.Error)

	Calendar(request model.CalendarRequest) ([]CalendarResult, *errors.Error)

	UpsertRating(request model.RatingRequest, customerID string, customerRating float32) (string, *errors.Error)

	CustomerRating(appointmentID string) (CustomerRatingResult, *errors.Error)

	CountTimeSlot(officeID, serviceID string, startDate, endDate time.Time) (int, *errors.Error)

	GetUserDevice(userID string) (string, *errors.Error)

	UpsertUserDevice(userID, token, lang string)  *errors.Error
}

type repository struct {
	db *pgsql.DB
}


func NewCalendarRepository(db *pgsql.DB) CalendarRepository {
	return repository{db: db}
}
