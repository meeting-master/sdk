package repository


const (
	insertNewService = `INSERT INTO common.service (alias, name, customer_id)
						VALUES (:alias, :name, :customerid) 
						RETURNING id`

	updateService = `UPDATE common.service SET alias = :alias, name = :name, is_archived = :isarchived, customer_id = :customerid WHERE id = :id
						RETURNING id`

	servicesByCustomer = `SELECT s.id, s.alias, s.name, s.is_archived, s.customer_id, c.name as "customer_name", c.alias as "customer_alias"
							FROM common.service s
							inner join common.customer c on s.customer_id = c.id
							WHERE s.customer_id = $1`

	deleteService = `UPDATE common.service SET is_archived = true WHERE id = :id RETURNING id`

	deleteServicesByCustomer = `UPDATE common.service SET is_archived = true WHERE customer_id = :id`

	servicesByOffice = `select s.id, s.name, s.is_archived
						from common.service s, common.office o
						where s.id = ANY(o.service_ids)
						and o.id = $1`
)
