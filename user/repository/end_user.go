package repository

import "gitlab.com/meeting-master/sdk/errors"

func (r repository) InsertEndUser(ID, detail string) *errors.Error {
	tx := r.db.MustBegin()
	tx.MustExec(deleteEndUser, ID)
	_, err := tx.Exec(insertEndUser, ID, detail)
	if err != nil {
		_ = tx.Rollback()
		return errors.DBError(err)
	}
	if err := tx.Commit(); err != nil {
		return errors.DBError(err)
	}
	return nil
}

func (r repository) EndUserByID(ID string) (string, *errors.Error) {
	row, err := r.db.Query(endUserByID, ID)
	if err != nil {
		return "", nil
	}
	var detail string
	if row != nil && row.Next() {
		_ = row.Scan(&detail)
	}
	return detail, nil
}