package service

import (
	"gitlab.com/meeting-master/sdk/calendar/model"
	"gitlab.com/meeting-master/sdk/errors"
)

func (cs calendarService) InsertDevice(request model.DeviceRequest) *errors.Error {
	if !request.IsValid() {
		return errors.InvalidRequestData()
	}

	return cs.repository.UpsertUserDevice(request.UserID, request.Token, request.Lang)
}
