package repository

const (
	insertAppointment = `INSERT INTO common.appointment(
							time_slot_id,
							place_number,
							details,
							owner,
							created_by)
					VALUES(	:time_slot_id,
							:place_number,
							:details,
							:owner,
							:created_by)
							RETURNING id`

	updateTsByAppointment = `UPDATE common.time_slots SET consumed_appointments = consumed_appointments + :value WHERE id = :id`

	getAppointmentById = `SELECT ap.id, ap.time_slot_id, ap.place_number, ap.owner, ap.details, c.id as customer_id 
							FROM common.appointment ap
							INNER JOIN common.time_slots ts ON ap.time_slot_id = ts.id
							INNER JOIN common.service s ON ts.service_id = s.id
							INNER JOIN common.customer c ON s.customer_id = c.id
							WHERE ap.id = $1`

	getAppointmentByTsId = "SELECT id, time_slot_id , place_number, owner, details, created_at, consumed_at FROM common.appointment WHERE time_slot_id IN (?) AND canceled_at ISNULL"

	cancelAppointment = "UPDATE common.appointment SET canceled_at = now(), canceled_by = $1 WHERE id = $2"

	consumedAppointment = "UPDATE common.appointment SET consumed_at = now() WHERE id = $1"

	appointmentHistory =`select 
						c.name as customer_name,
						o.name as office_name,
						o.street_number,
						o.street_name,
						o.zip_code,
						o.phone_number,
						ts.start_time,
						ts.end_time,
						a.created_at,
						a.owner,
						a.id as appointment_id,
						a.place_number,
						r.id as rating_id,
						s.name as service_name
						from common.customer c
						inner join common.office o on c.id = o.customer_id
						inner join common.time_slots ts on o.id = ts.office_id
						inner join common.service s on s.id = ts.service_id
						inner join common.appointment a on ts.id = a.time_slot_id
						left join common.rating r on a.id = r.appointment_id 
						where a.owner = $1 AND a.canceled_at ISNULL`

	appointmentNotif =`select 
						c.name as customer_name,
						o.name as office_name,
						o.street_number,
						o.street_name,
						o.zip_code,
						o.phone_number,
						ts.start_time,
						ts.end_time,
						s.name as service_name,
						a.owner,
						a.details, 
						a.id as appointment_id,
						a.place_number
						from common.customer c
						inner join common.office o on c.id = o.customer_id
						inner join common.time_slots ts on o.id = ts.office_id
						inner join common.service s on s.id = ts.service_id
						inner join common.appointment a on ts.id = a.time_slot_id
						where a.id = $1`

)
