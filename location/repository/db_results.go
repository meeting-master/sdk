package repository

import (
	"database/sql"
	"github.com/google/uuid"
)

type Country struct {
	ID        string `db:"id"`
	Name      string `db:"name"`
	IsoCode   string `db:"iso_code"`
	Indicator int    `db:"indicator"`
}

type Town struct {
	ID          uuid.UUID       `db:"id"`
	Name        string          `db:"name"`
	Longitude   sql.NullFloat64 `db:"longitude"`
	Latitude    sql.NullFloat64 `db:"latitude"`
	CountryID   uuid.UUID       `db:"country_id"`
	CountryName string          `db:"country_name"`
}

type District struct {
	ID       uuid.UUID `db:"id"`
	Name     string    `db:"name"`
	TownID   uuid.UUID `db:"town_id"`
	TownName string    `db:"town_name"`
}

type LocationResult struct {
	ID       string `db:"id"`
	Location string `db:"location"`
	Country  string `db:"code"`
}
