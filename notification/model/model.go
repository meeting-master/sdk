package model

type Message struct {
	Token    string `db:"token"`
	Lang     string `db:"lang"`
	Hour     string `db:"hour"`
	Location string `db:"location"`
}

type AnnounceNotification struct {
	Name        string
	MailAddress string
	Code        string
}

