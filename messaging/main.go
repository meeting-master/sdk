package main

import (
	"database/sql"
	"github.com/joho/godotenv"
	"gitlab.com/meeting-master/sdk/connector/mailing"
	"gitlab.com/meeting-master/sdk/connector/pgsql"
	"gitlab.com/meeting-master/sdk/messaging/engine"
	"gitlab.com/meeting-master/sdk/messaging/model"
	"gitlab.com/meeting-master/sdk/messaging/repository"
	"log"
	"os"
	"time"
)

var mailService  mailing.Mailing

func main()  {
	if err := godotenv.Load(); err != nil {
		panic(err)
	}
	ml, err := mailing.New()
	if err != nil {
		panic(err)
	}
	mailService = ml
	pg, err := pgsql.Open()
	if err != nil {
		panic(err)
	}
	defer pg.Close()

	repo := repository.NewMessagingRepository(pg)

	mailEngine := engine.Init(repo)

	messageToSend, er := mailEngine.Messages()

	if er != nil {
		panic(er)
	}

	if messageToSend == nil {
		return
	}
	res := send(messageToSend)
	er = mailEngine.UpdateMessage(res)
	log.Println(er)
}

func send(messagesToSend []model.Message) []model.Message {
	results := make([]model.Message, len(messagesToSend))
	mailFrom := os.Getenv("SMTP_USER_DISPLAY")
	for k, m := range messagesToSend {
		 err := mailService.Send(mailFrom, m.To, m.Subject, m.Body)
		 if err != nil {
			 m.ErrorMessage = append(m.ErrorMessage, err.Error())
		} else {
			m.ValidatedAt = sql.NullTime{
				Time:  time.Now().UTC(),
				Valid: true,
			}
		 }
		m.Retry = m.Retry + 1
		results[k] = m
	}
	return results
}
